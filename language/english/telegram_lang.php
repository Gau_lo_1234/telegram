<?php
$lang['message'] = "Message";
$lang['sent_date'] = "Sent Date";
$lang['view'] = "View";
$lang['telegram_setup'] = "Setup";

$lang['telegram_detail_title'] = "telegram account for %s";

$lang['telegram_account_type'] = 'Account Type';
$lang['telegram_account'] = 'telegram Account';
$lang['telegram_credits_used'] = 'telegrames used';
$lang['telegram_credits_available'] = 'telegrames available';

$lang['telegram_add_credits'] = "Add telegrames";
$lang['telegram_add_credits_success'] = "telegrames have successfully been added to account.";

$lang['telegram_remove_credits'] = "Remove telegrames";
$lang['telegram_remove_credits_success'] = "telegrames have successfully been removed from account.";

$lang['telegram_message_title'] = "telegrames Sent";
$lang['telegram_message_hint'] = "telegrames.";

$lang['telegram_email_notification_title'] = "Email Notifications";
$lang['telegram_email_notification_hint'] = "<h4>What is this?</h4><p><b>Credit Accounts:</b> A monthly statement is sent to the users listed</p><p><b>Prepaid Accounts:</b> An email is sent when credits on the account reach 5 and then evertime one is used till there is none left.</p>";
$lang['no_notifications'] = "No notifiers have been setup.";
$lang['telegram_add_email'] = "Add Notifier";
$lang['telegram_remove_email'] = "Remove Notifier";
$lang['telegram_email_add_success'] = "Email notification for %s has been added.";
$lang['telegram_email_add_duplicate'] = "Email notification already in system.";
$lang['telegram_email_remove_success'] = "Email notification for %s has been removed.";

$lang['telegram_credits_log'] = "telegrames Purchase Log";
$lang['telegram_credits_log_hint'] = "<h4>What is this?</h4>The telegrames Purchase Log allows you to view where and when telegrames have been used.";

$lang['no_log_entries'] = 'No log entries.';
$lang['not_setup'] = 'No telegram account setup yet.';

$lang['settings_telegram_template'] = 'Template Text';
$lang['settings_telegram_edit_template'] = 'Edit telegram Template Text';
$lang['settings_telegram_edit_template_success'] = 'telegram Template has successfully been edited.';


$lang['settings_telegram_test'] = 'Test telegram account';
$lang['settings_telegram_test_success'] = 'Test telegram has successfully been sent.';
$lang['settings_telegram_test_fail'] = 'An error occured sending test telegram';


$lang['settings_telegram_test_number'] = 'Test cellphone number:';


$lang['settings_telegram_title'] = "Telegram Settings";

$lang['settings_telegram_country'] = "Country";
$lang['settings_telegram_country_note'] = "Identifier for accounts...";

$lang['settings_telegram_key'] = "BOT Key";
$lang['settings_bot_name'] = "BOT name";
$lang['settings_telegram_key_note'] = "";

$lang['settings_telegram_key_pass'] = "Key Pass";
$lang['settings_telegram_key_pass_note'] = "";

$lang['settings_telegram_edit_account'] = "Edit telegram Account";
$lang['settings_telegram_edit_account_success'] = "telegram account has been updated.";
$lang['settings_telegram_add_account'] = "New Telegram BOT";
$lang['settings_telegram_add_account_success'] = "New Telegram account has been added.";
$lang['settings_telegram_delete_account_success'] = "telegram account has been deleted.";
$lang['telegram_accounts'] = "Telegram Accounts";
$lang['telegram_account'] = "Telegram Account";

$lang['telegram_client_setup_success'] = 'Telegram account for client has been updated.';

$lang['no_telegram_alerts'] = "No telegram alerts have been set up yet.";
$lang['manage_telegram_alerts'] = "Manage Telegram - Alert Points";
$lang['manage_site_telegram_alerts'] = "Manage Telegram - Site Alerts";
//$lang['manage_telegram_alerts'] = "Manage telegram Alerts";

$lang['telegram_alerts'] = "Telegram Alerts";
$lang['telegram_recipients'] = "Telegram Alerts Recipients";
$lang['telegram_no_users'] = "Hint: If you don't see the person / cellphone number you're looking for in the drop down, you will need to add a new user to the system first.";
$lang['telegram_alert_help'] = "<p>Hint: If you don't see the person / cellphone number you're looking for in the
            drop down, you will need to %s
             to the
            system first.
        </p>";

$lang['telegram_alerts_edit_success'] = "Telegram alerts have been updated! %s";
$lang['telegram_alert_remove_success'] = 'Telegram alert has been removed.';

/*mailers*/
$lang['credits_statement_subject'] = "%s: telegram Statement %s - %s.";
$lang['credits_statement_content_html'] = "<p>telegram Credits statement for %s - %s:</p>";
$lang['credits_statement_content_txt'] = "telegram Credits statement for %s - %s:";

$lang['telegram_credits_low_subject'] = "%s: Low telegram Credits Warning.";
$lang['telegram_credits_low_content_html'] = "<p>The telegram credits on the account %s is running low.</p><p>Please be aware that no telegram will be sent if there are no credits on the account</p>";
$lang['telegram_credits_low_content_txt'] = "The telegram credits on the account %s iis running low.\n\n Please be aware that no telegram will be sent if there are no credits on the account";

$lang['telegram_proof_of_purchase_subject'] = "%s: Proof of purchase for telegrames";
$lang['telegram_proof_of_purchase_content_html'] = "<p>Total cost: %s credit/s </p><p>telegrames bought: %s</p>";
$lang['telegram_proof_of_purchase_content_txt'] = "Total cost: %s  credit/s \n\n telegrames bought: %s\n";

$lang['report_telegram_messages_report_help'] = 'Report showing a list of telegram messages sent within a selected time span.';
$lang['report_telegram_messages_report_description'] = 'A list telegram messages sent.';