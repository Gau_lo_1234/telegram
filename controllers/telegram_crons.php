<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


/**
 * Cronjobs for the telegram Module
 *
 * @package telegram
 * @author Elsabe Lessing (http://www.lessink.co.za/)
 */
class telegram_Crons extends MY_Controller {
    const CRON_ID = 'accounts';

    function __construct() {
        parent::__construct();
        $this->load->helper('date');

        $this->load->config('telegram');
        $this->load->model('accounts/m_accounts');
        $this->load->model('m_telegram_sender');
        $this->load->model('m_telegram_alerts');
        $this->load->model('m_telegram_accounts');
        $this->load->model('m_telegram_reports');
        $this->load->model('m_telegram_credit_log');

        $this->load->model('m_client');
        $this->load->model('m_distributor');

        $this->load->helper('cron');
        $this->load->helper('report_telegram_messages_report');
        $this->load->helper('email');
        $this->load->helper('log');

        $this->load->language('accounts/accounts');
        $this->load->language('telegram');
    }

    function index() {
        echo "Shouldn't be here...";
        redirect('/');
    }

    function hourly_site_inctivity(){
        alerts_inactivity();
    }

    function send_telegram_reports() {

        if(($pid = CronHelper::lock('send_telegram_reports')) !== FALSE) {

            $google_url_api_key = $this->m_settings->get_by_key('google_url_api');
            if ($google_url_api_key->value !== '') {
                $this->load->library('telegram/GoogleURL');
                                //initialize with the google API Key
                $this->googleurl->_initialize($google_url_api_key->value);
            }

            $this->load->helper('report');
            $this->load->model('m_telegram_accounts');

            $now = time();
            $hour = date('H', $now);
            $day = date('d', $now);
            $day_of_week = date('N', $now);


            /* Hourly telegram Reports */
            $log_message = "Hourly telegram Reports: ".date('Y-m-d H:i:s', $now)."\n";

            $end_gmt = mktime($hour, 0, 0);
            $start_gmt = $end_gmt - 3600; //hour ago

            echo 'Hourly: '.date('c', $end_gmt)." ".date('c', $start_gmt)."\n";

            $telegram_alerts = $this->m_telegram_alerts->get_hourly_telegram_reports();
           
            if ($telegram_alerts) {
                $log_message .= $this->_process_telegram_report($telegram_alerts, $start_gmt, $end_gmt);
            }
            // echo '<hr/>';


            /* Daily telegram Reports */
            $log_message = "Daily telegram Reports: ".date('Y-m-d H:i:s', $now)."\n";

            $start_gmt = $now - 86400;
            $end_gmt = $now;

            echo 'Daily: '.date('c', $end_gmt)." ".date('c', $start_gmt)."\n";

            $telegram_alerts = $this->m_telegram_alerts->get_daily_telegram_reports($hour);
          
            if ($telegram_alerts) {
                $log_message .= $this->_process_telegram_report($telegram_alerts, $start_gmt, $end_gmt);
            }
            // echo '<hr/>';


            /* Weekly telegram Reports */
            $log_message = "Weekly telegram Reports: ".date('Y-m-d H:i:s', $now)."\n";

            $start_gmt = strtotime( date("Y-m-d H:00:00", strtotime( "-1 weeks")));
            $end_gmt = strtotime( date("Y-m-d H:00:00"));

            echo 'Weekly: '.date('c', $end_gmt)." ".date('c', $start_gmt)."\n";

            $telegram_alerts = $this->m_telegram_alerts->get_weekly_telegram_reports($hour, $day_of_week);

            if ($telegram_alerts) {
                $log_message .= $this->_process_telegram_report($telegram_alerts, $start_gmt, $end_gmt);
            }
            // echo '<hr/>';


            /* Monthly telegram Reports */
            $log_message = "Monthly telegram Reports: ".date('Y-m-d H:i:s', $now)."\n";

            $start_gmt = strtotime( date("Y-m-d H:00:00", strtotime( "-1 weeks")));
            $end_gmt = strtotime( date("Y-m-d H:00:00"));

            echo 'Monthly: '.date('c', $end_gmt)." ".date('c', $start_gmt)."\n";

            $mailer_day[] = $day;
            // Cater if someone selected "31" not all monhts have 31 days!!!! *lame*
            // do itfrom 218 for the februaury thingy...
            if ($day >= 28)  {
                $month = date('n', $now);
                $year = date('Y', $now);
                $day++;
                while (!checkdate($month, $day, $year) && $day < 32) {
                    $mailer_day[] = $day;
                    $day++;
                }

            }

            foreach($mailer_day as $day_of_month) {
                
                $telegram_alerts = $this->m_telegram_alerts->get_monthly_telegram_reports($hour, $day_of_month);
                if ($telegram_alerts) {
                  
                    $log_message .= $this->_process_telegram_report($telegram_alerts, $start_gmt, $end_gmt);
                    break;
                }//if mailers
            }
            // echo '<hr/>';


            $log_message .= 'Done.';
            //commented out 27_03_2018 Terence Bruwer Magtouch Electronics
            //$filename = APPPATH.'/logs/telegram_reports_'.date('m_Y').'.php';
            $filename = APPPATH.'/logs/telegram_reports_'.date('m_Y').'.log';
            write_log($filename, $log_message);


            CronHelper::unlock('send_telegram_reports');
         }
    }

    function _process_telegram_report($telegram_alerts, $start_gmt, $end_gmt) {
        $log_message = '';

        foreach($telegram_alerts as $telegram) {
            // echo $telegram->cellphone.' '.$telegram->module;
            // echo '<hr/>';
                //print_r($telegram);
            if (!$telegram->module || check_site_active($telegram->module, $telegram->site_id)) {
                $log_message .= "Send, {$telegram->report}, to {$telegram->cellphone}\n";
                 
                load_report_helper($telegram);

                $telegram_generation_func = "get_telegram_".$telegram->report;
                $report_link_func = "get_report_link_".$telegram->report;
          
                //generate report content...
                if (function_exists($telegram_generation_func)) {
                    // echo 'Function found <br/>';
                  
                    // TODO REPORT SETTINGS????
                    //send telegram
                    $telegram_content = $telegram_generation_func($telegram->site_id, $start_gmt, $end_gmt);
                    // echo '<br/>';

                    if (function_exists($report_link_func)) {
                        $link = $report_link_func($telegram->site_id, $start_gmt, $end_gmt);
                    } else {
                        $link = BASE_URL;
                    }
                    if (class_exists('GoogleURL')) {
                        $short_link = $this->googleurl->shorten($link);
                        if ($short_link) {
                            $link = $short_link;
                        }
                    }

                    $tail = " ".$link;

                    // telegram have a limited amount of characters.
                    $telegram_content = substr($telegram_content, 0, 560-strlen($tail)).$tail;

                    // echo $telegram_content."\n";

                    // echo '<br/>';
                    // echo 'Length: '.strlen($telegram_content);


                    $telegram_content ="This is a test message generated at ".time();
                    $this->_send_telegram($telegram->client_id, $telegram->chat_id, $telegram->site_id, $telegram->user_id, $telegram->cellphone, $telegram_content, $telegram->telegram_alert_id);
                    // echo '-';
                } else {
                    // echo 'No function <br/>';
                }
                

            }

        }
        return $log_message;
    }

    function _send_telegram($client_id, $chat_id, $site_id, $user_id, $cellphone, $telegram_content, $telegram_alert_id) {
        // SEND telegram
        $status = '';
        $delivery_response = '';
          
        $this->load->model('m_telegram_message');
        $this->load->model('m_telegram_accounts');
        $this->load->library('telegram/telegram_sender');

        $telegram_account = $this->m_telegram_accounts->get_by_client($client_id, true );
        
      
                // okay send it :D
                try {

                   
                    $delivery_response = $this->telegram_sender->send_message($chat_id, $telegram_content);
                    $this->m_telegram_alerts->update_last_sent($telegram_alert_id);
                    $this->m_telegram_message->create($telegram_alert_id, $client_id, $user_id, $cellphone, $telegram_content, $delivery_response, $status);

                } catch(Exception $e) {
                    $status = 'error';
                    $delivery_response = $e->getMessage();
                }
      
    

           }

    function _do_log($filename, $log_message="") {
        // open log file
        if (!file_exists($filename)) {
            $log_message = "<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>\n\n".$log_message;
        }
        $fh = fopen($filename, "a+");
        if ($fh) {
            fwrite($fh, $log_message);
            fclose($fh);
        } else {
            echo "No log file: \n";
            echo $log_message;
        }
    }

}

/* End of file accounts/accounts_crons.php */
/* Location: ./application/controllers/accounts/accounts_crons.php */
