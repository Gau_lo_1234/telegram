<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
* telegram Module
*
* @package telegram
* @author Elsabe Lessing (http://www.lessink.co.za/)
*/
class telegram_messages_report extends MY_Controller {
    var $data;

    function __construct() {
        parent::__construct();

        if (!$this->m_modules->is_active('telegram')) {
            $this->session->set_flashdata("message", $this->lang->line('module_not_enabled'));
            $this->session->Set_flashdata('message_type', "error");
            redirect('/');
        }

        $this->load->config('telegram');


        $this->load->language('telegram');

        $this->load->library('form_validation');


        $this->load->model('telegram/m_telegram_accounts');
        $this->load->model('telegram/m_telegram_message');
        $this->load->model('m_client_email');
        $this->load->model('m_site');
        $this->load->model('m_client');
        $this->load->helper('email');

        $this->load->helper('report_telegram_messages_report', 'telegram');
        $this->load->helper('report');
       $this->load->model('m_report_type');

        $this->data['acl'] = $this->data['acl'] = $this->m_permissions_roles->get_user_list($this->tank_auth->get_user_role());

        $this->data['asset_path'] = base_url()."assets/";
        $this->data['telegram_asset_path'] = base_url().$this->config->item('telegram_assets');
    }
    function view($param=null) {
        $this->Index($param);
    }
    function Index($param=null) {
        $this->data['show_point_groups'] = false;
        $this->data['show_points'] = false;
        $this->data['show_site'] = false;
        if (isset($this->data['acl']['single_client'])) {
            $this->data['show_client'] = false;
            $this->data = array_merge(get_accessible_sites('sites'), $this->data);
        } else {
            $this->data['show_client'] = true;
            $this->data = array_merge(get_accessible_clients('clients'), $this->data);
        }

        $this->data['tz'] = timezones($this->tank_auth->get_user_timezone());

        $this->data['title'] = $this->lang->line('report_telegram_messages_report_title');
        $this->data['report_name'] = 'telegram_messages_report';
        $this->data['module'] = 'telegram';
        $this->data['nav'] = 'reports';

        $this->__set_validation();
        $this->data['errors'] = array();
        if ($this->form_validation->run()) { // validation ok
            // PERMISSIONS
            check_permissions(array('client_id'=> $this->form_validation->set_value('client')));

            // gimme da cooookie
            $report_data = array(
                );
            if ($this->data['show_client'] ) {
                $report_data['client'] = $this->form_validation->set_value('client');
            }
            $this->session->set_userdata('telegram_messages_report', $report_data);
            //--

            $this->data['generate'] = True;


            $start_gmt = strtotime($this->form_validation->set_value('start_date')) - ($this->data['tz'] * 3600);
            $end_gmt = strtotime($this->form_validation->set_value('end_date')) - ($this->data['tz'] * 3600);

            $this->data['telegram_messages'] = get_data_telegram_messages_report($this->form_validation->set_value('client'), $start_gmt, $end_gmt);

        } else {
            $this->data['generate'] = False;
        }

        $this->data['saved_settings'] = $this->session->userdata('detail_report');
        if ($this->data['show_client']) {
            if (isset($this->data['saved_settings']['client'])) {
                $this->data['sites'] = $this->m_site->get_client_list($this->data['saved_settings']['client']);
            } else {
                $this->data['sites'] = $this->m_site->get_client_list($this->data['clients'][0]->id);
            }
        }

        /* Site mailers */
        $this->data['report'] = $this->m_report_type->get_by_handle($this->data['report_name']);
        $this->data['mailers'] = $this->m_client_email->get_by_user_id($this->tank_auth->get_user_id(), $this->data['report']->id);


        $this->data['start_field'] = array(
            'name'  => 'start_date',
            'id'    => 'start_date',
            'size'  => 20,
            'value' => set_value('start_date', date('Y-m-d H:i', gmt_to_local(time()-86400, $this->tank_auth->get_user_timezone()))),
            'class' => 'date'
            );
        $this->data['end_field'] = array(
            'name'  => 'end_date',
            'id'    => 'end_date',
            'size'  => 20,
            'value' => set_value('end_date', date('Y-m-d H:i', gmt_to_local(time(), $this->tank_auth->get_user_timezone()))),
            'class' => 'date'
            );
            //pass clied id and name to the vie
            $this->data['clientID'] = end($this->uri->segment_array());
            if(isset($this->data['clientID'])){
                if(is_int($this->data['clientID'])){
                    $this->data['clientName']=$this->m_client->fetch_client_name($this->data['clientID']);
                }
            }
        $this->layout->view('reports/layout', $this->data);

    }//Index


    /**
     * generate_csv
     *
     * Outputs report in CSV format to the browser
     *
     * @access public
     *
     * @return CSV
     */
    function generate_csv() {
        $this->data['show_client'] = !isset($this->data['acl']['single_client']);

        $this->__set_validation();
        $this->form_validation->run();

        $client = $this->m_client->get_by_id($this->form_validation->set_value('client'));
        if ($client) {
            // PERMISSIONS
            check_permissions(array('client_id'=> $client->id));

            $tz = timezones($this->tank_auth->get_user_timezone());
            $start = strtotime($this->form_validation->set_value('start_date')) - ($tz * 3600);
            $end = strtotime($this->form_validation->set_value('end_date')) - ($tz * 3600);

            $telegram_messages = get_data_telegram_messages_report($this->form_validation->set_value('client'), $start, $end);

            $vdata['filepath'] = 'tmp/';
            $vdata['filename'] = str_replace(":", "_", str_replace(" ", "_", 'telegram_message_report_'.$this->form_validation->set_value('client').'_'.$this->form_validation->set_value('start_date').'_to_'.$this->form_validation->set_value('end_date').'.csv'));

            $fh = fopen($vdata['filepath'].$vdata['filename'], "w");
            if ($fh) {
                //heading
                $data = array(
                    $this->lang->line('cell'),
                    $this->lang->line('message'),
                    'Status',
                    'Error',
                    $this->lang->line('sent_date'),
                    );

                fputcsv($fh, $data, ",", '"');
                //data
                $duration = $current_insert_date = $extra_duration = $dwell_time = $count = 0;
                foreach($telegram_messages as $key=>$message) {
                    try {
                        libxml_use_internal_errors(true);
                        $response = new SimpleXMLElement($message->delivery_response);
                        $message_response = $response->call_result->error;
                    } catch(Exception $e) {
                        $message_response = $message->delivery_response;
                    }
                    $data = array (
                        trim($message->number),
                        trim($message->message),
                        $message->status,
                        $message->status == 'okay' ? '' : $message_response,
                        ($message->created_date ? date('Y-m-d H:i:s',gmt_to_local($message->created_date, $this->tank_auth->get_user_timezone())) : '-')
                    );
                    fputcsv($fh, $data, ",", '"');
                    $count++;
                }

                fclose($fh);
            } else {
                echo "PROBLEM";
            }

            $this->layout->setLayout('layouts/csv');
            $this->layout->view('csv/plain', $vdata);
        } else {
            redirect('/');
        }
    }


    /**
     * generate_pdf
     *
     * Generates a PDF for the report, and prompts user to download.
     * Can handle AJAX or normal call, end result is slightly different:
     *  - AJAX: writes PDF to a temp file on server
     *  - Normal: streams PDF to browser, no files generated
     *
     * @access public
     */
    function generate_pdf() {
        $this->data['show_client'] = !isset($this->data['acl']['single_client']);
        $this->__set_validation();
        $this->form_validation->run();

        $this->data['client'] = $this->m_client->get_by_id($this->form_validation->set_value('client'));

        // PERMISSIONS
        check_permissions(array('client_id'=> $this->data['client']->id));

        $tz = timezones($this->tank_auth->get_user_timezone());
        $this->data['start'] = strtotime($this->form_validation->set_value('start_date')) - ($tz * 3600);
        $this->data['end'] = strtotime($this->form_validation->set_value('end_date')) - ($tz * 3600);

        $settings = array(
            'mailer' => (isset($this->data['send_mail']) && $this->data['send_mail']) ? $this->data['send_mail'] : false
            );

        $telegram_messages = get_data_telegram_messages_report($this->form_validation->set_value('client'), $this->data['start'], $this->data['end']);

        if(! $this->form_validation->set_value('ajax')) {
            // normal call streams to browser.
            $filename = str_replace(":", "_", str_replace(" ", "_", 'telegram_messages_report_'.$this->form_validation->set_value('client').'_'.$this->form_validation->set_value('start_date').'_to_'.$this->form_validation->set_value('end_date')));
            // generate_pdf_detail($clockings, $this->data['site'], $this->data['start'], $this->data['end'], $this->tank_auth->get_user_timezone(), $filename, 'I', '', $settings);

            $filename = generate_download_telegram_messages_report($telegram_messages, $this->data['client'], $this->data['start'], $this->data['end'], $this->tank_auth->get_user_timezone(), $filename, 'I', ASSET_PATH, $settings);
        } else {
            // AJAX call writes to a file
            $filename = str_replace(":", "_", 'tmp/'.str_replace(" ", "_", 'telegram_messages_report_'.$this->form_validation->set_value('client').'_'.$this->form_validation->set_value('start_date').'_to_'.$this->form_validation->set_value('end_date')));

            
            // REMOVED 27/09/2012 the file check because this report is a bit more complicated now.
            // if (!file_exists($filename)) { // We need to generate a new pdf!
            // generate_pdf_detail($clockings, $this->data['site'], $this->data['start'], $this->data['end'], $this->tank_auth->get_user_timezone(), $filename, 'F', '', $settings);
            $filename = generate_download_telegram_messages_report($telegram_messages, $this->data['client'], $this->data['start'], $this->data['end'], $this->tank_auth->get_user_timezone(), $filename, 'F', ASSET_PATH, $settings);
            // }
            if (isset($this->data['send_mail']) && $this->data['send_mail']) {
                return $filename;
            } else {
                echo $filename;
                return; // stop rendering!!!
            }
        }

    }

    /**
     * __set_validation
     *
     * @access private
     */
    function __set_validation() {
        if ($this->data['show_client']) {
            $this->form_validation->set_rules('client', $this->lang->line('client'), 'trim|required|xss_clean');
        }

        if (isset($this->data['send_mail']) && $this->data['send_mail']) {
            $this->form_validation->set_rules('user', $this->lang->line('user'), 'trim|required|xss_clean');
        }

        // $this->form_validation->set_rules('site', $this->lang->line('site'), 'trim|required|xss_clean');
        $this->form_validation->set_rules('start_date', $this->lang->line('start_date'), 'trim|required|xss_clean');
        $this->form_validation->set_rules('end_date', $this->lang->line('end_date'), 'trim|required|xss_clean');
        $this->form_validation->set_rules('ajax', 'Ajax', 'trim|xss_clean');
    }

}
