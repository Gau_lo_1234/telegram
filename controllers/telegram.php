<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
* telegram Module
*
* @package telegram
* @author Elsabe Lessing (http://www.lessink.co.za/)
*/
class telegram extends MY_Controller {
    var $data;

    function __construct() {
        parent::__construct();

        if (!$this->m_modules->is_active('telegram')) {
            $this->session->set_flashdata("message", $this->lang->line('module_not_enabled'));
            $this->session->Set_flashdata('message_type', "error");
            redirect('/');
        }

        $this->load->config('telegram');
        $this->load->language('telegram');
        $this->load->library('form_validation');
        $this->load->model('m_telegram_sender');
        $this->load->model('m_telegram_alerts');
        $this->load->model('m_telegram_accounts');
        $this->load->model('m_telegram_bot');
        $this->load->model('m_telegram_message');
        $this->load->model('m_telegram_reports');
        $this->load->model('m_telegram_credit_log');
        $this->load->model('m_site');
        $this->load->model('m_client');
        $this->load->model('accounts/m_accounts');
        $this->load->helper('email');
        $this->load->helper('report');
        $this->load->helper('util');
        $this->load->helper('telegram_bot');

        $this->data['acl'] = $this->data['acl'] = $this->m_permissions_roles->get_user_list($this->tank_auth->get_user_role());

        $this->data['asset_path'] = base_url()."assets/";
        $this->data['telegram_asset_path'] = base_url().$this->config->item('telegram_assets');
    }
    /**
     * @method encrypt
     * @param key
     */
    function encrypt(){
    $value=$this->input->get('name');
    echo $this->base_convert_arbitrary($value, 10, 31);
    }

    /**
     * @method del_multi_alerts
     */
    function del_multi_alerts(){

        $ids= $this->input->post('myData');
        $ids= json_decode( $ids);
        foreach ($ids as $id){
            $this->m_telegram_alerts->delete($id);
        }
        $this->session->set_flashdata('message', 'successfull');
        $this->session->set_flashdata('message_type', 'notice');
    }
    /**
     * @method base_convert_arbitrary
     * @param number
     * @param fromBase
     * @param toBase
     * @return string encrypted
     */
    function base_convert_arbitrary($number, $fromBase, $toBase) {
        $digits = '0123456789bcdfghjklmnpqrstvwxyz';
        $length = strlen($number);
        $result = '';
    
        $nibbles = array();
        for ($i = 0; $i < $length; ++$i) {
            $nibbles[$i] = strpos($digits, $number[$i]);
        }
    
        do {
            $value = 0;
            $newlen = 0;
            for ($i = 0; $i < $length; ++$i) {
                $value = $value * $fromBase + $nibbles[$i];
                if ($value >= $toBase) {
                    $nibbles[$newlen++] = (int)($value / $toBase);
                    $value %= $toBase;
                }
                else if ($newlen > 0) {
                    $nibbles[$newlen++] = 0;
                }
            }
            $length = $newlen;
            $result = $digits[$value].$result;
        }
        while ($newlen != 0);
        while (strlen($result)<4) {
            $result = $digits[0].$result;
        }
        return $result;
    }
    
    function Index() {
        redirect();
    }//Index

    /**
     * The following function unlinks a registered user
     */
    function remove_account($user_id=null){

        if($this->m_telegram_accounts->delete_by_user($user_id)){
            $this->session->set_flashdata('message', 'Account unlinked successfully');
            $this->session->set_flashdata('message_type', 'notice');
        }
        else{
            $this->session->set_flashdata('message', 'Failed to unlink accounts');
            $this->session->set_flashdata('message_type', 'error');
        }
        redirect('profile'); 
    }
    //convert from number to charaterstring for registartion
    //function to convert from number to string
    function numtokey($num) {
        $result = chr(ord('a')+(($num/(26*26*26)) % 26));
        $result .= chr(ord('a')+(($num/(26*26)) % 26));
        $result .= chr(ord('a')+(($num/26) % 26));
        $result .= chr(ord('a')+($num % 26));
        return($result);
    }
    
    
    //function to convert from string to number
    function keytonum($key) {
        $tize = strtolower($key);

        #check that all characters are in a-z and its 4 long otherwise error
        $num = 0;
        if (strlen($tize)==4) {
        $num += (ord( $tize[0] )-ord('a'))*26*26*26;
        $num += (ord( $tize[1] )-ord('a'))*26*26;
        $num += (ord( $tize[2] )-ord('a'))*26;
        $num += (ord( $tize[3] )-ord('a'));
        }
        return($num);
    }
    
    

     /**
     * @method reply
     */
    function reply(){
        
        // set location
        $token = "bot845865348:AAFoI5W24Ds-3Hj3MfIsSqiTdA9i_1xDO2I";

        //set map api url
        $url = "https://api.telegram.org/$token/getUpdates";
          
        //call api
        $json = file_get_contents($url);
        $json = json_decode($json);
        $data['json']=$json ;
       
        $this->load->view('telegram_bot',$data);
     }
   /**
     * @method get_list
     * @param chat_id
     * @param response
     * @param date
     */
     function get_list($chat_id=null, $response=null, $date=null){
         $this->m_telegram_bot->get_list($chat_id, $response, $date);
     }
    
    /**
     * @method add_new
     * @param array()
     */
    function add_new($data=array()){
        return $this->m_telegram_bot->add_new($data);
    }
    /**
     * @method fetch_client_users
     * @param user_id
     */
    function fetch_client_users($user_id=null){
        return $this->m_telegram_bot->fetch_client_users($user_id);
    }

    /**
     * @method account_setup
     * @param setup
     */
    function account_setup($setup=null){
        return $this->m_telegram_bot->account_setup($setup);
    }

     /**
     * @method update_chat_id
     * @param response
     * @param chat_id
     */
    function update_chat_id($response=null, $chat_id=null){
        return update_chat_id($response, $chat_id);
    }
    /**
    * @method reply
    */
    function server(){
    
    $this->load->view('telegram_server');
    }

    /**
    * @method reply
    */
    function bot_run(){
    
        $this->load->view('bot_run');
        }

    /**
     * detail
     *
     * DEPRECATED
     *
     * @param int  $telegram_account_id
     * @param string $type
     * @param int    $start
     *
     * @access public
     *
     * @return void
     */
    function detail($telegram_account_id, $type='log',  $start=0) {
        redirect('/');
    }

    /**
     * alerts
     *
     * @param int $site_id
     * @param int $user_id
     *
     * @access public
     *
     * @return void
     */
    function alerts($type="site", $id=0, $user_id=0, $c_id=0) {


        $this->load->model('m_report_type');
       
        if ($type =='client') {
            $site_id=$this->input->post('site_id');
            $this->data = array_merge($this->data, get_accessible_clients());
            $this->data['client_id'] = $id;
            $this->data['client'] = $this->m_client->get_by_id($id, false, true);
            $this->data['site_id'] = isset($site_id)?$site_id:0;
            $this->data['sites'] = $this->m_site->get_client_list($id,false,false,false,'patrols');
      

        } else {

            
            $this->data = array_merge($this->data, get_accessible_sites());
            $this->data['site_id'] = $id;
           
            if ($id == 0) {

                if ($c_id) {
                   
                    $this->data['client_id']  = $c_id;
                    $this->data = array_merge($this->data, get_accessible_sites('sites_list'));
                    $this->data['sites'] = array();
                   
                    foreach($this->data['sites_list'] as $site) {
                        if ($site->client_id == $c_id) {
                            //add access value for each site in list
                           
                            $site->access = $this->m_accounts->moduleAccessBySite('patrols',$c_id,$site->id);
                           
                            $this->data['sites'][] = $site;
                        }
                    }
                    //$this->data['site_id'] = $this->data['sites'][0]->id;

                    $this->data['sites'][0]->id=$this->input->post('site_id');
                    $this->data['site'] = $this->m_site->get_by_id($this->data['sites'][0]->id, false, false);
                  
                    unset($this->data['sites_list']);

                } else {
                    
                    $this->data['client_id']  = null;
                    $this->data = array_merge($this->data, get_accessible_sites('sites',true,true,true,'patrols'));
                
                }
            } else {

                $this->data['site'] = $this->m_site->get_by_id($id, false, true);
                $this->data['sites'] = $this->m_site->get_by_id($id, false, true,false,false,'patrols');
                $this->data['client_id'] = $this->data['site']->client_id;
              

            }
        }
      
        $this->data['edit'] = FALSE;
        $this->data['user_id'] = $user_id;
        $this->data['type'] = $type;
        $this->data['type_id'] = $id;

        check_permissions(array(
            'user_id'=>$user_id,
            'client_id'=>$this->data['client_id'],
            'acl'=>'manage_telegram_alerts',
            ));
         
        $this->data['telegram_grouped_reports'] = $this->m_telegram_reports->get_grouped_array(1);
        $this->data['tz'] = timezones($this->tank_auth->get_user_timezone());
        
        if(isset($_POST['client_id'])){
            $this->data['client_id'] = $_POST['client_id'];
        }
        if(isset($_POST['site_id'])){
            $this->data['site_id'] = $this->input->post('site_id');
        }
       

        if (isset($_POST['user_select'])) {
            $this->data['user_id'] = $_POST['user_select'];
        } else {
            
            $this->form_validation->set_rules('user_id', $this->lang->line('user'), 'trim|required|xss_clean');
            $this->form_validation->set_rules('frequency[]', $this->lang->line('frequency'), '');
            $this->data['errors'] = array();

          
          
            if ($this->form_validation->run()) { // validation ok

                echo "Client ID: ".$this->input->post('client_id');
                echo "Site ID: ".$this->input->post('site_id');
                echo "Alerts: ";
               
                $this->data['client_id'] = $this->input->post('client_id');
                $this->data['site_id'] = $this->input->post('site_id');
                check_permissions(array(
                    'user_id'=>$this->form_validation->set_value('user_id'),
                    ));

                $user = $this->users->get_user($this->form_validation->set_value('user_id'));
                $client_id = $this->input->post('client_id');
                $site_id = $this->input->post('site_id');
                $alerts = $this->input->post('alerts');
                $frequencies = $this->input->post('frequency');
                $daily_time = $this->input->post('daily_time');
                $weekly_time = $this->input->post('weekly_time');
                $weekly_day  = $this->input->post('weekly_day');
                $monthly_time  = $this->input->post('monthly_time');
                $monthly_day  = $this->input->post('monthly_day');
                $err = false;

                $report_settings = $this->input->post('settings');

                foreach( $this->data['telegram_grouped_reports'] as $group =>$reports) {
                    foreach($reports as $report_id=>$report) {


                        /* ALERT POINT REPORT */
                        if ($report->report == 'alert_point') {

                            if (isset($alerts[$report->id])) {
                                
                                $settings = isset($report_settings[$report_id]) ? $report_settings[$report_id] : array();
                                if (!empty($settings)) {

                                    $se = $this->m_telegram_alerts->get_by_site_user_id($this->form_validation->set_value('user_id'), $this->data['site_id'], $report->id, $this->data['client_id']);
                                    if (is_array($se)) {

                                        foreach($se as $mailers) {
                                            //check if setting in system.
                                            foreach($mailers as $mailer) {
                                                $mailer_setting = json_decode($mailer->settings);
                                                if ($mailer_setting && !in_array($mailer_setting->client_point_id, $settings)) {
                                                    $this->m_telegram_alerts->delete($mailer->telegram_alert_id);
                                                    $this->m_logger->log(
                                                        $this->tank_auth->get_user_id(),
                                                        'delete_telegram_alerts',
                                                        sprintf($this->lang->line('telegram_alerts_edit_success'),$user->cellphone),
                                                        array(
                                                            'client_id'=>$this->form_validation->set_value('client_id'),
                                                            'site_id'=>$this->form_validation->set_value('site_id'),
                                                            'telegram_alert_id'=>$mailer->telegram_alert_id,
                                                            'user_id'=>$mailer->user_id,
                                                            'report_id'=>$mailer->report_id,
                                                            'frequency'=>$mailer->frequency
                                                        ),
                                                        $this->form_validation->set_value('site_id')
                                                    );
                                                }
                                            }//foreach
                                        }//foreach
                                    } else {

                                        // what?
                                    }
                                    if($type=="site"){
                                        if($id!=0){
                                            $this->data['site_id']=$id;
                                        }

                                    }
                                    foreach($settings as $point_id) {
                                        $mailer_setting = array('client_point_id'=>$point_id);
                                       
                                        if ($this->m_telegram_alerts->create(
                                                $this->data['client_id'],
                                                $this->data['site_id'],
                                                $report->id,
                                                $this->form_validation->set_value('user_id'),
                                                5,
                                                0,
                                                0,
                                                0,
                                                $mailer_setting
                                                ) ) {
                                                $telegram_data[] = array(
                                                'report'=>$report,
                                                'frequency'=>5,
                                                'time'=>null,
                                                'day_of_week'=>null,
                                                'day_of_month'=>null
                                                );

                                        } else {
                                                $err = true;
                                                $this->data['message']['text'] = $this->lang->line('site_edit_error');
                                                $this->data['message']['type'] = 'error';
                                                break;
                                        }
                                        
                                    }

                                } else {

                                    $err = true;
                                    $this->data['message']['text'] = 'ERROR';
                                    $this->data['message']['type'] = 'error';
                                }
                            } else { //ELSE DELTE ALL
                                
                                $se = $this->m_telegram_alerts->get_by_site_user_id($this->form_validation->set_value('user_id'), $this->data['site_id'], $report->id, $this->data['client_id']);

                                if (is_array($se)) {

                                    foreach($se as $mailers) {
                                        foreach($mailers as $mailer) {
                                            //check if setting in system.
                                            $this->m_telegram_alerts->delete($mailer->telegram_alert_id);
                                            $this->m_logger->log(
                                                $this->tank_auth->get_user_id(),
                                                'delete_telegram_alerts',
                                                sprintf($this->lang->line('telegram_alerts_edit_success'),$user->cellphone),
                                                array(
                                                    'client_id'=>$this->form_validation->set_value('client_id'),
                                                    'site_id'=>$this->data['site_id'],//$this->form_validation->set_value('site_id'),
                                                    'telegram_alert_id'=>$mailer->telegram_alert_id,
                                                    'user_id'=>$mailer->user_id,
                                                    'report_id'=>$mailer->report_id,
                                                    'frequency'=>$mailer->frequency
                                                ),
                                                $this->form_validation->set_value('site_id')
                                            );
                                        }
                                    }
                                } else {
                                    // what?
                                }

                            }

                        /* ALL OTHER REPORTS */
                        } else {

                            if (isset($alerts[$report->id])) {
                                
                                if (isset($frequencies[$report->id])) {
                                    $f = $frequencies[$report->id];

                                    switch ($frequencies[$report->id]) {
                                        case 1: //daily
                                            $time = intval($daily_time[$report_id]) - $this->data['tz'];
                                            $day_of_week = 0;
                                            $day_of_month = 0;
                                            break;
                                        case 2: //weekly
                                            $time = intval($weekly_time[$report_id]) - $this->data['tz'];
                                            $day_of_week = intval($weekly_day[$report_id]);
                                            $day_of_month = 0;
                                            break;
                                        case 3: //monthly
                                            $time = intval($monthly_time[$report_id]) - $this->data['tz'];
                                            $day_of_week = 0;
                                            $day_of_month =  intval($monthly_day[$report_id]);
                                            break;
                                        case 6: //after_upload
                                        case 5: //alert
                                        case 4: //hourly
                                        default:
                                            $time = 0;
                                            $day_of_week = 0;
                                            $day_of_month = 0;
                                    }
                                    // echo $time.' '.$day_of_week.' '.$day_of_month.'<br/>';
                                } else {

                                    $f = 5;
                                    $time = 0;
                                    $day_of_week = 0;
                                    $day_of_month = 0;
                                }
                                if($type=="site"){
                                    if($id!=0){
                                        $this->data['site_id']=$id;
                                    }

                                }

                                if ($this->m_telegram_alerts->create(
                                        $this->data['client_id'],
                                        $this->data['site_id'],
                                        $report->id,
                                        $this->form_validation->set_value('user_id'),
                                        $f,
                                        $time,
                                        $day_of_week,
                                        $day_of_month
                                        ) ) {

                                    //log this shit
                                    $telegram_data[] = array(
                                        'report'=>$report->report,
                                        'frequency'=>intval($f),
                                        'time' => $time,
                                        'day_of_week' => $day_of_week,
                                        'day_of_month' => $day_of_month
                                        );
                                } else {
                                    $err = true;
                                    if ($this->m_telegram_alerts->error) {
                                        $this->data['errors']['email'] = $this->m_telegram_alerts->error;
                                    } else {
                                        $this->data['message']['text'] = $this->lang->line('site_edit_error');
                                        $this->data['message']['type'] = 'error';
                                    }
                                }

                            } else { //if alert
                                //delete entries.
                                $se = $this->m_telegram_alerts->get_by_site_user_id($this->form_validation->set_value('user_id'), $this->data['site_id'], $report->id, $this->data['client_id']);
                                if ($se) {
                                    foreach($se as $mailers) {
                                        foreach($mailers as $mailer) {
                                            $this->m_telegram_alerts->delete($mailer->telegram_alert_id);
                                                    $this->m_logger->log(
                                                    $this->tank_auth->get_user_id(),
                                                    'delete_telegram_alerts',
                                                    sprintf($this->lang->line('telegram_alerts_edit_success'),$user->cellphone),
                                                    array(
                                                        'client_id'=>$this->form_validation->set_value('client_id'),
                                                        'site_id'=>$this->form_validation->set_value('site_id'),
                                                        'telegram_alert_id'=>$mailer->telegram_alert_id,
                                                        'user_id'=>$mailer->user_id,
                                                        'report_id'=>$mailer->report_id,
                                                        'frequency'=>$mailer->frequency
                                                    ),
                                                    $this->form_validation->set_value('site_id')
                                            );
                                        }
                                    }
                                }//if se
                            } //elseifalert
                        }
                    }
                }

                if (!$err) {
                    $this->m_logger->log(
                        $this->tank_auth->get_user_id(),
                        'update_telegram_alerts',
                        sprintf($this->lang->line('telegram_alerts_edit_success'),$user->cellphone),
                        array(
                            'user_id'=>$this->form_validation->set_value('user_id'),
                            'site_id'=>$this->data['site_id'],
                            'telegrameses'=>isset($telegram_data) ? $telegram_data : '',
                            ),
                        $this->data['site_id'],
                        $this->data['client_id']
                        );

                    $this->session->set_flashdata('message', sprintf($this->lang->line('telegram_alerts_edit_success'),$user->cellphone));
                    $this->session->set_flashdata('message_type', 'notice');
                    if ($type == 'client') {
                        redirect('clients/view/'.$this->data['client_id']."/#telegram");
                    } elseif($type="site" && $id == 0) {
                        redirect('clients/view/'.$this->data['client_id']."/#telegram");
                    }else{
                        redirect('sites/view/'.$id."/#telegram");
                    }
                } else {
                    // echo "ERROR";
                    // print_r( $this->data['errors']);
                }
            }
        }
        
        $this->data['current_user'] = $this->users->get_user($this->tank_auth->get_user_id());
        $this->data['tz'] = timezones($this->tank_auth->get_user_timezone());
      
        if ($type == 'client') {
          
            if ($this->data['user_id'] > 0) {
               
                $this->data['telegram_details'] = $this->m_telegram_alerts->get_by_site_user_id($this->data['user_id'], 0, 0, $this->data['client_id']);
            }

            $this->data['alerts'] = $this->m_telegram_alerts->get_list_by_client($this->data['client_id']);
            
            //get a list of users that have access to that site.
            //$this->data['users'] = $this->users->get_list_by_client($this->data['client_id']);
            $this->data['users'] = $this->m_telegram_accounts->get_list_by_client($this->data['client_id']);
            $this->layout->setLayout('layouts/clients');
            $this->data['nav'] = 'client';
        } else {
          
            if ($this->data['user_id'] > 0) {


                if($id == 0){
              
                    $this->data['telegram_details'] = $this->m_telegram_alerts->get_by_site_user_id($this->data['user_id'], null,0,$this->data['client_id']);
                }else{
                   
                    
                    
                $this->data['telegram_details'] = $this->m_telegram_alerts->get_by_site_user_id($this->data['user_id'], $this->data['site_id']);                    
               
            }
                
            }
       

            $this->data['alerts'] = $this->m_telegram_alerts->get_list_by_site($this->data['site_id']);
            //get a list of users that have access to that site.
            //$this->data['users'] = $this->users->get_list_by_site($this->data['site_id']);
            $this->data['users'] = $this->m_telegram_accounts->get_list_by_client($this->data['client_id']);
            //$this->data['users'] = $this->m_telegram_accounts->get_all_by_client($this->data['client_id']);
           
            $this->layout->setLayout('layouts/sites');
            $this->data['nav'] = 'site';
        }


        // $this->data['report_types'] = $this->m_report_type->get_array();
        // $this->data['report_grouped_types'] = $this->m_report_type->get_alerts_grouped_array();

        $this->layout->view('telegram_alerts', $this->data);
    }

    /**
     * remove_alert
     *
     * @param int $site_id
     * @param int $alert_id
     *
     * @access public
     *
     * @return void
     */
    function remove_alert($type, $id, $alert_id) {
        if ($type == 'client') {
            $client_id = $id;
            $site_id = 0;
        } else {
            $site_id = $id;
            $site = $this->m_site->get_by_id($site_id);
            $client_id = $site->client_id;
        }
        check_permissions(array(
            'client_id'=>$client_id,
            'acl'=>'manage_telegram_alerts',
            ));

        $se = $this->m_telegram_alerts->get_by_id($alert_id);
        $this->m_telegram_alerts->delete($alert_id);
        $this->m_logger->log(
            $this->tank_auth->get_user_id(),
            'delete_telegram_alert',
            $this->lang->line('automated_emails_remove_success'),
            array(
                'site_id'=>$site_id,
                'client_id'=>$client_id,
                'alert_id'=>$alert_id,
                'report_id'=>$se->report_id,
                'frequency'=>$se->frequency,
                ),
            $site_id,
            $client_id
            );

        $this->session->set_flashdata('message', $this->lang->line('telegram_alert_remove_success'));
        $this->session->set_flashdata('message_type', 'notice');
        if ($type == 'client') {
            redirect('/clients/view/'.$client_id.'#telegram');
        } else {
            redirect('/sites/view/'.$site_id.'#telegram');
        }
    }

    function list_site_widget($site_id) {
        if (! isset($this->data['acl']['view_telegram_alerts'])) return; // Don't show thingy.

        // need a client id..
        $site = $this->m_site->get_by_id($site_id);

        $cp = check_permissions(array(
            'client_id'=>$site->client_id,
            'redirect'=>FALSE,
            ));
        if (is_array($cp)) {
            $this->data['widget_error'] = show_message_box($cp['type'], $cp['message'], TRUE);
        } else {
            $this->data['telegram_setup'] = true;//$this->m_telegram_accounts->get_by_client($site->client_id);
            $this->data['client_id'] = $site->client_id;

            //if ($this->data['telegram_setup']) {
                $this->data['alerts'] = $this->m_telegram_alerts->get_list_by_site($site_id);
            //}
            $this->data['site_id'] = $site_id;
        }

        $this->load->view('list_site_widget', $this->data);
    }

    /**
     * list_client_widget
     *
     * @param int $client_id
     *
     * @access public
     *
     * @return void
     */
    function list_client_widget($client_id)
    {
        if (! isset($this->data['acl']['view_client_telegram_detail'])) {
            return;
        } // Don't show thingy.

        $cp = check_permissions(array(
            'client_id'=>$client_id,
            'redirect'=>false,
            ));
        if (is_array($cp)) {
            $this->data['widget_error'] = show_message_box($cp['type'], $cp['message'], true);
        } else {
            $this->data['telegram_default_limit'] = $this->m_settings->get_by_key('telegram_default_limit');

            $this->data['show_add'] = true;
            $this->data['show_delete'] = true;

            $this->load->model('accounts/m_accounts');
            $account = $this->m_accounts->get_client_account($client_id);

            $this->data['telegram_setup'] = true;//$this->m_telegram_accounts->get_by_client($client_id);
            
            if ($this->data['telegram_setup']) {
                $this->data['start_date'] = mktime(0, 0, 0, date('m'), 1, date('Y'));
                $this->data['end_date'] = time();


              
                $this->data['show_delete'] = false;
                

                //$this->data['telegram_log'] = $this->m_telegram_credit_log->get_list($this->data['telegram_setup']->telegram_account_id, $this->data['start_date'], $this->data['end_date'], 5, 0);

             
                $this->data['alerts'] = $this->m_telegram_alerts->get_list_by_client($client_id, null, true);
            }
        }
    

        $this->data['client_id'] = $client_id;
        $this->load->view('list_client_widget', $this->data);
    
    }

    /**
     * setup
     *
     * @param int $client_id
     * @param int   $distributor_id
     *
     * @access public
     *
     * @return void
     */
    function setup($client_id, $distributor_id=0) {
        check_permissions(array(
            'client_id'=>$client_id,
            'distributor_id'=>$distributor_id,
            'acl'=>'edit_telegram_account'
            ));

        $this->_get_accounts_data();
      
         //The next line would probably just happen If you're a super user :)
        if(!isset($this->data['client_accounts'])) $this->data['client_accounts'] = $this->m_telegram_accounts->get_list_by_distributor($distributor_id, true, false);

        $this->data['telegram_default_limit'] = $this->m_settings->get_by_key('telegram_default_limit');

        $this->load->model('accounts/m_accounts');

        if (!$client_id && $distributor_id) {
            $this->data['telegram_setup'] = $this->m_telegram_accounts->get_by_distributor($distributor_id);
            $this->data['telegram_setup'] = $this->m_telegram_accounts->get_by_client($client_id);
            $this->data['account'] = $this->m_accounts->get_distributor_account($distributor_id);
        } else {
            $this->data['telegram_setup'] = $this->m_telegram_accounts->get_by_client($client_id);
            //$this->data['account'] = $this->m_accounts->get_client_account($client_id);
        }
        $this->data['telegram_senders'] = $this->m_telegram_sender->get_list();

        //if ($this->data['telegram_setup']) $this->data['telegram_account_id'] = $this->data['telegram_setup']->telegram_account_id;

         // $this->form_validation->set_rules('telegram_limit', 'telegram limit', 'trim|xss_clean|require|integer');
     
            if($userID=$this->input->post('user_id')){
                $client = $this->m_client->get_by_id($client_id);
                $distributor_id = $client->distributor_id;
                $telegram_account_id = $this->m_telegram_accounts->create(
                $distributor_id,
                $client_id,
                $userID);
                if( $telegram_account_id>0){
                    $this->session->set_flashdata('message', $this->lang->line('telegram_client_setup_success'));
                    $this->session->set_flashdata('message_type', 'notice');
                    redirect('telegram/setup/'.$client_id);
                }
                
    
    
           }
        

        $this->data['client_id'] = $client_id;
        $this->data['distributor_id'] = $distributor_id;
        $this->data['nav'] = 'accounts';
        $this->layout->setLayout('accounts/layout');
        $this->layout->view('client_setup', $this->data);
    }

    /**
    *  Add telegram credits to an account....
    *
    * Prepaid accounts:
    * If the account is a client account we need to deduct from the master.
    * If we're master... only super is adding credits!
    *
    * Credit accounts:
    * Odin help us.
    *
    * @param int $telegram_account_id
    */
    function add_credit($telegram_account_id) {

        $telegram_account = $this->m_telegram_accounts->get_by_id($telegram_account_id);
        check_permissions(array(
            'client_id'=>$telegram_account->client_id,
            'distributor_id'=>$telegram_account->distributor_id,
            'acl'=>'add_telegram_credits'
            ));

        $this->_get_accounts_data();
        //The next line would probably just happen If you're a super user :)
        if(!isset($this->data['client_accounts'])) $this->data['client_accounts'] = $this->m_telegram_accounts->get_list_by_distributor($telegram_account->distributor_id, true, false);

        $this->data['telegram_account'] = $telegram_account;
        $this->data['telegram_account_id'] = $telegram_account->telegram_account_id;

        $this->data['action'] = 'add';

        $this->load->helper('accounts/accounts');
        $this->load->model('accounts/m_accounts');
        $this->load->model('accounts/m_accounts_log');

        //CLIENT
        $this->data['account'] = $this->m_accounts->get_client_account($telegram_account->client_id);
        $this->data['client_id'] = $this->data['account']->client_id;
        $this->data['distributor_id'] = $this->data['account']->distributor_id;

        $data['telegram_cost'] = $this->m_settings->get_by_key('telegram_credit_cost')->value;

        $this->form_validation->set_rules('credits', 'Credits', 'trim|xss_clean|require|numeric');
        if ($this->form_validation->run() ) {

            if ($this->data['account']->credits < $this->form_validation->set_value('credits')) {
                $ERROR = "There is not enough credit on the master account to complete this action.";
            } else {
                 // Add credits to client telegram account
                $telegram_credits_bought = $data['telegram_cost'] * $this->form_validation->set_value('credits');

                $this->m_telegram_accounts->add_credits($telegram_account_id, $telegram_credits_bought);
                $this->m_telegram_credit_log->log_credits($telegram_account_id, $this->tank_auth->get_user_id(), $telegram_credits_bought, 0);

                // remove account credits
                $this->m_accounts->update_credits($telegram_account->distributor_id, $telegram_account->client_id, -$this->form_validation->set_value('credits'));
                $description = 'Credits used to buy '.$telegram_credits_bought.' telegrames.';
                $this->m_accounts_log->create($telegram_account->distributor_id, $telegram_account->client_id, $this->tank_auth->get_user_id(), -$this->form_validation->set_value('credits'), $description);

                // Proof of purchase...
                $this->load->helper('email');

                $user = $this->users->get_user( $this->tank_auth->get_user_id() );
                if ($user !== null && can_email_user($user)) {
                    // Email proof of purchase to user.
                    $subject = sprintf($this->lang->line('telegram_proof_of_purchase_subject'), $this->data['account']->name);

                    $data['site_name']  = $this->config->item('website_name', 'tank_auth');
                    $data['account_name'] = $this->data['account']->name;
                    $data['telegram_bought'] = $telegram_credits_bought;
                    $data['cost'] = $this->form_validation->set_value('credits');

                    $message = $this->load->view( 'telegram/email/accounts_credits_added_statement_email_html', $data, TRUE);
                    $alt_message = $this->load->view( 'telegram/email/accounts_credits_added_statement_email_txt', $data, TRUE);

                    if (sendMail($user->email, $subject, $message, $alt_message)) {
                       // echo "\nEMAIL SENT\n"."<br/>";
                    } else {
                        // echo "EMAIL FAILED\n"."<br/>";
                    }
                }

            }

            if (isset($ERROR)) {
                $this->session->set_flashdata('message', $ERROR);
                $this->session->set_flashdata('message_type', 'error');
            } else {
                $this->session->set_flashdata('message', $this->lang->line('telegram_add_credits_success'));
                $this->session->set_flashdata('message_type', 'notice');
            }

            if ($telegram_account->client_id) {
                redirect('accounts/client_view/'.$telegram_account->client_id.'#telegram');
            } else {
                if ($this->tank_auth->get_user_role() == 'distrib') {
                    redirect('accounts');
                } else {
                    redirect('accounts/distributor_view/'.$telegram_account->distributor_id.'#telegram');
                }
            }
        }

        $max_credit = $this->data['account']->credits;

        $js = '
        var telegram_exchange='.$data['telegram_cost'].';
        $( "#credit-slider" ).slider({
            min: 0,
            max: '.$max_credit.',
            step: 1,
            slide: function( event, ui ) {
                $("#credit-amount").html( ui.value * telegram_exchange );
                $("#credits").val( ui.value );
                $("#credits-used").text( ui.value );
            }
        });';
        $this->pixie_javascript->append_onload($js);

        $this->layout->setLayout('accounts/layout');
        $this->layout->view('client_add_credit', $this->data);
    }

    /*
    function remove_credit($telegram_account_id) {

        $telegram_account = $this->m_telegram_accounts->get_by_id($telegram_account_id);
        check_permissions(array(
            'client_id'=>$telegram_account->client_id,
            'distributor_id'=>$telegram_account->distributor_id,
            'acl'=>'remove_telegram_credits'
            ));

        $this->_get_accounts_data();
        //The next line would probably just happen If you're a super user :)
        if(!isset($this->data['client_accounts'])) $this->data['client_accounts'] = $this->m_telegram_accounts->get_list_by_distributor($telegram_account->distributor_id, true, false);

        $this->data['action'] = 'remove';

        $this->data['telegram_account'] = $telegram_account;
        $this->data['telegram_account_id'] = $telegram_account->telegram_account_id;
        $data['site_name'] = $this->config->item('website_name', 'tank_auth');
        $this->load->model('accounts/m_accounts');
        // $this->load->model('accounts/m_accounts_credits');

                $this->data['client_id'] = $this->data['account']->client_id;
        $this->data['distributor_id'] = $this->data['account']->distributor_id;

        if (!$telegram_account->client_id) {
            $type = 'distributor';
            //DISTRIBUTOR!
            $account = $this->m_accounts->get_distributor_account($telegram_account->distributor_id);
        } else {
            $type = 'client';
            //CLIENT
            $account = $this->m_accounts->get_client_account($telegram_account->client_id);
            $master_account = $this->m_accounts->get_distributor_account($telegram_account->distributor_id);
            $master_telegram_account = $this->m_telegram_accounts->get_by_distributor($telegram_account->distributor_id);
        }

        $this->form_validation->set_rules('credits', 'Credits', 'trim|xss_clean|require');

        if ($this->form_validation->run() ) {
            if ($type == 'client') {
                // Add credits to client account
                $this->m_telegram_accounts->add_credits($telegram_account_id, -$this->form_validation->set_value('credits'));
                $this->m_telegram_credit_log->log_credits($telegram_account_id, $this->tank_auth->get_user_id(), -$this->form_validation->set_value('credits'), $master_telegram_account->telegram_account_id);
                if ($this->tank_auth->get_user_role() != 'super') {
                    // Remove credits from distributor account
                    $this->m_telegram_accounts->add_credits($master_telegram_account->telegram_account_id, $this->form_validation->set_value('credits'));
                    $this->m_telegram_credit_log->log_credits($master_telegram_account->telegram_account_id, $this->tank_auth->get_user_id(), $this->form_validation->set_value('credits'), $telegram_account_id);
                }
            } else {
                // Add credits to distributor account
                $this->m_telegram_accounts->add_credits($telegram_account_id, -$this->form_validation->set_value('credits'));
                $this->m_telegram_credit_log->log_credits($telegram_account_id, $this->tank_auth->get_user_id(), -$this->form_validation->set_value('credits'));
            }

            if (isset($ERROR)) {
                $this->session->set_flashdata('message', $ERROR);
                $this->session->set_flashdata('message_type', 'error');
            } else {
                $this->session->set_flashdata('message', $this->lang->line('telegram_remove_credits_success'));
                $this->session->set_flashdata('message_type', 'notice');
            }
             $data['credits_removed'] = $this->form_validation->set_value('credits');
             $message = $this->load->view('email/accounts_credits_remove_statement_email_html', $data, TRUE);
             $emails = $this->m_telegram_emails->get_by_telegram_account($telegram_account_id);
                        foreach ($emails as $send_emails) {
                          sendMail($send_emails->email , 'telegram Credits Removed', $message , '');

                        }
            redirect('telegram/detail/'.$telegram_account->telegram_account_id);
        }

    // We can only take away what we have.
        $max_credit = $telegram_account->credits;
        $js = '
        $( "#credit-slider" ).slider({
            min: 1,
            max: '.$max_credit.',
            step: 1,
            slide: function( event, ui ) {
                $("#credit-amount").html( ui.value );
                $("#credits").val( ui.value );
            }
        });';
        $this->pixie_javascript->append_onload($js);

        $this->layout->setLayout('layout');
        $this->layout->view('client_add_credit', $this->data);
    }
    */

    /**
     * accounts_widget
     *
     * @param int $distributor_id
     * @param int $client_id
     *
     * @access public
     *
     * @return void
     */
    function accounts_widget($distributor_id, $client_id) {
        if ($client_id == 0) {
            $acl = 'view_distributor_telegram_detail';
        } else {
            $acl = 'view_client_telegram_detail';
        }

        $cp = check_permissions(array(
            'client_id'=>$client_id,
            'distributor_id'=>$distributor_id,
            'acl'=>$acl,
            'redirect'=>FALSE,
            ));
        if (is_array($cp)) {
            $this->data['widget_error'] = show_message_box($cp['type'], $cp['message'], TRUE);
        } else {
            $this->data['show_add'] = true;
            $this->data['show_delete'] = true;

            $this->data['telegram_default_limit'] = $this->m_settings->get_by_key('telegram_default_limit');

            $this->data['start_date'] = mktime(0, 0, 0, date('m'), 1, date('Y'));
            $this->data['end_date'] = time();

            if ($client_id) {
                $this->data['telegram_setup'] = true;//$this->m_telegram_accounts->get_by_client($client_id);
                $account = $this->m_accounts->get_client_account($client_id);

                if ((isset($this->data['acl']['add_telegram_credits']) && $account->credits)) {
                    $this->data['show_add'] = true;
                } else {
                    $this->data['show_add'] = false;
                }
                if (isset($this->data['acl']['remove_telegram_credits']) && $this->data['telegram_setup'] && $this->data['telegram_setup']->credits) {
                    $this->data['show_delete'] = true;
                } else {
                    $this->data['show_delete'] = false;
                }

                if ($this->data['telegram_setup']) {
                    $this->data['telegram_log'] = $this->m_telegram_credit_log->get_list($this->data['telegram_setup']->telegram_account_id,  $this->data['start_date'], $this->data['end_date'], 5, 0);
                }

                $this->data['type']='client';
            } else { //distributor account...
                $this->data['telegram_setup'] = $this->m_telegram_accounts->get_by_distributor($distributor_id);
                if ($this->tank_auth->get_user_role() != 'super') $this->data['show_add'] =  $this->data['show_delete'] = false;
                $this->data['type']='master';

                if ($this->data['telegram_setup']) {
                    $this->data['telegram_log'] = $this->m_telegram_credit_log->get_distributor_list($distributor_id, $this->data['start_date'], $this->data['end_date'], 5, 0);
                }
            }

            if ($this->data['telegram_setup']) {
                if (!$this->data['telegram_setup']->credits) $this->data['show_delete'] = false;
            }

            $this->data['client_id'] = $client_id;
            $this->data['distributor_id'] = $distributor_id;

            $this->data['current_client'] = $this->m_client->get_by_id($client_id);


            $this->load->view('account_widget', $this->data);
        }
    }

    /**
     * accounts_dash
     *
     * @access public
     *
     * @return void
     */
    function accounts_dash() {
        $start = mktime(0, 0, 0, date('m'), 1, date('Y'));
        $end = time();

        $this->data['total'] =  abs($this->m_telegram_credit_log->get_total_telegram_sent($start, $end));
        $this->data['by_distributor'] = $this->m_telegram_credit_log->get_total_telegram_sent_distributors($start, $end);

        $this->load->view('accounts_dashboard', $this->data);
    }

    /**
     * manage_module_widget
     *
     * @access public
     *
     * @return void
     */
    function manage_module_widget() {
        $cp = check_permissions(array(
                'allowed'=> array('super'),
                'redirect'=>FALSE
                ));
        if (is_array($cp)) {
            $this->data['widget_error'] = show_message_box($cp['type'], $cp['message'], TRUE);
        } else {
            $this->data['telegram_credit_cost'] = $this->m_settings->get_by_key('telegram_credit_cost');
        }
        $this->load->view('settings_widget', $this->data);
    }

    /**
     * settings_widget
     *
     * @access public
     *
     * @return void
     */
    function settings_widget() {
        $cp = check_permissions(array(
                'allowed'=> array('super'),
                'redirect'=>FALSE
                ));
        if (is_array($cp)) {
            $this->data['widget_error'] = show_message_box($cp['type'], $cp['message'], TRUE);
        } else {
            $this->data['telegram_senders'] = $this->m_telegram_sender->get_list();
            $this->data['telegram_templates'] = $this->m_telegram_reports->get_list();

            // $this->data['telegram_default_limit'] = $this->m_settings->get_by_key('telegram_default_limit');
            $this->data['telegram_credit_cost'] = $this->m_settings->get_by_key('telegram_credit_cost');
            $this->data['google_url_api'] = $this->m_settings->get_by_key('google_url_api');

            $this->load->model('m_report_type');
            $this->data['reports'] = $this->m_report_type->get_array();
        }
        $this->load->view('settings_widget', $this->data);
    }

    /**
     * save_settings
     *
     * @access public
     *
     * @return void
     */
    function save_settings() {
        check_permissions(array('allowed'=>array('super')));
        $this->form_validation->set_rules('telegram_credit_cost', 'telegrams per account credit', 'trim|xss_clean|require|integer');
        $this->form_validation->set_rules('google_url_api', 'Google URL Shortner API key', 'trim|xss_clean');
        if ($this->form_validation->run()) {
            // $this->m_settings->set_key('telegram_default_limit', $this->form_validation->set_value('telegram_default_limit'), 'telegram');
            $this->m_settings->set_key('telegram_credit_cost', $this->form_validation->set_value('telegram_credit_cost'), 'telegram');
            $this->m_settings->set_key('google_url_api', $this->form_validation->set_value('google_url_api'), 'telegram');

            $this->m_logger->log(
                $this->tank_auth->get_user_id(),
                'settings_saved',
                'Settings updated.',
                array('group'=>'telegram')
                );
        }

        $this->data['referrer'] = $this->session->flashdata('referrer').'#telegram';

        $this->session->set_flashdata('message', 'telegram settings have been updated.');
        $this->session->set_flashdata('message_type', 'notice');
        redirect($this->data['referrer']);
    }

    /**
     * telegram_sender
     *
     * @param int $id
     *
     * @access public
     *
     * @return void
     */
    function telegram_sender($id=0) {
        check_permissions(array('allowed'=>array('super')));
        $this->data['title'] = $this->lang->line('settings_telegram_add_account');
        if ($id) {
            $this->data['telegram_sender'] = $this->m_telegram_sender->get_by_id($id);
            $this->data['title'] = $this->lang->line('settings_telegram_edit_account');
        }
        $this->form_validation->set_rules('name', $this->lang->line('settings_bot_name'), 'trim|xss_clean|require');
         $this->form_validation->set_rules('key', $this->lang->line('settings_telegram_key'), 'trim|xss_clean|require');
         if ($this->form_validation->run() ) {
            if (isset($this->data['telegram_sender'])) {
                $this->m_telegram_sender->update($this->data['telegram_sender']->telegram_sender_id, $this->form_validation->set_value('name'), $this->form_validation->set_value('key'));
                $this->session->set_flashdata('message', $this->lang->line('settings_telegram_edit_account_success'));
                $type = 'telegram_sender_edit';
                $message = $this->lang->line('settings_telegram_edit_account_success');
            } else {
                $this->m_telegram_sender->create($this->form_validation->set_value('name'), $this->form_validation->set_value('key'));
                $this->session->set_flashdata('message', $this->lang->line('settings_telegram_add_account_success'));
                $type = 'telegram_sender_add';
                $message = $this->lang->line('settings_telegram_add_account_success');
            }
            $this->m_logger->log(
                $this->tank_auth->get_user_id(),
                $type,
                $message,
                array(
                    'telegram_sender_id'=>$id,
                    'name'=>$this->form_validation->set_value('name'),
                    'key'=>$this->form_validation->set_value('key')
                  
                    ),
                null,
                null
                );

            $this->session->set_flashdata('message_type', 'notice');
            redirect('profile');
        }
        $this->data['nav'] = 'settings';
        $this->layout->view('telegram_sender_form', $this->data);
    }

    /**
     * test
     *
     * @param int $sender_id
     *
     * @access public
     *
     * @return void
     */
    function test($chat_id, $msg=null) {
       
        check_permissions(array('allowed'=>array('super')));
             $this->load->library('telegram/telegram_sender');
      

            try {
                $delivery_response = $this->telegram_sender->send_message(
                    $chat_id,
                    $msg
                    );
            //                        $delivery_response = '<api_result>
            //        <send_info>
            //         <eventid>xxxxxxxx</eventid>
            //        </send_info>
            //        <call_result>
            //         <result>True</result>
            //         <error />
            //        </call_result>
            //        </api_result>';


                $status = 'okay';
                $this->session->set_flashdata('message', $this->lang->line('settings_telegram_test_success'));
                $this->session->set_flashdata('message_type', 'notice');

                $this->m_logger->log(
                    $this->tank_auth->get_user_id(),
                    'telegram_test_success',
                    $this->lang->line('settings_telegram_test_success'),
                    array(
                        'response'=>$delivery_response,
                        )
                    );

            } catch(Exception $e) {
                $status = 'error';
                $delivery_response = $e->getMessage();


                $this->session->set_flashdata('message', $this->lang->line('settings_telegram_test_fail').": ".$delivery_response);
                $this->session->set_flashdata('message_type', 'error');

                $this->m_logger->log(
                    $this->tank_auth->get_user_id(),
                    'telegram_test_fail',
                    $this->lang->line('settings_telegram_test_fail'),
                    array(
                        'response'=>$delivery_response,
                        )
                    );

            }
            redirect('profile');
        
    

        $this->data['nav'] = 'settings';
        $this->layout->view('telegram_test_form', $this->data);
    }


    
    
    
    
    /**
     * test
     *
     * @param int $sender_id
     *
     * @access public
     *
     * @return void
     */
    function test_message($user_id=null, $msg=null) {
             $this->load->library('telegram/telegram_sender');
          
             $chat_id =$this->m_telegram_accounts->get_chat_id($user_id);
                $msg=urldecode($msg);
            try {
                $delivery_response = $this->telegram_sender->send_message(
                    $chat_id,
                    $msg
                    );
            //                        $delivery_response = '<api_result>
            //        <send_info>
            //         <eventid>xxxxxxxx</eventid>
            //        </send_info>
            //        <call_result>
            //         <result>True</result>
            //         <error />
            //        </call_result>
            //        </api_result>';


                $status = 'okay';
                $this->session->set_flashdata('message', $this->lang->line('settings_telegram_test_success'));
                $this->session->set_flashdata('message_type', 'notice');

                $this->m_logger->log(
                    $this->tank_auth->get_user_id(),
                    'telegram_test_success',
                    $this->lang->line('settings_telegram_test_success'),
                    array(
                        'response'=>$delivery_response,
                        )
                    );

            } catch(Exception $e) {
                $status = 'error';
                $delivery_response = $e->getMessage();


                $this->session->set_flashdata('message', $this->lang->line('settings_telegram_test_fail').": ".$delivery_response);
                $this->session->set_flashdata('message_type', 'error');

                $this->m_logger->log(
                    $this->tank_auth->get_user_id(),
                    'telegram_test_fail',
                    $this->lang->line('settings_telegram_test_fail'),
                    array(
                        'response'=>$delivery_response,
                        )
                    );

            }
            redirect('profile');
        
    

        $this->data['nav'] = 'settings';
        $this->layout->view('telegram_test_form', $this->data);
    }

/**
     * test
     *
     * @param int $sender_id
     *
     * @access public
     *
     * @return void
     */
    function send_message($user_id=null, $msg=null) {
             $this->load->library('telegram/telegram_sender');
          
             $chat_id =$this->m_telegram_accounts->get_chat_id($user_id);

            try {
                $delivery_response = $this->telegram_sender->send_message(
                    $chat_id,
                    $msg
                    );
            //                        $delivery_response = '<api_result>
            //        <send_info>
            //         <eventid>xxxxxxxx</eventid>
            //        </send_info>
            //        <call_result>
            //         <result>True</result>
            //         <error />
            //        </call_result>
            //        </api_result>';


                $status = 'okay';
                $this->session->set_flashdata('message', $this->lang->line('settings_telegram_test_success'));
                $this->session->set_flashdata('message_type', 'notice');

                $this->m_logger->log(
                    $this->tank_auth->get_user_id(),
                    'telegram_test_success',
                    $this->lang->line('settings_telegram_test_success'),
                    array(
                        'response'=>$delivery_response,
                        )
                    );

            } catch(Exception $e) {
                $status = 'error';
                $delivery_response = $e->getMessage();


                $this->session->set_flashdata('message', $this->lang->line('settings_telegram_test_fail').": ".$delivery_response);
                $this->session->set_flashdata('message_type', 'error');

                $this->m_logger->log(
                    $this->tank_auth->get_user_id(),
                    'telegram_test_fail',
                    $this->lang->line('settings_telegram_test_fail'),
                    array(
                        'response'=>$delivery_response,
                        )
                    );

            }
            redirect('settings');
        
    

        $this->data['nav'] = 'settings';
        $this->layout->view('telegram_test_form', $this->data);
    }

    /**
     * template_setup
     *
     * @param int $report_id
     *
     * @access public
     *
     * @return void
     */
    function template_setup($report_id) {
        check_permissions(array('allowed'=>array('super')));
        $this->data = array();
        if ($report_id) {
            $this->load->model('m_report_type');

            $this->data['telegram_template'] = $this->m_telegram_reports->get_by_report($report_id);
            $this->data['report_name'] = $this->m_report_type->get_display_name($report_id);
            //                $this->data['telegram_template'] = $this->m_telegram_reports->get_by_id($template_id);

            $this->form_validation->set_rules('template', $this->lang->line('settings_telegram_template'), 'trim|xss_clean|require');
            if ($this->form_validation->run() ) {
                if ($this->data['telegram_template']) {
                    $this->m_telegram_reports->update(
                        $this->data['telegram_template']->telegram_template_id,
                        $report_id,
                        $this->form_validation->set_value('template')
                        );
                    $telegram_template_id = $this->data['telegram_template']->telegram_template_id;
                } else {
                    //create one
                    $telegram_template_id = $this->m_telegram_reports->create(
                        $report_id,
                        $this->form_validation->set_value('template')
                        );
                }

                $this->m_logger->log(
                    $this->tank_auth->get_user_id(),
                    'telegram_template_setup',
                    $this->lang->line('settings_telegram_edit_template_success'),
                    array(
                        'template_id'=>$telegram_template_id,
                        'report_id'=>$report_id,
                        'text'=>$this->form_validation->set_value('template')
                        )
                    );

                $this->session->set_flashdata('message', $this->lang->line('settings_telegram_edit_template_success'));
                $this->session->set_flashdata('message_type', 'notice');
                redirect('settings/#telegram');
            }
            $this->data['nav'] = 'settings';
            $this->layout->view('telegram_template_form', $this->data);
        } else {
            redirect('settings/#telegram');
        }
    }

    /**
     * remove_telegram_sender
     *
     * @param int $id
     *
     * @access public
     *
     * @return void
     */
    function remove_telegram_sender($id) {
        check_permissions(array('allowed'=>array('super')));
        $this->m_telegram_sender->delete($id);

        $this->m_logger->log(
            $this->tank_auth->get_user_id(),
            'telegram_sender_delete',
            $this->lang->line('settings_telegram_delete_account_success'),
            array(
                'telegram_sender_id'=>$id,
                )
            );

        $this->session->set_flashdata('message', $this->lang->line('settings_telegram_delete_account_success'));
        $this->session->set_flashdata('message_type', 'notice');
        redirect('settings');
    }

    /**
     * site_telegram_config_prompt
     *
     * Button displayed in sites tab to allow telegram setup...
     *
     * @param int $site_id
     * @param str $site_code
     *
     * @access public
     *
     * @return void
     */
    function site_telegram_config_prompt($site_id, $site_code) {

      
        if (isset($this->data['acl']['site_config_telegram'])) {
            $this->data['telegram_active'] = check_site_active('telegram', $site_id);
            $this->data['site_code'] = $site_code;
            if (!$this->data['telegram_active']) {
                $this->data['telegram_senders'] = $this->m_telegram_sender->get_list();
            }
            $handle = create_handle($site_code);
        // Prompt to setup with telegram
            //echo '( <a href="#" rel="#telegram-setup-prompt_'.$site_id.'_'.$handle.'" class="confirm">Setup via telegram</a> )';
            echo '<div id="telegram-setup-prompt_'.$site_id.'_'.$handle.'" class="overlay-big"> <div class="overlay-content">';
            $this->load->view('telegram/telegram_site_config_form', $this->data);
            echo '</div></div>';
        }
    }

    /**

    */

    /**
    * site_telegram_config
    *
    * Function that will use Magcell telegram configuration.
    *
    * telegram format:
    * ---------------
    *
    *      Magcell:ID:*URL:*Username:*Password:*APN:*DHCP:*Port:*IA:
    *
    *  Magcell is case sensitive.
    *  ID is the 6 digit Magcell ID.
    *
    *  Any filed which starts with an * will remain unchanged,
    *  any fields which are blank will be deleted, otherwise
    *  whatever is typed in the field will be stored as the new configuration.
    *
    * @access public
    *
    * @return mixed Value.
    */
    function site_telegram_config() {
        if (!$this->input->post('site_id')) {
            redirect('sites');
        }
        $site = $this->m_site->get_by_id($this->input->post('site_id'));
        check_permissions(array(
            'client'=>$site->client_id,
            'acl'=>'site_config_telegram'
        )); // only God can do this for now. Okay so maybe not just Dog

        $telegram_active = check_site_active('telegram', $this->input->post('site_id'));

        $errors = array();

        if (!empty($_POST)) { // Telephone number check
            if ($this->input->post('magcell_telephone') && !is_valid_phone($this->input->post('magcell_telephone'))) {
                $errors['magcell_telephone'] = $this->lang->line('auth_invalid_cellphone');
            }

            // all the other wonderful form checking ness
            $this->form_validation->set_rules('magcell_telephone', 'Cellphone Number', 'trim|required|xss_clean');
            $this->form_validation->set_rules('site_id', 'Site ID', 'trim|required|xss_clean');
            $this->form_validation->set_rules('magcell_code', 'Magcell Code', 'trim|xss_clean');
            if ($this->tank_auth->get_user_role() == 'super') {
                // Yes it might be limited to God now, but it might change in future,
                // no need to do more code when we change this way..
                $this->form_validation->set_rules('magcell_username', 'Username', 'trim|xss_clean');
                $this->form_validation->set_rules('magcell_password', 'Password', 'trim|xss_clean');
                $this->form_validation->set_rules('magcell_apn', 'APN', 'trim|xss_clean');
                $this->form_validation->set_rules('magcell_dhcp', 'DHCP', 'trim|xss_clean');
                $this->form_validation->set_rules('magcell_port', 'Port', 'trim|xss_clean');
                $this->form_validation->set_rules('magcell_path', 'Path', 'trim|xss_clean');
                $this->form_validation->set_rules('magcell_ia', 'ia', 'trim|xss_clean');
            }

            if ($this->form_validation->run() && empty($this->data['errors'])) {
                // Okay so we have all the data we need to send the setup telegram
                $this->load->library('telegram/telegram_sender');

                $cellphone_number = $this->form_validation->set_value('magcell_telephone');

                if ($telegram_active) {
                    $telegram_sender = $this->m_telegram_sender->get_by_client($site->client_id);
                } else {
                    $telegram_sender = $this->m_telegram_sender->get_by_id($this->input->post('telegram_sender'));
                }

                if ($telegram_sender) {
                //Build message:
                // Magcell:ID:*URL:*Username:*Password:*APN:*DHCP:*Port:*Path:*IA:

                    $new_url = str_replace('http://', '', site_url());
                    if(substr($new_url, -1) == '/') { // remove trailing slash.
                        $new_url = substr($new_url, 0, -1);
                    }

                    if ($this->tank_auth->get_user_role() == 'super') {
                        $msg = 'Magcell:'.$this->form_validation->set_value('magcell_code').':'.$new_url.':';
                        $msg .= ($this->form_validation->set_value('magcell_username') ? $this->form_validation->set_value('magcell_username') : '');
                        $msg .= ':';
                        $msg .= ($this->form_validation->set_value('magcell_password') ? $this->form_validation->set_value('magcell_password') : '');
                        $msg .= ':';
                        $msg .= ($this->form_validation->set_value('magcell_apn') ? $this->form_validation->set_value('magcell_apn') : '');
                        $msg .= ':';
                        $msg .= ($this->form_validation->set_value('magcell_dhcp') ? $this->form_validation->set_value('magcell_dhcp') : '');
                        $msg .= ':';
                        $msg .= ($this->form_validation->set_value('magcell_port') ? $this->form_validation->set_value('magcell_port') : '');
                        $msg .= ':';
                        $msg .= ($this->form_validation->set_value('magcell_path') ? $this->form_validation->set_value('magcell_path') : '');
                        $msg .= ':';
                        $msg .= ($this->form_validation->set_value('magcell_ia') ? $this->form_validation->set_value('magcell_ia') : '');
                        $msg .= ':';
                    } else {
                        $msg = 'Magcell:'.$this->form_validation->set_value('magcell_code').':'.$new_url.':*:*:*:*:*:*:*:';
                    }

                    try {
                                                       // if (telegram_DEBUGGING) {
                               //                             $delivery_response = '<api_result>
                               // <send_info>
                               //     <eventid>xxxxxxxx</eventid>
                               // </send_info>
                               // <call_result>
                               //     <result>True</result>
                               // <error />
                               // </call_result>
                               // </api_result>';
                                                       // } else {
                        $delivery_response = $this->telegram_sender->send_message($telegram_sender->chat_id,  $msg);
                        $this->message=$msg;
                        $this->response=$delivery_response;// }
                        //Check the response for a success or fail :D
                        $check = $this->telegram_sender->check_result($delivery_response);
                        if ($check != 'okay') {
                            $status = 'error';
                            $err_msg = $check;
                        } else {
                            $status = 'okay';
                        }

                        if ($status == 'okay') {
                            $this->m_logger->log(
                                $this->tank_auth->get_user_id(),
                                'setup_telegram_sent_successfully',
                                'telegram sent: '.$msg,
                                array('cellphone'=>$cellphone_number, 'telegram_sender'=>$telegram_sender->telegram_sender_id, 'response'=>$delivery_response, ),
                                $site->id,
                                $site->client_id
                                );
                            $this->session->set_flashdata('message', "Setup telegram has been sent to ".$cellphone_number);
                            $this->session->set_flashdata('message_type', 'notice');
                        } else {
                            $this->m_logger->log(
                                $this->tank_auth->get_user_id(),
                                'setup_telegram_sent_failed',
                                'telegram sent: '.$msg.(isset($err_msg) ? " ; Error: ".$err_msg : '' ),
                                array('cellphone'=>$cellphone_number, 'telegram_sender'=>$telegram_sender->telegram_sender_id, 'response'=>$delivery_response, ),
                                $site->id,
                                $site->client_id
                                );
                            $this->session->set_flashdata('message', 'Error sending setup telegram. '.(isset($err_msg) ? '"'.$err_msg.'"' : '' ));
                            $this->session->set_flashdata('message_type', 'error');
                        }

                    } catch(Exception $e) {
                        $status = 'error';
                        $delivery_response = $e->getMessage();

                        $this->m_logger->log(
                            $this->tank_auth->get_user_id(),
                            'setup_telegram_sent_failed',
                            'telegram sent: '.$msg.(isset($err_msg) ? " ; Error: ".$err_msg : '' ),
                            array('cellphone'=>$cellphone_number, 'telegram_sender'=>$telegram_sender->telegram_sender_id, 'response'=>$delivery_response, ),
                            $site->id,
                            $site->client_id
                            );
                        $this->session->set_flashdata('message', 'Error sending setup telegram. '.(isset($err_msg) ? '"'.$err_msg.'"' : '' ));
                        $this->session->set_flashdata('message_type', 'error');
                    }

                } else {
                    $this->m_logger->log(
                        $this->tank_auth->get_user_id(),
                        'setup_telegram_sent_failed',
                        'telegram sent: '.$this->message." ; Error: No telegram Sender selected!",
                        array('cellphone'=>$cellphone_number, 'telegram_sender'=>null, 'response'=>$this->response, ),
                        $site->id,
                        $site->client_id
                        );
                    $this->session->set_flashdata('message', "Error sending setup telegram. 'No telegram Sender selected!'");
                    $this->session->set_flashdata('message_type', 'error');
                }

                redirect('sites/view/'.$site->id);
            }

            $this->data = get_accessible_sites();
            $this->data['site_id'] = $this->input->post('site_id');
            $this->data['errors'] = $errors;
            $this->data['container'] = true;

            $this->data['telegram_active'] = $telegram_active;
            if (!$this->data['telegram_active']) {
                $this->data['telegram_senders'] = $this->m_telegram_sender->get_list();
            }

            $this->layout->view('telegram_site_config_form', $this->data);
        } else {
            redirect('sites');
        }
    }

    /**
     * _get_accounts_data
     *
     * @access public
     *
     * @return void
     */
    function _get_accounts_data() {
        $this->load->helper('accounts/accounts');
        if (function_exists('accounts_get_view_data')) {
            $this->data = accounts_get_view_data($this->data);
        } else { // backwards compatible.
            if ($this->tank_auth->get_user_role() == 'super') {
                $this->data['master_accounts'] = $this->m_telegram_accounts->get_distributor_list(true, false);
            } else {
                if (isset($this->data['acl']['view_distributor_telegram_account'])) {
                    $this->data['master_account'] = $this->m_telegram_accounts->get_by_distributor($this->tank_auth->get_distributor_id());

                    if (isset($this->data['acl']['user_client_link'])) {
                        $this->data['client_accounts'] = $this->m_telegram_accounts->get_list_by_user($this->tank_auth->get_user_id(), true, false);
                    } else {
                        $this->data['client_accounts'] = $this->m_telegram_accounts->get_list_by_distributor($this->tank_auth->get_distributor_id(), true, false);
                    }
                } else {
                    $this->data['client_accounts'] = $this->m_telegram_accounts->get_list_by_user($this->tank_auth->get_user_id(), true, false);
                }
            }
        }
    }

}