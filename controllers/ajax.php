<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * telegram Module
 *
 * @package telegram
 * @author Elsabe Lessing (http://www.lessink.co.za/)
 */
class Ajax extends MY_Controller {

    function __construct() {
        if ((isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') === FALSE)
            redirect('/');
        parent::__construct();
    }

    function telegram_alerts($type, $id, $user_id) {
        if (!$this->tank_auth->is_logged_in()) {
            echo "Error occured";
        } else {
            $this->load->model('m_telegram_alerts');
            $this->load->model('m_client_point');
            $this->load->model('m_report_type');
            $this->load->model('m_telegram_reports');
            $this->load->model('m_site');
            $this->load->helper('util');
            if ($type == 'site') {
                $data['site_id'] = intval($id);
                $data['site'] = $this->m_site->get_by_id($data['site_id']);
                $data['client_id'] = isset($data['site']->client_id)?$data['site']->client_id:0;
            } else {
                $data['site_id'] = 0;
                $data['client_id'] = intval($id);
            }
            $data['user_id'] = intval($user_id);
            $data['type'] = $type;
            $data['type_id'] = $id;

            $data['edit'] = true;
            $data['tz'] = timezones($this->tank_auth->get_user_timezone());

            $data['current_user'] = $this->users->get_user($user_id);
            $data['telegram_details'] = $this->m_telegram_alerts->get_by_site_user_id($user_id, $data['site_id'], 0, $data['client_id']);
            // $data['report_types'] = $this->m_report_type->get_array();
            // $data['report_grouped_types'] = $this->m_report_type->get_alerts_grouped_array();
            $data['telegram_grouped_reports'] = $this->m_telegram_reports->get_grouped_array(1);

            $this->load->view('telegram_alerts_form', $data);
        }
    }

}