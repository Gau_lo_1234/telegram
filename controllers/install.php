<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Installer for the telegram Module
 *
 * @package telegram
 * @author Elsabe Lessing (http://www.lessink.co.za/)
 */
class Install extends MY_Controller {
    private $module = 'telegram';
    private $version = '0.4.4';
    private $level = 'site';
    private $free = 1;
    private $asset_path = 'application/modules/telegram/assets';

    function __construct() {
        parent::__construct();
        $this->lang->load('telegram');

        check_permissions(array('super'));
    }

    function Index() {
           $data['version'] = $this->version;
           $this->layout->view('install', $data);
    }

    function vamos() {
        // if ($this->m_modules->is_installed('telegram') && $this->m_modules->get_version('telegram') == $this->version) {
        //     $this->_setup_permissions();
        //     $this->session->set_flashdata('message', 'Modules has been successfully installed.');
        //     $this->session->set_flashdata('message_type', 'notice');
        //     $this->m_modules->activate($this->module, $this->version, $this->level, $this->free);
        // } else {
            //check that accounts module is installed...
            if ($this->m_modules->is_installed('accounts')) {
                if ($this->_update_database()) {
                    $this->_setup_permissions();
                    $this->m_modules->activate($this->module, $this->version, $this->level, $this->free);
                    $this->session->set_flashdata('message', 'telegram Module has been successfully installed.');
                    $this->session->set_flashdata('message_type', 'notice');
                } else {
                    $this->session->set_flashdata('message', 'Failed to install telegram Module. Problem updating the database.');
                    $this->session->set_flashdata('message_type', 'error');
                }
            } else {
                    $this->session->set_flashdata('message', 'Failed to install telegram Module. Accounts module is required!');
                    $this->session->set_flashdata('message_type', 'error');
            }
        // }

        redirect('settings');
    }

    function _update_database() {
        $schema = file_get_contents(APPPATH.'modules/telegram/telegram_database.sql');

        // Parse the queries
        $queries = explode('-- command split --', $schema);



        $this->db->trans_start();
        foreach($queries as $query) {

                $query = rtrim( trim($query), "\n;");
                $this->db->query($query);
              
        }
       
        $this->db->trans_complete();

        if ($this->db->trans_status() === FALSE) {
           
            return FALSE;
        }

        // 1.5 changes.
        if (!$this->db->field_exists( 'limit', 'telegram_accounts')) {
            $this->db->query('ALTER TABLE  `telegram_accounts` ADD  `limit` INT NOT NULL');
        }

        $this->db->where('key', 'telegram_default_limit');
        $query = $this->db->get('settings');
        if ($query->num_rows == 0) {
            $data = array(
                'key' =>'telegram_default_limit',
                'value' =>155,
                'group'=>'telegram'
            );
            $this->db->insert('settings', $data); // +/- 5 a day.
        }

        // 0.3.0
        $this->db->where('key', 'telegram_credit_cost');
        $query = $this->db->get('settings');
        if ($query->num_rows == 0) {
            $data = array(
                'key' =>'telegram_credit_cost',
                'value' =>20,
                'group'=>'telegram'
            );
            $this->db->insert('settings', $data);
        }


        // Reports
        $this->_create_report();

        // 0.3.2

        if (!$this->db->field_exists( 'time', 'telegram_alerts')) {
            $this->db->query('ALTER TABLE  `telegram_alerts` ADD COLUMN `time` INT NULL DEFAULT "0" AFTER `last_sent`');
        }
        if (!$this->db->field_exists( 'day_of_week', 'telegram_alerts')) {
            $this->db->query('ALTER TABLE  `telegram_alerts` ADD COLUMN `day_of_week` TINYINT(1) NULL DEFAULT "0" AFTER `time`');
        }
        if (!$this->db->field_exists( 'day_of_month', 'telegram_alerts')) {
            $this->db->query('ALTER TABLE  `telegram_alerts` ADD COLUMN `day_of_month` TINYINT(2) NULL DEFAULT "0" AFTER `day_of_week`');
        }

        // telegram Templates
        if ($this->db->table_exists('telegram_templates')) {
            $this->db->query( 'ALTER TABLE `telegram_templates` ADD COLUMN `active` TINYINT NOT NULL DEFAULT 0 AFTER `text`, RENAME TO  `telegram_reports`' );
        }

        $telegram_templates = array(
            'inactive'=>array(
                // Core
                'sequence_error' => 'Magcell Sequence Error',
                'signal_b_up' => 'Mains Restore',
                // telegram
                'telegram_credits_low' => 'Telegram Credits Low',
                // Accounts
                'account_expiry_notice' => 'Module Expiry Notice',
                ),
            'active'=>array(
                // Core
                'signal_c' => 'Tamper',
                'signal_b' => 'Mains Fail',
                'signal_a' => 'Call Button Activation',
                'battery_low' => 'Battery Low',
                'battery_okay' => 'Battery Okay',
                // Patrols
                'patrol_okay' => 'Patrol: Okay',
                'missed_points' => 'Patrol: Missed Points',
                'fail_patrol' => 'Patrol: Failed Patrol',
                'patrol_irregular' => 'Patrol: Patrol Irregular',
                'patrol_too_fast' => 'Patrol: Patrol Too Fast',
                // Interactive Guard
                'patrol_okay_guard' => 'Patrol: Complete',
                'missed_points_guard' => 'Patrol: Incomplete',
                'fail_patrol_guard' => 'Patrol: Incomplete',
                'patrol_time_between' => 'Patrol: Time Between Points',
                // Quick Summary Report
                'quick_summary' => 'Quick Summary Report',
                //Alert point 0.7
                'alert_point'=>'Alert Points',
                'signal_event_message'=>'Event Message',
                'signal_police'=>'Armed response required',
                'signal_fire'=>'Fire',
                'signal_medical'=>'Medical emergency',
                )
            );
        foreach($telegram_templates as $active_status=>$templates) {
            $active = $active_status == 'active' ? '1' : '0';

            foreach($templates as $report=>$template) {
                $this->db->where('report', $report);
                $query_rt = $this->db->get('report_types');
                if ($query_rt && $query_rt->num_rows() > 0) {
                    $row = $query_rt->row();
                    if ($row) {
                        //check if we have it yet, we don't want to overwrite current ones...
                        $this->db->where('report_id', $row->id);
                        $query_telegram = $this->db->get('telegram_reports');
                        if (!$query_telegram || $query_telegram->num_rows == 0) {
                            $template_data = array(
                                'report_id'=>$row->id,
                                'text'=>$template,
                                'active'=>$active,
                                );
                            $this->db->insert('telegram_reports', $template_data);
                        }
                    }

                }
            } // foreach
        } // foreach

        return TRUE;
    }

    function _create_report() {
        // 0.3.0
        $this->db->where('report', 'telegram_credits_low');
        $query = $this->db->get('report_types');
        if ($query && $query->num_rows() > 0 ) {
            //update
            $this->db->query("UPDATE `report_types` SET
                `report`='telegram_credits_low', `display_name`='Telegram Credits Low', `hourly`=0, `daily`=0, `weekly`=0, `monthly`=0, `alert`=1,  `after_upload`=0, `remind`=0,
                `group`='telegram', `icon_path`='', `sort_order`=129, `dashboard`=0, `automated`=1, `module`='telegram', `point_group`=0, `automated_type`='client'
                WHERE report = 'superviser_report'; ");
        } else {
            //insert
            $this->db->query("INSERT INTO `report_types`
                (`id`, `report`, `display_name`, `hourly`, `daily`, `weekly`, `monthly`, `alert`,  `after_upload`, `remind`, `group`, `icon_path`, `sort_order`, `dashboard`, `automated`, `module`, `point_group`, `automated_type`) VALUES
                (NULL, 'telegram_credits_low', 'telegram Credits Low', 0, 0, 0, 0, 1, 0, 0, 'telegram', '', 129, 0, 1, 'telegram', 0, 'client');");
        }

        $this->db->where('report', 'telegram_messages_report');
        $query = $this->db->get('report_types');
        if ($query && $query->num_rows() > 0 ) {
            //update
            $this->db->query("UPDATE `report_types` SET
                `report`='telegram_messages_report', `display_name`='Telegram Messages Sent', `hourly`=1, `daily`=1, `weekly`=1, `monthly`=1, `alert`=0,  `after_upload`=0, `remind`=0,
                `group`='telegram', `icon_path`='application/modules/telegram/assets/icon_16.png', `sort_order`=110, `dashboard`=1, `automated`=1, `module`='telegram', `point_group`=0, `automated_type`='client'
                WHERE report = 'superviser_report'; ");
        } else {
            //insert
            $this->db->query("INSERT INTO `report_types`
                (`id`, `report`, `display_name`, `hourly`, `daily`, `weekly`, `monthly`, `alert`, `after_upload`, `remind`, `group`, `icon_path`, `sort_order`, `dashboard`, `automated`, `module`, `point_group`, `automated_type`) VALUES
                (NULL, 'telegram_messages_report', 'Telegram Messages Sent', 1, 1, 1, 1, 0, 0, 0, 'telegram', 'application/modules/telegram/assets/icon_16.png', 110, 1, 1, 'telegram', 0, 'client');");
        }
    }

    function _setup_permissions() {
        $this->load->model('m_permissions');

        // Add new permissions for accounts.
        $new_permissions = array(
            array('permission'=>'telegram', 'group'=>'nav', 'roles'=>array('super', 'distrib', 'admin', 'user')),
            array('permission'=>'add_telegram_credits', 'group'=>'telegram', 'roles'=>array('super', 'distrib', 'admin')),
            array('permission'=>'edit_telegram_account', 'group'=>'telegram', 'roles'=>array('super', 'distrib', 'admin')),
            array('permission'=>'edit_telegram_notification', 'group'=>'telegram', 'roles'=>array('super', 'distrib', 'admin')),
            array('permission'=>'manage_telegram_alerts', 'group'=>'telegram', 'roles'=>array('super', 'distrib', 'admin', 'user')),
            array('permission'=>'remove_telegram_credits', 'group'=>'telegram', 'roles'=>array('super', 'distrib', 'admin')),
            array('permission'=>'site_config_telegram', 'group'=>'telegram', 'roles'=>array('super', 'distrib')),
            array('permission'=>'view_client_telegram_detail', 'group'=>'telegram', 'roles'=>array('super', 'distrib', 'admin','user')),
            array('permission'=>'view_distributor_telegram_detail', 'group'=>'telegram', 'roles'=>array('super', 'distrib', 'admin')),
            array('permission'=>'view_telegram_alerts', 'group'=>'telegram', 'roles'=>array('super', 'distrib', 'admin', 'user')),
            );
        foreach ($new_permissions as $n) {
            $permission_id = $this->m_permissions->add($n['permission'], $n['group']);
            // Who has access??
            if (!empty($n['roles'])) {
                foreach ($n['roles'] as $role) {
                    $this->m_permissions_roles->add($permission_id, $role);
                }
            }
        }

    }

}

/* End of file accounts/install.php */
/* Location: ./application/controllers/telegram/install.php */