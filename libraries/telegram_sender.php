<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
define('BOT_KEY', 'bot845865348:AAFoI5W24Ds-3Hj3MfIsSqiTdA9i_1xDO2I');
/**
 * Class to send telegram with mymobileapi.com
 *
 * @package telegram
 * @author Elsabe Lessing (http://www.lessink.co.za/)
 */
class telegram_sender {

    function send_message($chat_id, $msg) {
        //This code block can be customised.
        //The $data array contains data that must be modified as per the API documentation. The array contains data that you will post to the server
        $data= array(
                "chat_id" => $chat_id,
                "message" => $msg
        ) ; //This contains data that you will send to the server.
        
        	
        return $this->do_post_request($data);  //Sends the post, and returns the result from the server.
    }

    function get_reply($key, $key_pass, $lastId) {
        $data = array (
           'Type'=>'repliesparam',
            'Username'=>$key,
            'Password'=>$key_pass,
            'Id'=>$lastId
        );
        print_r($data);
        return $this->do_post_request('http://www.mymobileapi.com/api5/http5.aspx', $data);
    }

    //Posts data to server and recieves response from server
    //DO NOT EDIT unless you are sure of your changes
    function do_post_request($data) {
         $chat_id=$data['chat_id'];
         $msg=$data['message'];
         
        	 
        $url='https://api.telegram.org/'.BOT_KEY.'/sendMessage?chat_id='.trim($chat_id).'&text='.urlencode ($msg).'';
 	file_get_contents( $url);
            }

    //takes the XML output from the server and makes it into a readable xml file layout
    //DO NOT EDIT unless you are sure of your changes
    function formatXmlString($xml) {
        // add marker linefeeds to aid the pretty-tokeniser (adds a linefeed between all tag-end boundaries)
        $xml = preg_replace('/(>)(<)(\/*)/', "$1\n$2$3", $xml);

        // now indent the tags
        $token      = strtok($xml, "\n");
        $result     = ''; // holds formatted version as it is built
        $pad        = 0; // initial indent
        $matches    = array(); // returns from preg_matches()

        // scan each line and adjust indent based on opening/closing tags
        while ($token !== false) :

        // test for the various tag states

        // 1. open and closing tags on same line - no change
        if (preg_match('/.+<\/\w[^>]*>$/', $token, $matches)) :
        $indent=0;
        // 2. closing tag - outdent now
        elseif (preg_match('/^<\/\w/', $token, $matches)) :
        $pad--;
        // 3. opening tag - don't pad this one, only subsequent tags
        elseif (preg_match('/^<\w[^>]*[^\/]>.*$/', $token, $matches)) :
        $indent=1;
        // 4. no indentation needed
        else :
        $indent = 0;
        endif;

        // pad the line with the required number of leading spaces
        $line    = str_pad($token, strlen($token)+$pad, ' ', STR_PAD_LEFT);
        $result .= $line . "\n"; // add to the cumulative result, with linefeed
        $token   = strtok("\n"); // get the next token
        $pad    += $indent; // update the pad size for subsequent lines
        endwhile;

        return $result;
    }

    /**
     *  Checks XML for error codes and returns a okay or errstring
     */
    function check_result($xml_string) {
        $return = '';
        try {
            libxml_use_internal_errors(true);
            $response = new SimpleXMLElement($xml_string);
            if ($response->call_result->result == "True") {
                $return = 'okay';
            } else {
                $return = $response->call_result->error;
            }
        } catch(Exception $e) {
            $return = "XML Error: ";
            foreach(libxml_get_errors() as $error) {
                $return .= "\t". $error->message;
            }
        }
        return $return;
    }

}
