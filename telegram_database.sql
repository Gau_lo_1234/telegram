CREATE TABLE IF NOT EXISTS `telegrams`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `chatid` int(20) NOT NULL,
  `chat_id` int(20) DEFAULT NULL,
  `response` varchar(400) NOT NULL,
  `date` int(20) NOT NULL,
  PRIMARY KEY(id)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- command split --

CREATE TABLE IF NOT EXISTS `telegram_accounts` (
  `telegram_account_id` int(11) NOT NULL AUTO_INCREMENT,
  `distributor_id` int(11) DEFAULT NULL,
  `client_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `telegram_key` varchar(30) DEFAULT NULL,
  `chat_id` varchar(30) DEFAULT NULL,
  `activated` int(11) NOT NULL,
  `created` int(11) NOT NULL,
  PRIMARY KEY (telegram_account_id)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- command split --

CREATE TABLE IF NOT EXISTS `telegram_alerts` (
  `telegram_alert_id` int(11) NOT NULL AUTO_INCREMENT,
  `client_id` int(11) NOT NULL,
  `site_id` int(11) NOT NULL,
  `report_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `frequency` int(11) NOT NULL,
  `last_sent` int(11) NOT NULL DEFAULT 0,
  `time` int(11) DEFAULT 0,
  `day_of_week` tinyint(1) DEFAULT 0,
  `day_of_month` tinyint(2) DEFAULT 0,
  PRIMARY KEY (telegram_alert_id)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- command split --

CREATE TABLE IF NOT EXISTS `telegram_credits_log` (
  `telegram_account_id` int(11) NOT NULL AUTO_INCREMENT,
  `site_id` int(11) NOT NULL DEFAULT -1,
  `target_account_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `credits` int(11) NOT NULL,
  `date` int(11) NOT NULL,
  PRIMARY KEY (telegram_account_id)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- command split --

CREATE TABLE IF NOT EXISTS `telegram_emails` (
  `telegram_email_id` int(11) NOT NULL AUTO_INCREMENT,
  `telegram_account_id` int(11) NOT NULL,
  `type` varchar(11) COLLATE utf8_unicode_ci NOT NULL,
  `last_sent` int(11) DEFAULT NULL,
  `frequency` tinyint(1) DEFAULT 0 COMMENT '1 daily, 2 weekly, 3 monthly, 4 hourly, 5 alert',
  `user_id` int(10) UNSIGNED NOT NULL,
  PRIMARY KEY (telegram_email_id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- command split --

CREATE TABLE IF NOT EXISTS `telegram_messages` (
  `telegram_message_id` int(11) NOT NULL AUTO_INCREMENT,
  `telegram_account_id` int(11) NOT NULL,
  `telegram_sender_id` int(11) NOT NULL,
  `client_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `number` varchar(255) NOT NULL,
  `message` varchar(255) NOT NULL,
  `delivery_response` text NOT NULL,
  `status` varchar(255) NOT NULL,
  `created_date` int(11) NOT NULL,
  PRIMARY KEY (telegram_message_id)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- command split --

CREATE TABLE IF NOT EXISTS `telegram_sender` (
  `telegram_sender_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL,
  `key` varchar(255) DEFAULT NULL,
  `created` int(11) NOT NULL,
  `modified` int(11) NOT NULL,
   PRIMARY KEY (telegram_sender_id)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- command split --

CREATE TABLE IF NOT EXISTS `telegram_reports` (
  `telegram_template_id` int(11) NOT NULL AUTO_INCREMENT,
  `report_id` int(11) NOT NULL,
  `text` varchar(160) NOT NULL,
  `active` tinyint(4) NOT NULL DEFAULT 0,
   PRIMARY KEY (telegram_template_id)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
