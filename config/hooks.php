<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------------
| Hooks
| -------------------------------------------------------------------------
| This file lets you define "hooks" to extend CI without hacking the core
| files.  Please see the user guide for info:
|
|	http://codeigniter.com/user_guide/general/hooks.html
|
*/

$module_hooks_path = 'modules/telegram/hooks';

/*Settings*/
$hook['settings_modules'][] = array(
        'function' => 'telegram_settings_widget',
        'filename' => 'hook_functions.php',
        'filepath' => $module_hooks_path,
);
/*Site*/
$hook['site_list_modules'][] = array(
        'function' => 'telegram_list_site_widget',
        'filename' => 'hook_functions.php',
        'filepath' => $module_hooks_path,
);
$hook['site_activity'][] = array(
        'function' => 'telegram_site_activity',
        'filename' => 'hook_functions.php',
        'filepath' => $module_hooks_path,
);
$hook['site_activity_never'][] = array(
        'function' => 'telegram_site_activity_never',
        'filename' => 'hook_functions.php',
        'filepath' => $module_hooks_path,
);


/*Client*/
$hook['client_list_modules'][] = array(
        'function' => 'telegram_list_clients_widget',
        'filename' => 'hook_functions.php',
        'filepath' => $module_hooks_path,
);
$hook['client_edit'][] = array(
        'function' => 'telegram_client_edit',
        'filename' => 'hook_functions.php',
        'filepath' => $module_hooks_path,
);
$hook['delete_client'][] = array(
        'function' => 'telegram_client_delete',
        'filename' => 'hook_functions.php',
        'filepath' => $module_hooks_path,
);


$hook['alert_point_unassign'][] = array(
        'function' => 'telegram_alert_point_unassign',
        'filename' => 'hook_functions.php',
        'filepath' => $module_hooks_path,
);
$hook['alert_point_notification_setup'][] = array(
        'function' => 'telegram_client_point_alert_setup',
        'filename' => 'hook_functions.php',
        'filepath' => $module_hooks_path,
);
$hook['alert_point_notification_setup_save'][] = array(
        'function' => 'telegram_client_point_alert_setup_save',
        'filename' => 'hook_functions.php',
        'filepath' => $module_hooks_path,
);
$hook['client_point_more_info_popup'][] = array(
        'function' => 'telegram_client_point_more_info_popup',
        'filename' => 'hook_functions.php',
        'filepath' => $module_hooks_path,
);





/*Distributor*/
$hook['distributor_add'][] = array(
        'function' => 'telegram_distributor_add',
        'filename' => 'hook_functions.php',
        'filepath' => $module_hooks_path,
);
$hook['distributor_delete'][] = array(
        'function' => 'telegram_distributor_delete',
        'filename' => 'hook_functions.php',
        'filepath' => $module_hooks_path,
);

/*Account*/
//Hook in accounts module.
$hook['account_manage_modules'][] = array(
        'function' => 'telegram_accounts_manage_module',
        'filename' => 'hook_functions.php',
        'filepath' => $module_hooks_path,
);

$hook['account_detail'][] = array(
        'function' => 'telegram_accounts_widget',
        'filename' => 'hook_functions.php',
        'filepath' => $module_hooks_path,
);

/* Account credit report.... */
// $hook['accounts_credit_report'][] = array(
//         'function' => 'telegram_accounts_credit_report',
//         'filename' => 'hook_functions.php',
//         'filepath' => $module_hooks_path,
// );

//hook for super dash
$hook['account_super_dash'][] = array(
        'function' => 'telegram_accounts_dash',
        'filename' => 'hook_functions.php',
        'filepath' => $module_hooks_path,
);

/*Alerts*/
$hook['handle_alert'][] = array(
        'function' => 'telegram_handle_alert',
        'filename' => 'hook_functions.php',
        'filepath' => $module_hooks_path,
);

/*Crons*/

// $hook['daily_cron'][] = array(
//         'function' => 'telegram_cron',
//         'filename' => 'hook_functions.php',
//         'filepath' => $module_hooks_path,
// );

$hook['hourly_cron'][] = array(
        'function' => 'telegram_cron',
        'filename' => 'hook_functions.php',
        'filepath' => $module_hooks_path,
);

$hook['data_maintenance_cron'][] = array(
        'function' => 'telegram_data_maintenance_cron',
        'filename' => 'hook_functions.php',
        'filepath' => $module_hooks_path,
);


/* End of file hooks.php */
/* Location: ./application/modules/xena/config/hooks.php */
