<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if(!defined('telegram_DEBUGGING')) define('telegram_DEBUGGING', true);

$config['telegram_assets'] = APPPATH.'/modules/telegram/assets/';
