<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * telegram_accounts
 *
 * @package telegram
 * @author Elsabe Lessing (http://www.lessink.co.za/)
 */
class m_telegram_Accounts extends CI_Model {
    private $table_name = 'telegram_accounts';

    function __construct() {
        parent::__construct();
    }


    /**
     * @method get_chat_id
     */
    function get_chat_id($user_id=null){

        $this->db->where('user_id', $user_id);
        $this->db->limit(1);
        $query=$this->db->get('telegram_accounts');
        if($query->num_rows()>0){
            return $query->row()->chat_id;
        }
    }
    /**
     * get_by_id
     *
     * @param mixed $telegram_account_id
     * @param mixed $get_sender
     *
     * @access public
     *
     * @return mixed Value.
     */
    function get_by_id($telegram_account_id, $get_sender=true) {
        $this->db->where('telegram_account_id',$telegram_account_id);
        if ($get_sender) {
            $this->db->join('telegram_sender', $this->table_name.'.telegram_sender_id = telegram_sender.telegram_sender_id', 'left');
        }
        $query = $this->db->get($this->table_name);
        if ($query && $query->num_rows() == 1) {
            return $query->row();
        }
        return false;
    }

     /**
     * get_by_id
     *
     * @param mixed $telegram_account_id
     * @param mixed $get_sender
     *
     * @access public
     *
     * @return mixed Value.
     */
    function get_by_user($user_id=null) {
        $this->db->where('telegram_accounts.user_id',$user_id);
        $this->db->join('users','users.id=telegram_accounts.user_id');
        $query = $this->db->get($this->table_name);
        if ($query && $query->num_rows() >0) {
            return $query->row();
        }
        return false;
    }
    /**
     * @method check_account_exists
     */
    function check_account_exists($user_id){
        $this->db->where('user_id', $user_id);
        $query=$this->db->get('telegram_accounts');
       return $query->num_rows()>0;
    }
    /**
     * get_by_distributor
     *
     * @param mixed $distributor_id
     * @param mixed $get_sender
     *
     * @access public
     *
     * @return mixed Value.
     */
    function get_by_distributor($distributor_id, $get_sender=true) {
        $this->db->join('distributors', 'distributors.id = '.$this->table_name.'.distributor_id');
        $this->db->where('distributor_id', $distributor_id);
        $this->db->where('client_id', 0);
        if ($get_sender) {
            //$this->db->join('telegram_sender', $this->table_name.'.telegram_sender_id = telegram_sender.telegram_sender_id', 'left');
        }
        $query = $this->db->get($this->table_name);
        if ($query && $query->num_rows() == 1) {
            return $query->row();
        }
        return false;
    }

    /**
     * get_by_client
     *
     * @param mixed $client_id
     * @param mixed $get_sender
     *
     * @access public
     *
     * @return mixed Value.
     */
    function get_by_client($client_id, $get_sender=true) {
        $this->db->select('users.*, users_clients.*, A.*');
        $this->db->where('users_clients.client_id', $client_id);
        
        
        if ($get_sender) {
            
            $this->db->join('users_clients', 'users.id =users_clients.user_id');
            $this->db->join('telegram_accounts A', 'A.client_id =users_clients.client_id');
        }
        $query = $this->db->get('users');
       
        if ($query && $query->num_rows()>0) {
            return $query->result();
      }
      return FALSE;
    }

    /**
     * Get a list of accounts associated with a distributor.
     * Everything wher client_id!=0
     *
     * @param int $distributor_id
     * @param bool $get_client (optional)
     * @param bool $get_credits (optional)
     * @return array or false
     */
    function get_list_by_distributor($distributor_id, $get_client=false, $get_credits=false) {
        $this->db->where($this->table_name.'.distributor_id', $distributor_id);
        return $this->get_list($get_client, $get_credits);
    }

    /**
     * get_array_by_distributor
     *
     * @param mixed $distributor_id
     * @param mixed $get_client
     * @param mixed $get_credits
     *
     * @access public
     *
     * @return mixed Value.
     */
    function get_array_by_distributor($distributor_id, $get_client=false, $get_credits=false) {
        $this->db->where($this->table_name.'.distributor_id', $distributor_id);
        return $this->get_array($get_client, $get_credits);
    }

    /**
     * Get a list of accounts that a user would have access to.
     * Checkls the users_clients account for relationships.
     *
     * @param int $user_id
     * @param bool $get_client (optional)
     * @param bool $get_credits (optional)
     * @return array or false
     */
    function get_list_by_user($user_id, $get_client=false, $get_credits=false) {
        // accounts for clients that user is assocaited wiht.... which is one. haha
        $this->db->join('users_clients', 'users_clients.client_id = '.$this->table_name.'.client_id');
        $this->db->where('users_clients.user_id', $user_id);
        return $this->get_list($get_client, $get_credits);
    }

    /**
     * Gets a list of accounts
     *
     * @param bool $get_client (optional)
     * @param bool $get_credits (optional)
     * @return array or false
     */
    function get_list($get_client=false) {
        if ($get_client) {
            $this->db->join('clients', 'clients.id = '.$this->table_name.'.client_id');
        }
        $query = $this->db->get($this->table_name);
        if ($query && $query->num_rows() > 0) {
            return $query->result();
        } else {
            return FALSE;
        }
    }

    /**
     * Returns a list of distributor accounts.
     * (client_id=0)
     *
     * @param bool $get_client
     * @param bool $get_credits
     * @return array or false
     */
    function get_distributor_list($get_client=false) {
        if ($get_client) {
            $this->db->join('distributors', 'distributors.id = '.$this->table_name.'.distributor_id');
        }
        $this->db->where('client_id',0);
        $query = $this->db->get($this->table_name);
        if ($query && $query->num_rows() > 0) {
            return $query->result();
        } else {
            return FALSE;
        }
    }

    /**
     * create
     *
     * @param mixed $distributor_id
     * @param mixed $client_id
     * @param int   $telegram_sender_id
     * @param int   $telegram_limit
     *
     * @access public
     *
     * @return mixed Value.
     */
    function create($distributor_id, $client_id, $user_id=null) {
        $data = array(
            '`distributor_id`'=>$distributor_id,
            '`client_id`'=>$client_id,
            '`user_id`'=>$user_id,
            '`chat_id`'=>null,
            '`activated`'=>0,
            '`created`'=>time()
            );
        if (!$this->check_if_exists(trim($user_id))) {
            $this->db->insert($this->table_name, $data);
            return $this->db->insert_id();
        }
    }
    function hello(){
        echo "solved";
    }
    /**
     * @method check_if_exists
     * 
     */
    function check_if_exists($user_id=null){
        $this->db->where('user_id', $user_id);
        $query=$this->db->get($this->table_name);
        return $query->num_rows()>0;
    }
    /**
     * @method get_list_by_client
     * @param client_id
     */
    function get_list_by_client($client_id=null){
      
        $this->db->where('us.client_id',$client_id);
        //$this->db->where('B.chat_id IS NOT NULL');
        $this->db->join('users_clients us', 'us.user_id=A.id');
        $query=$this->db->get('users A');
        
        return $query->result();
    }
    /**
     * @method get_list_by_site
     */
    function get_list_by_site($client_id=null){
        $this->db->join('telegram_accounts B', 'B.user_id=A.id');
        $this->db->join('sites C', 'C.client_id=B.client_id');
        $this->db->where('B.client_id',$client_id);
        $query=$this->db->get('users A');
        //echo $this->db->last_query();
        if($query && $query->num_rows()>0){
            return $query->result();
        }
       
    }
    /**
     * update
     *
     * @param mixed $telegram_accounts_id
     * @param mixed $telegram_sender_id
     * @param int   $distributor_id
     * @param mixed $telegram_limit
     *
     * @access public
     *
     * @return mixed Value.
     */
    function update($telegram_accounts_id, $telegram_sender_id, $distributor_id=0, $telegram_limit=-1 ) {
        $this->db->where('telegram_account_id', $telegram_accounts_id);
        $data = array('telegram_sender_id'=>$telegram_sender_id);
        if ($distributor_id) {
            $data['distributor_id'] = $distributor_id;
        }
        if ($telegram_limit > -1) {
            $data['limit'] = $telegram_limit;
        }
        $this->db->update($this->table_name, $data );
    }

    /**
     * update_user
     *
     * @param mixed $telegram_accounts_id
     * @param mixed $user_id
     *
     * @access public
     *
     * @return mixed Value.
     */
    function update_user($telegram_accounts_id, $user_id) {
        $this->db->where('telegram_account_id', $telegram_accounts_id);
        $data = array('user_id'=>$user_id);
        $this->db->update($this->table_name, $data );
    }

    /**
     * delete
     *
     * @param mixed $telegram_accounts_id
     *
     * @access public
     *
     * @return mixed Value.
     */
    function delete($telegram_accounts_id) {
        $account = $this->get_by_id($telegram_accounts_id);

        $this->  db->where('telegram_account_id', $telegram_accounts_id);
        $this->db->delete($this->table_name);

        // credit logs
        $this->load->model('telegram/m_telegram_credit_log');
        $this->m_telegram_credit_log->delete($telegram_accounts_id);

        if ($account && $account->client_id) {
            // messages
            $this->load->model('telegram/m_telegram_message');
            $this->m_telegram_message->delete_by_client($account->client_id);
            //m_telegram_alerts
            $this->load->model('telegram/m_telegram_alerts');
            $this->m_telegram_alerts->delete_by_client($account->client_id);
        }

    } // delete

    /**
     * delete_by_user
     *
     * @param mixed $telegram_accounts_id
     *
     * @access public
     *
     * @return mixed Value.
     */
    function delete_by_user($user_id=null) {
        $account = $this->get_by_user($user_id);

        $this->  db->where('user_id', $user_id);
        $this->db->delete($this->table_name);

        

        if ($account && $account->client_id) {
            // messages
            $this->load->model('telegram/m_telegram_message');
            $this->m_telegram_message->delete_by_client($account->client_id);
            //m_telegram_alerts
            $this->load->model('telegram/m_telegram_alerts');
            $this->m_telegram_alerts->delete_by_client($account->client_id);
        }

        return $this->db->affected_rows();
    } // delete

    /**
     * add_credits
     *
     * @param mixed $telegram_accounts_id
     * @param mixed $credits
     *
     * @access public
     *
     * @return mixed Value.
     */
    function add_credits($telegram_accounts_id, $credits) {
        $this->db->where('telegram_account_id', $telegram_accounts_id);
        $query = $this->db->get($this->table_name);
        if ( $query && $query->num_rows() > 0 ) {
            $row = $query->row();
            $data['credits'] = $row->credits + $credits;
            $this->db->where('telegram_account_id', $telegram_accounts_id);
            $this->db->update($this->table_name, $data);
        } else {
            return FALSE;
        }
    }
}
