<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * telegram_templates
 *
 * @package telegram
 * @author Elsabe Lessing (http://www.lessink.co.za/)
 */
class m_telegram_Templates extends CI_Model
{
	private $table_name = 'telegram_templates';

	function __construct() {
		parent::__construct();
	}

	function get_list() {
                $this->db->join('report_types', 'report_types.id = '.$this->table_name.'.report_id');
		$query = $this->db->get($this->table_name);
                if ($query->num_rows() > 0) {
			return $query->result();
		} else {
			return FALSE;
		}
	}

        function get_by_report($report_id) {
            $this->db->where('report_id', $report_id);
		$query = $this->db->get($this->table_name);
		if ($query->num_rows() == 1) {
                          return $query->row();
		}
		return FALSE;
        }

	function get_by_id($id) {
		$this->db->where('telegram_template_id', $id);
		$query = $this->db->get($this->table_name);
		if ($query->num_rows() == 1) {
                          return $query->row();
		}
		return FALSE;
	}

	function create($report_id, $text) {
		$data = array(
                    	'report_id'=>$report_id,
                        'text'=>$text,
		);

		if ($this->db->insert($this->table_name, $data)) {
			return $this->db->insert_id();
		}
		return 0;
	} // create

	function update($telegram_template_id, $report_id, $text) {
		$data = array(
                    	'report_id'=>$report_id,
                        'text'=>$text,
		);

                $this->db->where('telegram_template_id', $telegram_template_id);
		if ($this->db->update($this->table_name, $data)) {
			return TRUE;
		}
		return FALSE;
	} // update

	function delete($telegram_template_id) {
               $this->db->where('telegram_template_id', $telegram_template_id);
               $this->db->delete($this->table_name);
        } // delete

}
