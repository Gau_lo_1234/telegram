<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * telegram Emails
 *
 * @package telegram
 * @author Elsabe Lessing (http://www.lessink.co.za/)
 */
class m_telegram_bot extends CI_Model {
    private $table_name = 'telegrams';

    function __construct() {
        parent::__construct();	
    }

    function get_list($chatid=null,  $response=null, $date=null) {
       

     if(isset($chatid)){
          $this->db->where('chatid', $chatid);
     }
      if(isset($response)){
        $this->db->where('response', $response);
    }
    if(isset($date)){
        $this->db->where('date', $date);
    }
     $query=$this->db->get($this->table_name);


     if($query->num_rows()>0){
        
         return   $query->result();
     }
     return null;
    }
  
    /**
     * @method fetch_client_users
     * @param user_id
     * @return int
     */
    function fetch_client_users($user_id=null){
        $this->db->select('A.*, C.name client_name, C.id client_id');
        if(isset($user_id)){
            $this->db->where('A.id',$user_id);
        }
        $this->db->join('users_clients B', 'B.user_id=A.id','LEFT');
        $this->db->join('clients C', 'C.id=B.client_id','LEFT');
        $query=$this->db->get("users A");
        if($query->num_rows()>0){
            return $query->row();
        }
        else{
            return null;
        }
    }
    /**
     * @method update_chat_id
     * @param chat_id
     * @param client_id
     * @return affected_rows
     */
    function update_chat_id($user_id, $chat_id){
        $this->db->where('user_id',$user_id );
        $data['chat_id']=$chat_id;
        $this->db->update('telegram_accounts', $data);
     
        return $this->db->affected_rows();
    }
    /**
     * @method add_new
     * @param array()
     * @return affected_rows
     */
    function add_new($data){
    $this->db->insert($this->table_name,$data);
    return $this->db->insert_id();
    }


    /**
     * @method add_new
     * @param array 
     * @return int
     */
    function account_setup($data){
        if (!$this->check_account_exists($data['user_id'])) {
          
            $data['created']=time();
            $this->db->insert('telegram_accounts', $data);
       
            return $this->db->insert_id();
        }
    }

    /**
     * @method check_account_exists
     * @param user_id
     * @return int
     */
    function check_account_exists($user_id=null){
    $this->db->where('user_id', $user_id);
    $query=$this->db->get('telegram_accounts');
     
    return $query->num_rows()>0;

    }
    
    /**
     * @method get_role
     * @param role
     * @return boolean
     */
    function get_role($role) {
        $this->db->where('role', $role);
        $query = $this->db->get($this->roles_table);
        if ($query && $query->num_rows() > 0) {
            return $query->row();
        }
        return false;
    }
     /**
     * @method get_role
     * @param role
     * @param display_name
     * @param module
     * @param sort_order
     * @return int
     */
    function create($role, $display_name, $module='', $sort_order=0) {
        $r = $this->get_role($role);
        if ($r === false || empty($r)) {
            $data = array(
                'role'=>$role,
                'display_name'=>$display_name,
                'module'=>$module,
                'sort_order'=>$sort_order
            );
            $this->db->insert($this->roles_table, $data);
            return $this->db->insert_id();
        }
    }
}

