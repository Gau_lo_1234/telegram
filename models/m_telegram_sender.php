<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * telegram_sender
 *
 * @package telegram
 * @author Elsabe Lessing (http://www.lessink.co.za/)
 */
class m_telegram_Sender extends CI_Model {
    private $table_name = 'telegram_sender';

    function __construct() {
        parent::__construct();
    }

    /**
     * get_list
     *
     * @access public
     *
     * @return array || false
     */
    function get_list() {
        $query = $this->db->get($this->table_name);
        if ($query && $query->num_rows() > 0) {
            return $query->result();
        } else {
            return FALSE;
        }
    }

    /**
     * get_by_client
     *
     * @param mixed $client_id
     *
     * @access public
     *
     * @return obj || false
     */
    function get_by_client($client_id) {
        $this->db->join($this->table_name, $this->table_name.'.telegram_sender_id = telegram_accounts.telegram_sender_id');
        $this->db->where('client_id', $client_id);
        $query = $this->db->get('telegram_accounts');
        if ($query && $query->num_rows() == 1) {
            return $query->row();
        }
        return FALSE;
    }

    /**
     * get_by_id
     *
     * @param mixed $id
     *
     * @access public
     *
     * @return obj || false
     */
    function get_by_id($id) {
        $this->db->where('telegram_sender_id', $id);
        $query = $this->db->get($this->table_name);
        if ($query && $query->num_rows() == 1) {
            return $query->row();
        }
        return FALSE;
    }

    /**
     * create
     *
     * @param mixed $country
     * @param mixed $key
     * @param mixed $key_pass
     * @access public
     *
     * @return int
     */
    function create($name, $key) {
        $data = array(
            'name'=> $name,
            'key'=> $key,
            'created'=> time(),
            'modified'=> time(),
            );

        if ($this->db->insert($this->table_name, $data)) {
            return $this->db->insert_id();
        }
        return 0;
    } // create

    /**
     * update
     *
     * @param mixed $telegram_sender_id
     * @param mixed $country
     * @param mixed $key
     * @param mixed $key_pass
     *
     * @access public
     *
     * @return boolean
     */
    function update($telegram_sender_id, $country, $key, $key_pass) {
        $data = array(
            'key'=> $key,
            'modified'=> time(),
            );

        $this->db->where('telegram_sender_id', $telegram_sender_id);
        if ($this->db->update($this->table_name, $data)) {
            return TRUE;
        }
        return FALSE;
    } // update

    /**
     * delete
     *
     * @param mixed $telegram_sender_id
     *
     * @access public
     *
     * @return void
     */
    function delete($telegram_sender_id) {
        $this->db->where('telegram_sender_id', $telegram_sender_id);
        $this->db->delete($this->table_name);
    } // delete
}
