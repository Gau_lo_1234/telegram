<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * m_telegram_Alerts
 *
 * @package telegram
 * @author Elsabe Lessing (http://www.lessink.co.za/)
 */
class m_telegram_Alerts extends CI_Model {
    private $table_name = 'telegram_alerts';

    function __construct() {
        parent::__construct();
        
        $this->load->helper('log');
    }


    
    /**
     * get_hourly_mailers
     *
     * @param int $report
     *
     * @access public
     *
     * @return array
     */
    function get_hourly_alerts($report=null) {

        /*
        SELECT  u.*, s.*, rt.*, ta.*, ta.telegram_alert_id
        as alert_id  FROM telegram_alerts ta
        JOIN users u ON ta.user_id=u.id
        JOIN sites s ON ta.site_id=s.id
        JOIN report_types rt ON ta.report_id = rt.id 
        where ta.frequency=4 and ta.report_id=3

        */
        $this->db->select('u.*');
        $this->db->select('s.*');
        $this->db->select('s.name siteName');
        $this->db->select('rt.*');
        $this->db->select('ta.*');
        $this->db->select('tc.chat_id');
        $this->db->select('ta.telegram_alert_id as alert_id');
        $this->db->from($this->table_name.' ta', FALSE);
        $this->db->join('users u', 'u.id = ta.user_id');
        $this->db->join('sites s', 's.id = ta.site_id');
        $this->db->join('report_types rt', 'rt.id = ta.report_id');
        $this->db->join('telegram_accounts tc', 'tc.user_id = ta.user_id');

        if (!is_null($report )) $this->db->where('ta.report_id', $report);
        $this->db->where('(ta.frequency=4 OR ta.frequency=5)');
        $query = $this->db->get();
       
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return array();
        }
    }

    /**
     * get_list
     *
     * @access public
     *
     * @return  array || false
     */
    function get_list() {
        $this->db->order_by('country', 'asc');

        $query = $this->db->get($this->table_name);
        if ($query && $query->num_rows() > 0) {
            return $query->result();
        } else {
            return FALSE;
        }
    }

    /**
     * get_list_by_site
     *
     *
     * @access public
     *
     * @return  array || false
     */
    function get_list_by_site($site_id, $client_id=null) {
        $this->db->select($this->table_name.".*, users.cellphone, users.username, telegram_accounts.chat_id", 'LEFT');
        $this->db->join('users', $this->table_name.".user_id = users.id");
        $this->db->join('telegram_accounts', 'telegram_accounts.user_id =users.id','LEFT');
        if ($site_id !== null) {
            $this->db->where('site_id', $site_id);
        }
        if (isset($client_id)) {
            $this->db->where('client_id', $client_id);
        }
        $this->db->order_by('user_id', 'asc');
        $query = $this->db->get($this->table_name);
        if ($query && $query->num_rows() > 0) {
            return $query->result();
        } else {
            return FALSE;
        }
    }

    /**
     * get_list_by_client
     *
     * @param mixed $client_id Description.
     *
     * @access public
     *
     * @return mixed Value.
     */
    function get_list_by_client($client_id, $report_id=null, $include_site= false) {
        $this->db->select($this->table_name.".*, users.cellphone, users.username");
        $this->db->join('users', $this->table_name.".user_id = users.id");

        if (!$include_site) {
            $this->db->where('site_id', 0);
            $this->db->join('sites', $this->table_name.'.site_id = sites.id', 'LEFT');
        } else {
            $this->db->select('sites.name site_name');
            $this->db->join('sites', $this->table_name.'.site_id = sites.id', 'LEFT');
        }
        if ($client_id) {
            $this->db->where($this->table_name.'.client_id', $client_id);
        }
        if ($report_id !== null) {
            $this->db->where('report_id', $report_id);
        }
        $this->db->order_by('user_id', 'asc');
        $query = $this->db->get($this->table_name);
        // echo $this->db->last_query();
        if ($query && $query->num_rows() > 0) {
            return $query->result();
        } else {
            return FALSE;
        }
    }

    /**
     * get_by_site_user_id
     *
     * @param int $user_id
     * @param int $site_id
     * @param int   $report_id
     *
     * @access public
     *
     * @return array || false
     */
    function get_by_site_user_id($user_id, $site_id = null, $report_id=0, $client_id=0) {
        $this->db->join('users', $this->table_name.".user_id = users.id", 'LEFT');
        $this->db->join('telegram_accounts', 'telegram_accounts.user_id =users.id','LEFT');
        $this->db->where($this->table_name.'.user_id', $user_id);

        if(!is_null($site_id)){
            $this->db->where($this->table_name.'.site_id', $site_id);
        }
        

        if ($report_id) {
            $this->db->where($this->table_name.'.report_id', $report_id);
        }
         if ($client_id) {
            $this->db->where($this->table_name.'.client_id', $client_id);
        }
        $query = $this->db->get($this->table_name);

        if ($query && $query->num_rows() > 0) {
            foreach($query->result() as $row) {
                    $data[$row->report_id][] = $row;
            }
        }
        if (isset($data)) return $data;
        return FALSE;
    }

    /**
     * get_alerts_to_handle
     *
     * @param int $site_id
     * @param int $report_id
     *
     * @access public
     *
     * @return array || false
     */
    function get_alerts_to_handle($site_id, $report_id) {
        $this->db->select('telegram_accounts.chat_id, telegram_alerts.*');
        $this->db->where('telegram_alerts.report_id', $report_id);
        $this->db->where('telegram_alerts.site_id', $site_id);
        $this->db->where('telegram_alerts.frequency', 5);
        $this->db->join('users', 'users.id = '.$this->table_name.'.user_id');
        $this->db->join('telegram_accounts', 'telegram_accounts.user_id =users.id');
        $query = $this->db->get($this->table_name);
      
        if ($query && $query->num_rows() > 0) {
            return $query->result();
        } else {
            return FALSE;
        }
    }

    /**
     * get_alerts_to_handle_client
     *
     * @param int $client_id
     * @param int $report_id
     *
     * @access public
     *
     * @return array || false
     */
    function get_alerts_to_handle_client($client_id, $report_id) {
        $this->db->select('telegram_accounts.chat_id, telegram_alerts.*');
        $this->db->where('telegram_alerts.report_id', $report_id);
        $this->db->where('telegram_alerts.client_id', $client_id);
        $this->db->where('telegram_alerts.frequency', 5);
        $this->db->join('users', 'users.id = '.$this->table_name.'.user_id');
        $this->db->join('telegram_accounts', 'telegram_accounts.user_id =users.id');
        $query = $this->db->get($this->table_name);
        
        
        if ($query && $query->num_rows() > 0) {
            return $query->result();
        } else {
            return FALSE;
        }
    }

    /**
     * get_daily_telegram_reports
     *
     * @param int $time
     *
     * @access public
     *
     * @return array
     */
    function get_daily_telegram_reports($time) {
        $this->db->select('users.*');
        $this->db->select('sites.*');
        $this->db->select('report_types.*');
        $this->db->select('telegram_accounts.chat_id');
        $this->db->select($this->table_name.'.*');
        // $this->db->select($this->table_name.'.id as mailer_id');

        $this->db->join('users', 'users.id = '.$this->table_name.'.user_id');
        $this->db->join('sites', 'sites.id = '.$this->table_name.'.site_id');
        $this->db->join('telegram_accounts', 'telegram_accounts.client_id =sites.client_id');
        $this->db->join('report_types', 'report_types.id = '.$this->table_name.'.report_id');
        $this->db->where($this->table_name.'.frequency', 1);
        $this->db->where($this->table_name.'.time', $time);
        $query = $this->db->get($this->table_name);
        // echo $this->db->last_query();
        if ($query && $query->num_rows() > 0) {
            return $query->result();
        } else {
            return array();
        }
    }

    /**
     * get_weekly_telegram_reports
     *
     * @param int $time
     * @param int $day_of_week
     *
     * @access public
     *
     * @return array
     */
    function get_weekly_telegram_reports($time, $day_of_week) {
        $this->db->select('users.*');
        $this->db->select('sites.*');
        $this->db->select('report_types.*');
        $this->db->select('telegram_accounts.chat_id');
        $this->db->select($this->table_name.'.*');
        // $this->db->select($this->table_name.'.id as mailer_id');

        $this->db->join('users', 'users.id = '.$this->table_name.'.user_id');
        $this->db->join('sites', 'sites.id = '.$this->table_name.'.site_id');
        $this->db->join('telegram_accounts', 'telegram_accounts.client_id =sites.client_id');
        $this->db->join('report_types', 'report_types.id = '.$this->table_name.'.report_id');
        $this->db->where($this->table_name.'.frequency', 2);
        $this->db->where('day_of_week', $day_of_week);
        $this->db->where('time', $time);
        $query = $this->db->get($this->table_name);
        // echo $this->db->last_query();
        if ($query && $query->num_rows() > 0) {
            return $query->result();
        } else {
            return array();
        }
    }


    /**
     * get_monthly_telegram_reports
     *
     * @param int $time
     * @param int $day_of_month
     *
     * @access public
     *
     * @return array
     */
    function get_monthly_telegram_reports($time, $day_of_month) {
        $this->db->select('users.*');
        $this->db->select('sites.*');
        $this->db->select('report_types.*');
        $this->db->select('telegram_accounts.chat_id');
        $this->db->select($this->table_name.'.*');
        // $this->db->select($this->table_name.'.id as mailer_id');

        $this->db->join('users', 'users.id = '.$this->table_name.'.user_id');
        $this->db->join('sites', 'sites.id = '.$this->table_name.'.site_id');
        $this->db->join('telegram_accounts', 'telegram_accounts.client_id =sites.client_id');
        $this->db->join('report_types', 'report_types.id = '.$this->table_name.'.report_id');
        $this->db->where($this->table_name.'.frequency', 3);
        $this->db->where('day_of_month', $day_of_month);
        $this->db->where('time', $time);
        $query = $this->db->get($this->table_name);
        // echo $this->db->last_query();
        if ($query && $query->num_rows() > 0) {
            return $query->result();
        } else {
            return array();
        }
    }

    /**
     * get_hourly_telegram_reports
     *
     * @param int $report (optional)
     *
     * @access public
     *
     * @return array
     */
    function get_hourly_telegram_reports($report=null) {
     
        $this->db->select('users.*');
        $this->db->select('sites.*');
        $this->db->select('report_types.*');
        $this->db->select('telegram_accounts.chat_id');
        $this->db->select($this->table_name.'.*');
        // $this->db->select($this->table_name.'.id as mailer_id');

        $this->db->join('users', 'users.id = '.$this->table_name.'.user_id');
        $this->db->join('sites', 'sites.id = '.$this->table_name.'.site_id');
        $this->db->join('telegram_accounts', 'telegram_accounts.client_id =sites.client_id');
        $this->db->join('report_types', 'report_types.id = '.$this->table_name.'.report_id');
        if (!is_null($report )) $this->db->where($this->table_name.'.report_id', $report);
        $this->db->where($this->table_name.'.frequency', 4);
        $query = $this->db->get($this->table_name);

       
        if ($query && $query->num_rows() > 0) {
          
            return $query->result();
        } else {
            return array();
        }
    }

    /**
     * get_after_upload_telegram_reports
     *
     * @param int $site_id
     *
     * @access public
     *
     * @return array
     */
    function get_after_upload_telegram_reports($site_id) {
        $this->db->select('telegram_accounts.chat_id');
        $this->db->select('users.*, report_types.*, sites.*, '.$this->table_name.'.*, '.$this->table_name.'.id as site_email_id');
        $this->db->join('users', 'users.id = '.$this->table_name.'.user_id');
        $this->db->join('report_types', 'report_types.id = '.$this->table_name.'.report_id');
        $this->db->join('sites', 'sites.id = '.$this->table_name.'.site_id');
        $this->db->join('telegram_accounts', 'telegram_accounts.client_id =sites.client_id');
        $this->db->where($this->table_name.'.frequency', 6);
        $this->db->where('site_id', $site_id);
        $query = $this->db->get($this->table_name);
        // echo $this->db->last_query();
        if ($query && $query->num_rows() > 0) {
            return $query->result();
        } else {
            return array();
        }
    }

    /**
     * get_by_id
     *
     * @param int $id
     *
     * @access public
     *
     * @return obj || false
     */
    function get_by_id($id) {
        $this->db->where('telegram_alert_id', $id);
        $query = $this->db->get($this->table_name);
        if ($query && $query->num_rows() == 1) {
              return $query->row();
          }
          return FALSE;
      }

    /**
     * create
     *
     * @param int $client_id
     * @param int $site_id
     * @param int $report_id
     * @param int $user_id
     * @param int   $frequency
     *
     * @access public
     *
     * @return boolean
     */
    function create($client_id, $site_id, $report_id, $user_id, $frequency=0, $time=0, $day_of_week=0, $day_of_month=0, $settings=null) {
       
     
        $err = false;
        $s = '';
        //check if it exists.
        if(isset($site_id) && !empty($site_id)){
            $this->db->where('site_id', $site_id);
        }
        else{
            $site_id=0;
        }
       
        $this->db->where('client_id', $client_id);
        $this->db->where('user_id', $user_id);
        $this->db->where('report_id', $report_id);
        $query = $this->db->get($this->table_name);
        //echo $this->db->last_query();
       
        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
    if(isset($row->settings)):
                $s = (array)json_decode($row->settings);
                if(!is_null($settings)){
                 if ($s == $settings) {
                    $err = true;
                    break;
                }                   
                }
                endif;


            }
        }
        if ($err) {
            // $this->error = 'Duplicate mailer';
            return FALSE;
        }
        // if ($query && $query->num_rows() == 1) {
        //     $this->error = 'Email recipient is already added to this site';
        //     return FALSE;
        // }
        //So not captured yet so lets go on.

        $data = array(
            'client_id' => $client_id,
            'site_id' => isset($site_id)?$site_id:0,
            'report_id' => $report_id,
            'user_id' => $user_id,
            'frequency' => $frequency,
            'time' => $time,
            'day_of_week' => $day_of_week,
            'day_of_month' => $day_of_month
        );
        print_r($data );
        if ($settings != null) {
            $data['settings'] = json_encode($settings);
        }
     
        if ($this->db->insert($this->table_name, $data)) {
            return TRUE;
        }
        //echo $this->db->last_query();

        return FALSE;
    }

    /**
     * update
     *
     * @param int $client_id
     * @param int $site_id
     * @param int $report_id
     * @param int $user_id
     * @param int   $frequency
     *
     * @access public
     *
     * @return boolean
     */
    function update($client_id, $site_id, $report_id, $user_id, $frequency=0, $time=0, $day_of_week=0, $day_of_month=0, $settings=null) {
        //check if it exsits...
        $update = false;
        $update_id = false;

        // check settings match....
        $result = $this->get_by_site_user_id($user_id, $site_id, $report_id, $client_id);
        if ($result) {
            if ($settings !== null) {
                foreach ($result as $report_id=>$alerts) {
                    foreach($alerts as $alert ) {
                        if ($alert->settings) {
                            $s = (array)json_decode($alert->settings);
                            if ($s == $settings) {
                                $update_id = $alert->telegram_alert_id;
                                $update = true;
                                break;
                            }
                        }
                    }
                }
            } else {
                $update = true;
            }
        }// result

        if ($update) {
            $data = array(
                'frequency' => $frequency,
                'time' => $time,
                'day_of_week' => $day_of_week,
                'day_of_month' => $day_of_month
                );
            if ($settings != null) {
                $data['settings'] = json_encode($settings);
            }
            if ($update_id) {
                $this->db->where('telegram_alert_id', $update_id);
            } else {
                $this->db->where('user_id', $user_id);
                $this->db->where('site_id', $site_id);
                $this->db->where('report_id', $report_id);
                $this->db->where('client_id', $client_id);
            }
            if ($this->db->update($this->table_name, $data)) {
                return TRUE;
            }
        } else {
            if ($frequency != 0) {
                // echo 'CREATE';
                return $this->create($client_id, $site_id, $report_id, $user_id, $frequency, $time, $day_of_week, $day_of_month, $settings);
            } else {
                // echo 'TRUE';
                return TRUE;
            }
        }

        return FALSE;
    }

    /**
     * delete
     *
     * @param int $id
     *
     * @access public
     *
     * @return void
     */
    function delete($id) {
        $this->db->where('telegram_alert_id', $id);
        $this->db->delete($this->table_name);
    } // delete

    /**
     * delete_by_client
     *
     * @param int $client_id
     *
     * @access public
     *
     * @return void
     */
    function delete_by_client($client_id) {
        $this->db->where('client_id', $client_id);
        $this->db->delete($this->table_name);
    } // delete

/**
     * delete_by_client
     *
     * @param int $site_id
     *
     * @access public
     *
     * @return void
     */
    function delete_by_site($site_id) {
        $this->db->where('site_id', $client_id);
        $this->db->delete($this->table_name);
    } // delete

    /**
     * update_last_sent
     *
     * @param int $id
     *
     * @access public
     *
     * @return void
     */
    function update_last_sent($id) {
        $this->db->where('telegram_alert_id', $id);
        $this->db->update($this->table_name, array('last_sent'=>time()));
    }
}
