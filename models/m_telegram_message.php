<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * m_telegram_Message
 *
 * @package telegram
 * @author Elsabe Lessing (http://www.lessink.co.za/)
 */
class m_telegram_Message extends CI_Model {
    private $table_name = 'telegram_messages';

    function __construct() {
        parent::__construct();
        $this->load->helper('log');
    }

    /**
     * get_list
     *
     * @param int $limit optional
     * @param int $start optional
     *
     * @access public
     *
     * @return array || FALSE
     */
    function get_list($limit=0, $start = 0, $start_date = null, $end_date = null) {
        if (isset($start_date) ) {
            $this->db->where('created_date >', $start_date);
        }
        if (isset($end_date)) {
            $this->db->where('created_date <', $end_date);
        }
        if ($limit) {
            $query = $this->db->get($this->table_name, $limit, $start);
        } else {
            $query = $this->db->get($this->table_name);
        }

        // echo $this->db->last_query();
        if ($query && $query->num_rows() > 0) {
            return $query->result();
        } else {
            return FALSE;
        }
    }

    /**
     * get_list_by_client_total
     *
     * @param int $client_id
     *
     * @access public
     *
     * @return int.
     */
    function get_list_by_client_total($client_id) {
        $this->db->where('client_id', $client_id);
        return $this->db->count_all_results($this->table_name);
    }

    /**
     * get_list_by_client
     *
     * @param int $client_id
     * @param int   $limit     Optional.
     * @param int   $start     Optional.
     *
     * @access public
     *
     * @return array || FALSE
     */
    function get_list_by_client($client_id, $limit=0, $start = 0, $start_date = null, $end_date = null) {
        $this->db->order_by('created_date', 'desc');
        $this->db->where('client_id', $client_id);
        return $this->get_list($limit, $start, $start_date, $end_date);
    }

    /**
     * get_by_id
     *
     * @param int $id
     *
     * @access public
     *
     * @return obj || FALSE
     */
    function get_by_id($id) {
        $this->db->where('telegram_message_id', $id);
        $query = $this->db->get($this->table_name);
        if ($query && $query->num_rows() == 1) {
            return $query->row();
        }
        return FALSE;
    }

    /**
     * create
     *
     * @param int $telegram_sender_id     .
     * @param int $client_id         .
     * @param int $user_id           .
     * @param string $number            .
     * @param string $message           .
     * @param string $delivery_response .
     * @param string $status            .
     *
     * @access public
     *
     * @return int
     */
    function create($telegram_sender_id=null, $client_id=null, $user_id=null, $number=null, $message=null, $delivery_response=null, $status=null) {
        $data = array(
            'telegram_sender_id' => isset($telegram_sender_id)?$telegram_sender_id:0,
            'client_id'=> isset($client_id)?$client_id:0,
            'user_id'=> isset($user_id)?$user_id:0,
            'number'=> isset($number)?$number:0,
            'message'=> isset($message)?$message:"",
            'delivery_response'=> isset($delivery_response)?$delivery_response:"",
            'created_date'=> time(),
            'status'=>isset($status)?$status:0
        );

        if ($this->db->insert($this->table_name, $data)) {
            write_log($this->db->last_query());
            return $this->db->insert_id();
        }else{
            return 0;

        }
      
    } // create

    /**
     * delete
     *
     * @param int $telegram_msg_id
     *
     * @access public
     *
     * @return void
     */
    function delete($telegram_msg_id) {
           $this->db->where('telegram_message_id', $telegram_msg_id);
           $this->db->delete($this->table_name);
    } // delete

    /**
     * delete_by_client
     *
     * @param int $client_id
     *
     * @access public
     *
     * @return void
     */
    function delete_by_client($client_id) {
           $this->db->where('client_id', $client_id);
           $this->db->delete($this->table_name);
    } // delete
}
