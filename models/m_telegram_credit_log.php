<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * telegram Credits Log
 *
 * @package telegram
 * @author Elsabe Lessing (http://www.lessink.co.za/)
 */
class m_telegram_credit_log extends CI_Model {
    var $table_name = 'telegram_credits_log';

    /**
     * get_list_total
     *
     * @param mixed $telegram_account_id
     * @param mixed $start_date
     * @param mixed $end_date
     *
     * @access public
     *
     * @return mixed Value.
     */
    function get_list_total($telegram_account_id, $start_date, $end_date) {
        $this->db->where('telegram_account_id', $telegram_account_id);
        $this->db->join('users', 'users.id = '.$this->table_name.'.user_id');
        $this->db->join('sites', 'sites.id = '.$this->table_name.'.site_id');
        $this->db->where('date >=', $start_date);
        $this->db->where('date <=', $end_date);
        return $this->db->count_all_results($this->table_name);;
    }

    /**
     * get_list
     *
     * @param mixed $telegram_account_id
     * @param mixed $start_date
     * @param mixed $end_date
     * @param int   $limit
     * @param int   $start
     *
     * @access public
     *
     * @return mixed Value.
     */
    function get_list($telegram_account_id, $start_date, $end_date, $limit=0, $start = 0) {
        $this->db->where('telegram_account_id', $telegram_account_id);
        $this->db->join('users', 'users.id = '.$this->table_name.'.user_id', 'left');
        $this->db->join('sites', 'sites.id = '.$this->table_name.'.site_id');
        $this->db->where('date >=', $start_date);
        $this->db->where('date <=', $end_date);
        $this->db->order_by('date', 'desc');
        if ($limit) {
            $query = $this->db->get($this->table_name, $limit, $start);
        } else {
            $query = $this->db->get($this->table_name);
        }
        return $query->result();
    }

    /**
     * get_distributor_list_total
     *
     * @param mixed $distributor_id
     * @param mixed $start_date
     * @param mixed $end_date
     *
     * @access public
     *
     * @return mixed Value.
     */
    function get_distributor_list_total($distributor_id, $start_date, $end_date) {
        $this->db->select('telegram_credits_log.credits, telegram_credits_log.date, clients.name, users.username, users.email');
        $this->db->join('telegram_accounts', 'telegram_accounts.telegram_account_id = '.$this->table_name.'.telegram_account_id');
        $this->db->where('telegram_accounts.distributor_id', $distributor_id);
        $this->db->join('clients', 'clients.id = telegram_accounts.client_id', 'left');
        $this->db->join('users', 'users.id = '.$this->table_name.'.user_id', 'left');
        $this->db->where('date >=', $start_date);
        $this->db->where('date <=', $end_date);
        return $this->db->count_all_results($this->table_name);
    }

    /**
     * get_distributor_list
     *
     * @param mixed $distributor_id
     * @param mixed $start_date
     * @param mixed $end_date
     * @param int   $limit
     * @param int   $start
     *
     * @access public
     *
     * @return mixed Value.
     */
    function get_distributor_list($distributor_id, $start_date, $end_date, $limit=0, $start=0) {
        $this->db->select('telegram_credits_log.credits, telegram_credits_log.date, clients.name, users.username, users.email');
        $this->db->join('telegram_accounts', 'telegram_accounts.telegram_account_id = '.$this->table_name.'.telegram_account_id');
        $this->db->where('telegram_accounts.distributor_id', $distributor_id);
        $this->db->join('clients', 'clients.id = telegram_accounts.client_id', 'left');
        $this->db->join('users', 'users.id = '.$this->table_name.'.user_id', 'left');
        $this->db->where('date >=', $start_date);
        $this->db->where('date <=', $end_date);
        if ($limit) {
            $query = $this->db->get($this->table_name, $limit, $start);
        } else {
            $query = $this->db->get($this->table_name);
        }
        if ($query && $query->num_rows() > 0) {
            return $query->result();
        } else {
            return FALSE;
        }
    }

    /**
     * log_credits
     *
     * @param mixed $telegram_account_id
     * @param mixed $user_id
     * @param mixed $credits
     * @param mixed $target_account_id
     * @param mixed $site_id
     *
     * @access public
     *
     * @return mixed Value.
     */
    function log_credits($telegram_account_id, $user_id, $credits, $target_account_id=-1, $site_id=-1) {
        $data = array(
            'telegram_account_id' => $telegram_account_id,
            'site_id'=>$site_id,
            'user_id'=> $user_id,
            'target_account_id'=>$target_account_id,
            'credits'=> $credits,
            'date'=>time()
        );
        $this->db->insert($this->table_name, $data);
    }

    /**
     * delete
     *
     * @param mixed $telegram_account_id
     *
     * @access public
     *
     * @return mixed Value.
     */
    function delete($telegram_account_id) {
        $this->db->where('telegram_account_id', $telegram_account_id);
        $this->db->delete($this->table_name);
    }

    /**
     * get_credits_used
     *
     * @param mixed $telegram_account_id
     * @param mixed $start
     * @param mixed $end
     * @param mixed $group_by_site
     *
     * @access public
     *
     * @return mixed Value.
     */
    function get_credits_used($telegram_account_id, $start, $end, $group_by_site = false) {
        if ($start && $end) {
            if ($start > $end) {
                $tmp = $start;
                $start = $end;
                $end=$tmp;
            }
        }
        if ($start) {
            $this->db->where('date >', $start);
        }
        if ($end) {
            $this->db->where('date <', $end);
        }

        $this->db->select_sum('credits');
        $this->db->where('telegram_account_id',$telegram_account_id);

        if ($group_by_site) {
            $this->db->select('site_id');
            $this->db->select('name');
            $this->db->join('sites', 'sites.id = '.$this->table_name.'.site_id');
            $this->db->group_by('site_id');
            $query = $this->db->get($this->table_name);
            $data = array();
            foreach ($query->result() as $row) {
                $data[$row->site_id] = $row;
            }
            return $data;
        } else {
            $query = $this->db->get($this->table_name);
            if ($query && $query->num_rows() > 0) {
                $row = $query->row();
                return abs($row->credits);
            } else {
                return 0;
            }
        }
        return 0;
    }

    /**
     * get_total_telegram_sent_distributors
     *
     * @param int $start
     * @param int $end
     *
     * @access public
     *
     * @return mixed Value.
     */
    function get_total_telegram_sent_distributors($start=0, $end=0) {
        if ($start && $end) {
            if ($start > $end) {
                $tmp = $start;
                $start = $end;
                $end=$tmp;
            }
        }
        if ($start) {
            $this->db->where('date >', $start);
        }
        if ($end) {
            $this->db->where('date <', $end);
        }

        $this->db->select('distributors.name');
        $this->db->select_sum($this->table_name.'.credits');
        $this->db->group_by('telegram_accounts.distributor_id');
        $this->db->join('telegram_accounts', 'telegram_accounts.telegram_account_id = '.$this->table_name.'.telegram_account_id');
        $this->db->join('distributors', 'distributors.id = telegram_accounts.distributor_id');
        $this->db->where('user_id', 0);
        $query = $this->db->get($this->table_name);

        if ($query && $query->num_rows() > 0) {
            return $query->result();
        } else {
            return FALSE;
        }
    }

    /**
     * get_total_telegram_sent
     *
     * @param int $start
     * @param int $end
     *
     * @access public
     *
     * @return mixed Value.
     */
    function get_total_telegram_sent($start=0,$end=0){
        if ($start && $end) {
            if ($start > $end) {
                $tmp = $start;
                $start = $end;
                $end=$tmp;
            }
        }
        if ($start) {
            $this->db->where('date >', $start);
        }
        if ($end) {
            $this->db->where('date <', $end);
        }

        $this->db->select_sum('credits');

        $this->db->where('user_id', 0);
        $query = $this->db->get($this->table_name);
        if ($query && $query->num_rows() > 0) {
            $row = $query->row();
            return $row->credits;
        } else {
            return FALSE;
        }
    }
}
