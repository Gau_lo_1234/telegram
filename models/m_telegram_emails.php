<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * telegram Emails
 *
 * @package telegram
 * @author Elsabe Lessing (http://www.lessink.co.za/)
 */
class m_telegram_emails extends CI_Model {
    const telegram_EMAIL_LOWCREDITS = 1;
    const telegram_EMAIL_STATEMENT = 2;
    var $table_name = 'telegram_emails';

    function get_by_telegram_account($telegram_account_id, $type=null) {
        $this->db->where('telegram_account_id', $telegram_account_id);
        if (!is_null($type)) {
            $this->db->where('type', $type);
        }
        $this->db->join('users', 'users.id = '.$this->table_name.'.user_id');
        $query = $this->db->get($this->table_name);
        return $query->result();
    }

    function get_by_user($telegram_email_id, $user_id, $type=null) {
        $this->db->where('telegram_email_id', $telegram_email_id);
        $this->db->where('user_id', $user_id);
        if (!is_null($type)) { $this->db->where('type', $type); }
        $this->db->join('users', 'users.id = '.$this->table_name.'.user_id');
        $query = $this->db->get($this->table_name);
        return $query->row();
    }

    function get_by_id($telegram_email_id, $type=null) {
        $this->db->where('telegram_email_id', $telegram_email_id);
        if (!is_null($type)) { $this->db->where('type', $type); }
        $this->db->join('users', 'users.id = '.$this->table_name.'.user_id');
        $query = $this->db->get($this->table_name);
        return $query->row();
    }

    function add($telegram_account_id, $user_id, $type, $frequency) {
            $data['telegram_account_id'] = $telegram_account_id;
            $data['user_id'] = $user_id;
            $data['type'] = $type;
            $data['frequency'] = $frequency;
            return $this->db->insert($this->table_name, $data);
    }

    function delete($telegram_email_id) {
        $this->db->where('telegram_email_id', $telegram_email_id);
        return $this->db->delete($this->table_name);
    }

    function delete_by_account($telegram_account_id) {
        $this->db->where('telegram_account_id', $telegram_account_id);
        return $this->db->delete($this->table_name);
    }

    function update_last_sent($id) {
        $this->db->where('telegram_email_id', $id);
        $this->db->update($this->table_name, array('last_sent'=>time()));
    }

}

