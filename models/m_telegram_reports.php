  <?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * telegram_reports
 *
 * @package telegram
 * @author Elsabe Lessing (http://www.lessink.co.za/)
 */
class m_telegram_Reports extends CI_Model
{
    private $table_name = 'telegram_reports';

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * get_list
     *
     * @access public
     *
     * @return array || false
     */
    public function get_list($active=null)
    {
        $this->db->join('report_types', 'report_types.id = '.$this->table_name.'.report_id');
        if ($active !== null) {
            $this->db->where('active', $active);
        }
        $this->db->order_by('sort_order', 'asc');
        $query = $this->db->get($this->table_name);
        if ($query && $query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }

    /**
     * get_grouped_array
     *
     * @access public
     *
     * @return mixed Value.
     */
    public function get_grouped_array($active=null)
    {
        $this->db->join('report_types', 'report_types.id = '.$this->table_name.'.report_id');
        if ($active !== null) {
            $this->db->where('active', $active);
        }
        $this->db->order_by('sort_order', 'asc');
        $query = $this->db->get($this->table_name);
        if ($query && $query->num_rows() > 0) {
            //echo $this->db->last_query();
            //exit();
            //index accounts table,
            if ($query && $query->num_rows() > 0) {
                foreach ($query->result() as $row) {
                    $result[$row->group][$row->id] = $row;
                }
            }
            return $result;
        }
    }
        /**
         * get_by_report
         *
         * @param mixed $report_id
         *
         * @access public
         *
         * @return obj || false
         */
        function get_by_report($report_id)
        {
            $this->db->where('report_id', $report_id);
            $query = $this->db->get($this->table_name);
            if ($query && $query->num_rows() == 1) {
                return $query->row();
            }
            return false;
        }

  
        /**
           * get_by_id
           *
           * @param mixed $id
           *
           * @access public
           *
           * @return obj || false
           */
        function get_by_id($id)
        {
            $this->db->where('telegram_template_id', $id);
            $query = $this->db->get($this->table_name);
            if ($query && $query->num_rows() == 1) {
                return $query->row();
            }
            return false;
        }

        /**
         * create
         *
         * @param mixed $report_id
         * @param mixed $text
         *
         * @access public
         *
         * @return int
         */
        function create($report_id, $text)
        {
            $data = array(
            'report_id'=>$report_id,
            'text'=>$text,
        );

            if ($this->db->insert($this->table_name, $data)) {
                return $this->db->insert_id();
            }
            return 0;
        } // create

        /**
         * update
         *
         * @param mixed $telegram_template_id
         * @param mixed $report_id
         * @param mixed $text
         *
         * @access public
         *
         * @return bool
         */
        function update($telegram_template_id, $report_id, $text)
        {
            $data = array(
            'report_id'=>$report_id,
            'text'=>$text,
        );
            $this->db->where('telegram_template_id', $telegram_template_id);
            if ($this->db->update($this->table_name, $data)) {
                return true;
            }
            return false;
        } // update

        /**
         * delete
         *
         * @param mixed $telegram_template_id
         *
         * @access public
         *
         * @return void
         */
        function delete($telegram_template_id)
        {
            $this->db->where('telegram_template_id', $telegram_template_id);
            $this->db->delete($this->table_name);
        } // delete

}