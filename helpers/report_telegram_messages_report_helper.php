<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if (!function_exists("get_data_telegram_messages_report")) {
    /**
     * get_data_telegram_messages_report
     *
     * This function fetches the clocking data for a detailed report.
     *
     * @param int  $client_id   Client ID.
     * @param int  $start_gmt unix timestamp
     * @param int  $end_gmt   unix timestamp
     * @param string $tz        timezone
     * @param array $settings   An array with additional settings. For this report: 'point_group', 'simple', 'order_by'
     *
     * @access public
     *
     * @return mixed Value.
     */
function get_data_telegram_messages_report($client_id, $start_gmt, $end_gmt, $tz='', $settings=array() ) { //$simple = false, $group = null, $point_codes=null) {
        $ci = &get_instance();
        $ci->load->model('telegram/m_telegram_message');

        $telegram_messages = $ci->m_telegram_message->get_list_by_client($client_id, 0, 0, $start_gmt, $end_gmt);

        return $telegram_messages;
}
}
function tele_test(){
    echo "working";
}

if (!function_exists("alerts_inactivity")) {
    /**
     * alerts_inactivity
     *
     * @param int  $now               Unix timestamp.
     * @param string $identifier     The identifier for the cron lock.
     * @param string $log_prefix    Prefix used in the log filename.
     *
     * @access public
     *
     * @return void
     */
    function alerts_inactivity($now=null, $identifier="inactivity_alert", $log_prefix='inactivity', $img_path=null) {
        $CI =& get_instance();
        $CI->load->model('telegram/m_telegram_alerts');
        $CI->load->model('m_report_type');
        $CI->load->helper('cron');
        $CI->load->config('telegram/telegram');

       

        if(($pid = CronHelper::lock($identifier)) !== FALSE) {
            $now = $now == null ? time() : $now;

            $log_message = "Inactivty Alerts Started: ".date('Y-m-d H:i:s', $now)."\n";
 

            $report = $CI->m_report_type->get_by_handle('site_inactive');
           
           

            //get inactive site alerts
            $alerts = $CI->m_telegram_alerts->get_hourly_alerts($report->id);

           

            $three_hours_ago = $now - 10800; //3 hours
            $one_hour_ago = $now - 3000; //1 hours  make it 50 minutes...

            $current_language = "";
            $CI->lang->load('og', 'english');
            foreach ($alerts as $alert) {
                    $log_message .= "User: \t".$alert->user_id."\n";
                    $log_message .= "Last sent: \t".date("Y-m-d H:i:s", $alert->last_signal_received)."\n";
                    $log_message .= "Email: \t".$alert->email;

                    // check that we can send mails to the users

                      
            
                        if($alert->last_sent!=0){
                             $last_sent = strtotime($alert->last_sent);

                        }else{
                            $last_sent = $alert->last_sent;
                        }
                       
                       

                        // Only send if site has been inactive for more than 3 hours. and only send 1 email in an hour.
                        if ($alert->last_signal_received < $three_hours_ago && $last_sent < $one_hour_ago) {
                            // echo $alert->email." ";

                            // okay site is inactive..... blargh.

                            /* TELEGRAM ALERT */
                            $subject = sprintf($CI->lang->line('inactive_alert_subject'), $alert->name);

                            $data['site_name']  = $CI->config->item('website_name', 'tank_auth');
                            $data['site'] = $alert->name;
                            $data['Last Sent'] = $alert->last_sent;
                            $data['last_received'] = (is_null($alert->last_signal_received) ? 'never' : date('Y-m-d H:i:s',gmt_to_local($alert->last_signal_received, $alert->timezone)));
                          
                            $chat_id=$alert->chat_id;
                            $alt_message = "The site, ". $alert->siteName.", has been inactive since ". $data['last_received'];
                            $CI->load->library('telegram/telegram_sender');
                            $CI->m_telegram_alerts->update_last_sent($alert->telegram_alert_id);
                            
                           
                            $CI->telegram_sender->send_message($chat_id,  $alt_message);
                            try{
                                $CI->load->model('telegram/m_telegram_message');

                            $CI->m_telegram_message->create($chat_id, $alert->client_id, $alert->user_id, null, $alt_message);

                        }catch(Exception $ex){
                            $ex->get_Message();
                        }

                              

                           
                        }
                   
            }//foreach

            $log_message .= 'Done.';
            //commented out 27_03_2018 Terence Bruwer Magtouch Electronics
            //$filename = APPPATH.'/logs/'.$log_prefix.'_'.date('m_Y').'.php';
            //$filename = APPPATH.'/logs/'.$log_prefix.'_'.date('m_Y').'.log';
            //write_log($log_message);
            CronHelper::unlock($identifier);
           
         }
    }
}


if (!function_exists("generate_pdf_telegram_messages_report")) {
    /**
     * generate_pdf_detail
     *
     * @param array  $clockings  Report data, see get_data_detail
     * @param obj   $site Site object
     * @param string  $start_gmt Start date string in gmt
     * @param string  $end_gmt   End date string in gmt
     * @param string $tz    Timezone string
     * @param string  $filename   Filename of tmp PDF.
     * @param string $output     Ouput to file ( F ) or stream to browser ( I ). See TCPDF doc for options.
     * @param string $img_path   Custom path to images.
     * @param array $settings   An array with additional settings. For this report: 'point_group', 'simple', 'order_by'
     *
     * @access public
     *
     * @return outputs PDF type depends on $output param
     */
function generate_pdf_telegram_messages_report($messages, $client, $start_gmt, $end_gmt, $tz='', $filename='tmp.pdf', $output='I', $img_path="", $settings=array() ) { //$simple=false, $order_by='upload') {
        $ci = &get_instance();

        if (!isset($ci->mpdf)) { $ci->load->library('mpdf'); }

/*
class mPDF ([ string $mode [, mixed $format [, float $default_font_size [, string $default_font
[, float $margin_left , float $margin_right , float $margin_top , float $margin_bottom , float $margin_header , float $margin_footer [, string $orientation ]]]]]])
*/
        $mpdf=new mPDF('c','A4','','',10,10,10,20,16,13);
        $mpdf->SetDisplayMode('fullpage');
        $mpdf->setFooter('{PAGENO} / {nb}');
        $mpdf->list_indent_first_level = 0; // 1 or 0 - whether to indent the first level of a list

        $site_name = $site->name;


        $start = date('Y-m-d', gmt_to_local($start_gmt, $tz));
        $end = date('Y-m-d', gmt_to_local($end_gmt, $tz));
        
        /*
        $start = date('Y-m-d', strtotime($start_gmt));
        $end = date('Y-m-d', strtotime($end_gmt));
*/
        $logo_path = $img_path.'images/logo2.png';
        // Set some content to print
        $html = <<<EOD
        <img src="$logo_path"><br>
        <hr/>
        <h1>Telegram Messages Sent Report for: {$client->name}</h1>
        <p>From  <b>$start</b>  to  <b>$end</b>  </p>
EOD;
         // Set some content to print
        $html .= $ci->load->view('telegram/telegram_messages_report/telegram_messages_report', array('telegram_messages'=>$messages), true);
        $css = file_get_contents(ASSET_PATH.'css/report_style.css');

        $mpdf->WriteHTML($css,1); // The parameter 1 tells that this is css/style only and no body/html/text
        $mpdf->WriteHTML($html);
        $mpdf->Output($filename, $output);
}
}//if exists


if (!function_exists("generate_html_telegram_messages_report")) {
    /**
     * generate_html_detail
     *
     * @param array  $clockings  Report data, see get_data_detail
     * @param obj   $site Site object
     * @param string  $start_gmt Start date string in gmt
     * @param string  $end_gmt   End date string in gmt
     * @param string $tz    Timezone string
     * @param string $img_path   Custom path to images.
     * @param array $settings   An array with additional settings. For this report: 'point_group', 'simple', 'order_by'
     *
     * @access public
     *
     * @return HTML string
     */
function generate_html_telegram_messages_report($messages, $client, $start_gmt, $end_gmt, $tz='', $img_path="", $settings=array() ) {
        $ci = &get_instance();

        if (!isset($ci->m_point_group)) { $ci->load->model('m_point_group'); }

        $start = date('Y-m-d', gmt_to_local($start_gmt, $tz));
        $end = date('Y-m-d', gmt_to_local($end_gmt, $tz));

         // Set some content to print
        $ci->layout->setLayout('layouts/html_report');
        $report_header = '<img src="'.data_uri($img_path.'images/logo2.png', 'image/png').'"><br>
        <h1>Telegram Messages Sent Report for: '.$client->name.'</h1>
        <p>From  <b>'.$start.'</b>  to  <b>'.$end.'</b>  </p>';

        $css = file_get_contents(ASSET_PATH.'css/report_style.css');

        $html = $ci->layout->view('telegram/telegram_messages_report/telegram_messages_report', array('css'=>$css, 'report_header'=>$report_header, 'telegram_messages'=>$messages, 'tz'=>$tz), true);
        return $html;
}
}//if exists

if (!function_exists("generate_download_telegram_messages_report")) {
    /**
     * generate_download_detail
     *
     * @param array  $messages  telegram messages
     * @param obj   $client Client object
     * @param string  $start_gmt Start date string in gmt
     * @param string  $end_gmt   End date string in gmt
     * @param string $tz    Timezone string
     * @param string  $filename   Filename of tmp PDF.
     * @param string $output     Ouput to file ( F ) or stream to browser ( I ). See TCPDF doc for options.
     * @param string $img_path   Custom path to images.
     * @param array $settings   An array with additional settings. For this report: 'point_group', 'simple', 'order_by'
     *
     * @access public
     *
     * @return mixed Value.
     */
function generate_download_telegram_messages_report($messages, $client, $start_gmt, $end_gmt, $tz='', $filename='tmp.pdf', $output='I', $img_path="", $settings=array() ) {
    if (count($messages) < 50) {
        generate_pdf_telegram_messages_report($messages, $client, $start_gmt, $end_gmt, $tz, $filename.'.pdf', $output, $img_path, $settings);
        return $filename.'.pdf';
    } else {
        $html = generate_html_telegram_messages_report($messages, $client, $start_gmt, $end_gmt, $tz, $img_path, $settings);
        // write html to file...
        $fh = fopen($filename.'.html', 'w');
        if ($fh) {
            fwrite($fh, $html);
            fclose($fh);
        }

        if ((isset($settings['cron']) && $settings['cron']) || (isset($settings['mailer']) && $settings['mailer'])) {
            return $filename.'.html';
        } else {
            return site_url('download/html/'.str_replace('tmp/', '', $filename));
        }
    }
}
}