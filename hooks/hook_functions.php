<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


/**
 * Contains all functions used for hooks. Hooks are specified inhand
 * config/hooks.php
 *
 * @package telegram
 * @author Elsabe Lessing (http://www.lessink.co.za/)
 */

/**
 * List widget that gets called from the Sites page.
 */
function telegram_list_site_widget() {
    $CI = &get_instance();
    $arg_list = func_get_args();
    if (check_site_active('telegram', $arg_list[1]['site_id'])) {
        echo Modules::run('telegram/list_site_widget', $arg_list[1]['site_id']);
    }
}

/**
 *  Show a button and prompt to allow telegram setup!
 */
function telegram_site_activity() {
    $CI = &get_instance();
    ///echo Modules::run('telegram_crons/hourly_site_inctivity');
   
}
function telegram_site_activity_never() {
    $CI = &get_instance();
    $arg_list = func_get_args(); 
    if (isset($arg_list[1]['site_code'])) {
        echo Modules::run('telegram/site_telegram_config_prompt', $arg_list[1]['site_id'], $arg_list[1]['site_code']);
    }
}

/**
 * List widget that gets called from the clients page.
 */
function telegram_list_clients_widget() {
    $CI = &get_instance();
    $arg_list = func_get_args();

    //if (check_client_active('telegram', $arg_list[1]['client_id'])) {
        echo Modules::run('telegram/list_client_widget', $arg_list[1]['client_id']);
    //}
}

function telegram_accounts_widget() {
    $CI = &get_instance();
    $arg_list = func_get_args();
    // should have account_id + client_id, don't need acc shit.
    $distributor_id = $arg_list[1]['distributor_id'];
    $client_id = $arg_list[1]['client_id'];
    if (check_client_active('telegram', $client_id)) {
        echo Modules::run('telegram/accounts_widget', $distributor_id, $client_id);
    }
}

function telegram_accounts_dash() {
    $CI = &get_instance();
    echo Modules::run('telegram/accounts_dash');
}

function telegram_settings_widget() {
    $CI = &get_instance();
    $arg_list = func_get_args();
    echo Modules::run('telegram/settings_widget');
}

function telegram_accounts_manage_module() {
    $CI = &get_instance();
    $arg_list = func_get_args();
    echo Modules::run('telegram/manage_module_widget');
}

/**
 * When an alert get handled telegram get sent to those setup to
 * receive them!!
 *
 * Take account type into consideration!
 *
 *
 */
function telegram_handle_alert() {
    $CI =& get_instance();
    $arg_list = func_get_args();

    $CI->load->model('telegram/m_telegram_accounts');
    $CI->load->model('telegram/m_telegram_alerts');
    $CI->load->model('telegram/m_telegram_message');
    $CI->load->model('telegram/m_telegram_reports');
    $CI->load->model('telegram/m_telegram_credit_log');
    $CI->load->library('telegram/telegram_sender');
    $CI->load->model('m_logger');
    $CI->load->helper('log');
    $CI->load->model('accounts/m_accounts');
    // $CI->load->model('accounts/m_accounts_credits');

    $CI->load->language('telegram/telegram');
  
    //just a comment

    //site //report
    $site = $arg_list[1]['site'];
    $client = isset($arg_list[1]['client']) ? $arg_list[1]['client'] : null;
    $report = $arg_list[1]['report'];
    $additional_message = $arg_list[1]['message'];
    $settings = isset($arg_list[1]['settings']) ? $arg_list[1]['settings'] : null;
    ///write_log("settings:", print_r( $settings, true));
    $google_url_api_key = $CI->m_settings->get_by_key('google_url_api');
    $google_url_api_key = $google_url_api_key->value;

    $telegram_active = false;

    //check if site has permission for this module!!
    if (($site && check_site_active('telegram', $site->id)) || $client) {
        $telegram_active = true;
    }


    /*
    Check for active on client is a bit dodge.....
        if (!$telegram_active && $client) {
            // if ($client && check_client_active('telegram', $client->id)) {

            // TODO: 10/04/2017 , Handle alert for telegram on client level not implemented!
            // Currently we only support telegram alerts on a site level. If we get a client alert,
            // just ignore it in this module.
            return;
        }
    */


        $telegram_report = $CI->m_telegram_reports->get_by_report($report->id);
        if (!$telegram_report || !$telegram_report->active) {
            // Check if there is a telegram alert for this report.
            return false;
        }

        $msg = $telegram_report->text;
        if ($site) {

            if(isset($site->site_code)){
                $msg .=" for unit ". $site->site_code. " on ".$site->name;
            }
            else{
                $msg .=" on ".$site->name;
            }

        } else {
            $msg .= " on ".$client->name;
        }

        if ($additional_message) {
            $msg .= ' - '.$additional_message;
        }

        //TODO add report links.

        $link = BASE_URL;
        if ($google_url_api_key !== '') {
            $CI->load->library('telegram/GoogleURL');
            //initialize with the google API Key
            $CI->googleurl->_initialize($google_url_api_key);
            $short_link = $CI->googleurl->shorten($link);
            if ($short_link) {
                $link = $short_link;
            }
        }

        $tail = " ".$link;

        // telegram have a limited amount of characters.
        $msg = substr($msg, 0, 460-strlen($tail)).$tail;
        



        // Get alerts....
        if (isset($site)) {
       	    $alerts=$telegram_alerts = $CI->m_telegram_alerts->get_alerts_to_handle($site->id, $report->id);
            
        } else {
            $alerts=$telegram_alerts = $CI->m_telegram_alerts->get_alerts_to_handle_client($client->id, $report->id);
        }

        if ($settings && $telegram_alerts) { //check if there is settings.
            $tmp = $telegram_alerts;
            $telegram_alerts = array();
            foreach ($tmp as $telegram_alert) {
                if ($settings == (array)json_decode($telegram_alert->settings)) {
                    $telegram_alerts[] = $telegram_alert;
                }
            }//foreach
        }//$user_id, $log_type, $log_message, $log_data, $site_id=null, $client_id=null
        
        
   
        if (isset($telegram_alerts) && is_array($telegram_alerts)) {
            foreach ($telegram_alerts as $telegram_alert) {
               
    
                $status = '';
                $delivery_response = '';
                $CI->load->library('telegram/telegram_sender');
     
      
                if (isset($telegram_alert)) {
                        try {
                            $delivery_response = $CI->telegram_sender->send_message(trim($telegram_alert->chat_id),  trim($msg));
                              $CI->m_telegram_alerts->update_last_sent($telegram_alert->telegram_alert_id);
                           // }
                            //Check the response for a success or fail :D 
                           $check = $CI->telegram_sender->check_result($delivery_response);
                           if ($check != 'okay') {
                               $status = 'error';
                               $err_msg = $check;
                           } else {
                               $status = 'okay';
                           }

                            if ($status == 'okay') {
                              

                                $CI->m_logger->log(
                                        0,
                                        'telegram_sent_successfully',
                                        'telegram sent: '.$msg,
                                        array('alert'=>$telegram_alert->telegram_alert_id),
                                        isset($site->id)?$site->id:'',
                                        $telegram_alert->client_id
                                );

                                //deduct some credits :)
                                $CI->m_telegram_accounts->add_credits($telegram_account->telegram_account_id, -1);
                                $CI->m_telegram_credit_log->log_credits($telegram_account->telegram_account_id, 0, -1, -1, $telegram_alert->site_id);

                                $telegram_account->credits--;

                                /* EMAIL */
                                // IF PREPAID AND CREDIT < 5 SEND NOTIFICATION
                                if ($telegram_account->credits <= 5) {
                                    // Okay we need to send a low credit alert....
                                    $CI->load->helper('magcell');
                                    handle_alert(array(), 'telegram_credits_low', $telegram_account->credits.' telegrams remaining', $site->client_id);
                                }
                            } else {
                                $CI->m_logger->log(
                                        0,
                                        'telegram_sent_failed',
                                        'telegram sent: '.$msg.(isset($err_msg) ? " ; Error: ".$err_msg : '' ),
                                        array('alert'=>$telegram_alert->telegram_alert_id),
                                        isset($site->id)?$site->id:'',
                                        $telegram_alert->client_id
                                );
                            }

                        } catch(Exception $e) {
                            $status = 'error';
                            $delivery_response = $e->getMessage();
                        }
                    

                } else {
                    $status = 'error';
                    $delivery_response =  "No account setup";
                }
                  
            }//foreach

        }
    // check site

}


/**
 * Some model stuff....
 *
 */

/**
 * When client is edited the distributor id might change... so telegram_Account dist id should change too.
 */
function telegram_client_edit() {
    $CI = &get_instance();
    $CI->load->model('telegram/m_telegram_accounts');
    $CI->load->model('m_client');
    $arg_list = func_get_args();

    $telegram_account = $CI->m_telegram_accounts->get_by_client($arg_list[1]['client_id']);
    if ($telegram_account) {
        //$CI->m_telegram_accounts->update($telegram_account->telegram_account_id, $telegram_account->telegram_sender_id, $arg_list[1]['distributor_id']);
    }
}

/**
 * Some cleanup. Account should be removed when client is deleted!
 */
function telegram_client_delete() {
    $CI = &get_instance();
    $CI->load->model('telegram/m_telegram_accounts');
    $CI->load->model('m_client');
    $arg_list = func_get_args();

    $telegram_account = $CI->m_telegram_accounts->get_by_client($arg_list[1]['client_id']);
    if ($telegram_account) {
        $CI->m_telegram_accounts->delete($telegram_account->telegram_account_id);
    }
}

/**
 * When distributor is added an telegram account need to be created.
 */
function telegram_distributor_add() {
    $CI = &get_instance();
    $CI->load->model('telegram/m_telegram_accounts');
    $arg_list = func_get_args();
    $distributor_id = $arg_list[1]['distributor_id'];
    if ($distributor_id) {
        $CI->m_telegram_accounts->create($distributor_id, 0, 0);
    }
}

/**
 * Delete the distributor accounts whne it gets deleted.
 * We don't need to worry about distributor's client accounts to be
 * deleted as these will be deleted when the client itself is deleted.
 * (delete_client hook)
 */
function telegram_distributor_delete() {
    $CI = &get_instance();
    $CI->load->model('telegram/m_telegram_accounts');
    $arg_list = func_get_args();

    $telegram_account = $CI->m_telegram_accounts->get_by_distributor($arg_list[1]['distributor_id'], false);
    if ($telegram_account) {
        $CI->m_telegram_accounts->delete($telegram_account->telegram_account_id);
    }
    echo "done";
}


/**
 * Add a bit about the telegram used to the Accounts Credit Report.
 * @return null
 */
function telegram_accounts_credit_report() {
    $CI = &get_instance();
    $CI->load->model('telegram/m_telegram_accounts');
    $CI->load->model('telegram/m_telegram_credit_log');

    $CI->load->config('telegram/telegram');

    $arg_list = func_get_args();
    $account = $arg_list[1]['account'];

    // check if module is active.
    if (check_client_active('telegram', $account->client_id)) {
        $content = array();

        $data['telegram_asset_path'] = base_url().$CI->config->item('telegram_assets');

        $data['start_date'] = $arg_list[1]['start_date'];
        $data['end_date'] = $arg_list[1]['end_date'];

        // get telegram accounts
        if ($account->client_id > 0) { //only client accounts.
            $telegram_account = $CI->m_telegram_accounts->get_by_client($account->client_id);
            if ($telegram_account) {
                $data['credits_used'] = $CI->m_telegram_credit_log->get_credits_used($telegram_account->telegram_account_id, $data['start_date'], $data['end_date'], true);
            }
             $CI->load->view('telegram/telegram_accounts_report', $data);
        }
    }
}


/**
 * Function that runs all the cron functions...
 */
function telegram_cron() {
    echo Modules::run('telegram/telegram_crons/send_telegram_reports');
}

function telegram_data_maintenance_cron() {
    $CI = &get_instance();
    $CI->load->model('m_maintenance');

    $arg_list = func_get_args();
    $cutoff = $arg_list[1]['cutoff'];

    $CI->m_maintenance->clean_table('telegram_credits_log', 'date', $cutoff);
    $CI->m_maintenance->clean_table('telegram_messages', 'created_date', $cutoff);
}


    /**
     * telegram_client_point_alert_setup
     *
     * @access public
     *
     * @return mixed Value.
     */
function telegram_client_point_alert_setup() {
    $CI = &get_instance();
    $CI->load->model('telegram/m_telegram_alerts');
    $CI->load->model('telegram/m_telegram_accounts');
    // $CI->load->model('telegram/m_telegram_credit_log');

    $CI->load->config('telegram/telegram');

    $arg_list = func_get_args();
    $client_id = $arg_list[1]['client_id'];
    $point = $arg_list[1]['point'];
    $users = $arg_list[1]['users'];
    $report = $CI->m_report_type->get_by_handle('alert_point');
    $active_users = array();

    // check if module is active.
    if (check_client_active('telegram', $client_id)) {
        // if (!empty($users) && $point) {
            // TODO: Get active users...

            if ($point) {
                $settings = array('client_point_id'=>$point->id);
            } else {
                $settings = false;
            }

            echo '<br class="clrflt"/>';
            echo '<br class="clrflt"/>';
            echo '<div id="telegram_notifications">';
            echo form_label('telegram notifications:');
            echo '<select name="users_telegram[]" multiple="multiple" style="width:500px;">';
            foreach($users as $user) {
                $selected = false;
                $se = $CI->m_telegram_accounts->get_by_user($user->id);
               
                // echo form_checkbox('users[]', $user->id, in_array($user->id, $active_users), 'id="user_check_'.$user->id.'" class="user_checkbox fltl"');
                // echo form_label($user->username.' ('.$user->email.')', 'user_check_'.$user->id);
                // echo '<br class="clrflt"/>';

                if ($user->cellphone && $user->id==$CI->session->userdata('user_id') ){
                    echo '<option value="'.$user->id.'" '.($selected ? 'selected="selected"' : '').'>'.$user->username.' ('.$user->cellphone.')'.'</option>';
                }

            }
            echo '</select>';
            echo '</div>';
        // } else {
            // Do nothing.
            // echo 'No users found.';
        // }
    }

}

    /**
     * telegram_client_point_alert_setup_save
     *
     * @access public
     *
     * @return mixed Value.
     */
function telegram_client_point_alert_setup_save() {
    $CI = &get_instance();

    $CI->load->model('telegram/m_telegram_alerts');
    $CI->load->model('m_report_type');

    // $CI->load->config('telegram/telegram');

    $arg_list = func_get_args();
    $point = $arg_list[1]['point'];
    $report = $CI->m_report_type->get_by_handle('alert_point');

    // check if module is active.
    if (check_client_active('telegram', $point->client_id)) {

        // check alert point set.
        $alert_point_toggle = $CI->input->post('alert_point') ? true : false;
        if (!$alert_point_toggle) {  // Remove all telegram alerts for the point because it's not an alert point anymore
            // remove all mails for this client point case it's not an alert...
            $ss = $CI->m_telegram_alerts->get_list_by_client($point->client_id, $report->id);
            if ($ss) {
                // Need to check mailer settings before deleting an entry.
                foreach($ss as $s) {
                    $telegram_settings = json_decode($s->settings);
                    if ($telegram_settings->client_point_id == $point->id) {
                        $CI->m_telegram_alerts->delete($s->telegram_alert_id);
                    }
                }
            }

        } else {
            // We are saving and alert point so check out how to add/edit the list of telegram alserts.

            $telegram_users = $CI->input->post('users_telegram');
            if (!$telegram_users) { $telegram_users = array(); }
            $settings = array('client_point_id'=> $point->id);

            $client_users = $CI->users->get_list_by_client($point->client_id);
            array_unshift($client_users, $CI->users->get_user($CI->tank_auth->get_user_id()));
            $link_users = array(); // This array will contain users that need to be added
            // They already have an alerpoint thing setup, but we want to setup for a
            // different point.
            if ($telegram_users) {
                foreach($telegram_users as $u) {
                    $link_users[$u] = $u;
                }
            }
            foreach($client_users as $client_user) {
                $user_id = $client_user->id;

                $se = $CI->m_telegram_alerts->get_by_site_user_id($user_id, 0, $report->id, $point->client_id);
                if ($se) {
                    foreach($se as $ss) {
                        foreach($ss as $s) {
                            $mailer_settings = json_decode($s->settings);
                            if (!in_array($client_user->id, $telegram_users) && $mailer_settings->client_point_id == $point->id) {
                                $CI->m_telegram_alerts->delete($s->telegram_alert_id);
                            } else if ($mailer_settings->client_point_id == $point->id) {
                                // we have one with the same settings
                                unset($link_users[$user_id]);
                            }
                        }
                    }
                    // STORE telegram ALERTS
                } else {
                    if (in_array($client_user->id, $telegram_users)) {
                        $CI->m_telegram_alerts->create($point->client_id, 0, $report->id, $client_user->id, 5, 0, 0, 0, $settings);
                    }
                }
            } //foreach

            if (!empty($link_users)) {
                // This array will contain users that need to be added
                // They already have an alerpoint thing setup, but we want to setup for a
                // different point.
                foreach($link_users as $u) {
                    $CI->m_telegram_alerts->create($point->client_id, 0, $report->id, $u, 5, 0, 0, 0, $settings);
                }
            }
        }//else alert_point_toggle

    }//if telegram_users

}

    /**
     * telegram_client_point_more_info_popup
     *
     * @access public
     *
     * @return mixed Value.
     */
function telegram_client_point_more_info_popup() {
    $CI = &get_instance();

    $CI->load->model('telegram/m_telegram_alerts');
    $CI->load->model('m_report_type');

    // $CI->load->config('telegram/telegram');

    $arg_list = func_get_args();
    $client_id = $arg_list[1]['client_id'];
    $client_point = $arg_list[1]['client_point'];

    // check if module is active.
    if (check_client_active('telegram', $client_id)) {

        $item_alerts = '';

        $client_alerts = $CI->m_telegram_alerts->get_list_by_client($client_point->client_id);
        
        if ($client_alerts) {
            foreach($client_alerts as $alert) {
                // check settings.
                $settings = json_decode($alert->settings);
                if ($settings->client_point_id == $client_point->id) {
                    $item_alerts .= '<tr>';
                    $item_alerts .= '<td>'.$alert->username.' ('.$alert->cellphone.')</td>';
                    //$item_alerts .= '<td>'.($alert->last_sent ? date('Y-m-d H:i:s',gmt_to_local($alert->last_sent, $this->tank_auth->get_user_timezone())) : 'Never').'</td>';
                    $item_alerts .= '<td>'.($alert->last_sent ? date('Y-m-d H:i:s',gmt_to_local($alert->last_sent, $CI->tank_auth->get_user_timezone())) : 'Never').'</td>';
                    $item_alerts .= '</tr>';
                }
            }//foreach
        }//$client_alerts
        if ($item_alerts) {
            $return_str = '<label><b>Telegram Alerts: </b></label><br class="clrflt"/><table>'.$item_alerts.'</table><br class="clrflt"/>';
            return $return_str;
        }
    }
}


    /**
     * telegram_alert_point_unassign
     *
     * @access public
     *
     * @return mixed Value.
     */
function telegram_alert_point_unassign() {
    $CI = &get_instance();

    $CI->load->model('telegram/m_telegram_alerts');
    $CI->load->model('m_report_type');

    // $CI->load->config('telegram/telegram');

    $arg_list = func_get_args();
    $client_id = $arg_list[1]['client_id'];
    $client_point = $arg_list[1]['point'];

    $report = $CI->m_report_type->get_by_handle('alert_point');

    // check if module is active.
    if (check_client_active('telegram', $client_id)) {

        $se = $CI->m_telegram_alerts->get_list_by_client($client_id);
        if ($se) {
            foreach($se as $alert) {
                if ($alert->report_id == $report->id) {
                    $mailer_settings = json_decode($alert->settings);
                    if ($mailer_settings->client_point_id == $client_point->id) {
                         $CI->m_telegram_alerts->delete($alert->telegram_alert_id);
                    }
                }
            } //foreach
        }// if

    }
}