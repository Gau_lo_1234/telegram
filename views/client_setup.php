<div class="container" style="width:100%;">
    <h2>TELEGRAM SETUP </h2>
    <br class="clrflt"/>
            <section class="content">
      <div class="callout callout-default">
        <h4>Copy Your Code Below</h4>
        <p> <?php $user_id=$this->session->userdata('user_id');
         echo "Your telegrams Registration code is: <strong id='my_key'>/s".strtolower(base_convert_arbitrary($user_id, 10, 31))."</strong>"; ?></p>
        <p> <?php echo "Follow this link <strong style='color:#000;'>".MAGTOUCH_BOT."</strong> and type <strong>/start</strong> to setup your account" ?></p>
      </div>
      
    </section>
    <!-- /.content -->

    <?php
//    / print_r($this->session->all_userdata());
    //print_r($telegram_setup);

    $acc_type = array(
            'name' => 'telegram_sender',
            'id' => 'telegram_sender',
            'attr' => 'id="telegram_sender"',
            'options' => array(),
            'value' => set_value('telegram_sender', $telegram_setup ? $telegram_setup[0]->id : ''),
    );
   
        ?>

        <?php echo form_open($this->uri->uri_string()); ?>

            <?php echo form_label('Telegram account to use <span>*</span>', $acc_type['id']); ?>
            <select id="user_id" name="user_id" >
            <option value="<?php echo $this->session->userdata('user_id') ?>" selected><?php echo $this->session->userdata('username')."(".$this->session->userdata('user_id').")" ?></option>
            <?php foreach($telegram_setup as $test): ?>
            <option value="<?php echo $test->id ?>"><?php echo $test->username." (".$test->email.")"; ?></option>
            <?php endforeach ?>
            </select>
            <input type='hidden' id="client_id" name="client_id">
            <div class="error">
                <?php echo form_error($acc_type['name']); ?><?php echo isset($errors[$acc_type['name']])?$errors[$acc_type['name']]:''; ?>
            </div>
            <br class="clrflt"/>
            
            <div class="fltr">
            <?php echo '<button type="submit"><img src="'.ASSET_URL.'images/save.png"/> create</button>';//form_submit('submit', "Save"); ?>
            </div>
            <div class="fltr">
            <?php 
                if ($client_id) {
                    //$link = ('accounts/client_view/'.$client_id);
                    $link = ('clients/view/'.$client_id);
                } else {
                    //$link = ('accounts/distributor_view/'.$distributor_id);
                   // $link = ('clients/distributor_view/'.$distributor_id);
                }
                if (isset($link)) echo anchor(site_url($link), '<img src="'.ASSET_URL.'images/cancel.png"/> '.$this->lang->line('cancel'), 'class="button"'); ?>
            </div>            
            <br class="clrflt"/>
        <?php echo form_close(); ?>
   
</div>
<script>
 
 $(document).ready(function() {

    $('#user_id').change(function() {
        var contents = $("#user_id").val();

       $.get( "<?php echo base_url()."telegram/encrypt" ?>",
                  { name: contents },
                  function(data) {
                     $('#my_key').html(data);
                     $('#telegram_key').val(data);
                  }
               );
    //$('#unitCodes').val(contents);
    });

});
</script>