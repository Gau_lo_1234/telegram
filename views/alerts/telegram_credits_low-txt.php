<?php printf( $this->lang->line('telegram_credits_low_subject'), $name) ?>

<?php printf( $this->lang->line('telegram_credits_low_content_txt'), $name ) ?>

<?php printf( nl2br($this->lang->line('mailer_footer')), $site_name) ?>

<?php 
if ($message !== null) {
    echo 'You have  '.$message.'.';
}
?>

<?php printf( nl2br($this->lang->line('unsubscribe_email_txt')), $unsubscribe_link) ?>