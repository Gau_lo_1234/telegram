<?php
$telegram_cost_field = array(
    'name'  => 'telegram_credit_cost',
    'id'    => 'telegram_credit_cost',
    'attr' => 'id="telegram_credit_cost"',
    'value' => set_value('telegram_credit_cost', (isset($telegram_credit_cost) ? $telegram_credit_cost->value : "1")),
    'class'=>'telegram-setting-spinner'
);
$google_url_field = array(
    'name'  => 'google_url_api',
    'id'    => 'google_url_api_telegram',
    'attr' => 'id="google_url_api_telegram" style="width:370px"',
    'value' => set_value('google_url_api', (isset($google_url_api) ? $google_url_api->value : "")),
);
?>



<h3 id="telegram"><img src="<?php echo $telegram_asset_path ?>icon_16.png" /> <?php echo $this->lang->line('settings_telegram_title') ?></h3>
<div class="container" style="width:100%;">


    <?php if (isset($widget_error)) { echo $widget_error; } else { ?>

        <script>
        $(function() {
          // Basic spinner setup
          $( ".telegram-setting-spinner" ).spinner({
              min: 1,
          });
        });
        </script>
<style>
.success{
    background:green;
}

</style>

<?php echo form_label('BOT server', $google_url_field['id']) ?>
<button value="Start Insert" id="startbutton" class="btn btn-success" name="startbutton" style="width:200px; background:green; color:#fff;" onclick="start();">Start </button>
<button value="Stop Insert" id="stopbutton" class="btn btn-danger" name="stopbutton" style="width:200px; background:red; color:#fff;" onclick="stop();">Stop </button>
<div id="server"> </div>
<br>
 <script type="text/javascript">
            var intervals;
            var interval1;
            var interval2;
            var status=false;
            var url="<?php echo base_url() ?>telegram/reply";
            $(document).ready(function(){

            });
            if(status){
                $("#server" ).html("MagTouch Bot running..");
                interval1=setInterval(function(){ $("#server" ).html("MagTouch Bot running.."); }, 3000);
                interval2=setInterval(function(){ $("#server" ).html("MagTouch Bot running......"); }, 5000);
                $("#stopbutton" ).show();
                $("#startbutton" ).hide();
            }else{
                clearInterval(interval1);
                clearInterval(interval2);
                $("#server" ).html("MagTouch Bot stopped");
                $("#startbutton" ).show();
                $("#stopbutton" ).hide();
            }
            function start()
            {
                startInsert();
                intervals = setInterval(startInsert, 10000);
            }
            function stop(){
              status=false;
              $("#startbutton" ).show();
              $("#stopbutton" ).hide();
              clearInterval(intervals);
            }
            function startInsert() {
              status=true;
             $("#stopbutton" ).show();
             $("#startbutton" ).hide();
             $("#server" ).empty();
          
              $('#server').load(url);
              
            }

           
</script>


        <?php echo form_open(site_url('telegram/save_settings')); ?>

            <div class="help">Empty key field will disable this feature.</div>
            <?php echo form_label('Telegram BOT key', $google_url_field['id']); ?>
            <?php echo form_input($google_url_field); ?>
            <div class="error"><?php echo form_error($google_url_field['name']); ?><?php echo isset($errors[$google_url_field['name']])?$errors[$google_url_field['name']]:''; ?></div>

            <div class="fltr">
                &nbsp;<?php echo '<button type="submit"><img src="'.ASSET_URL.'images/save.png"/> '.$this->lang->line('save').'</button>'; ?>
            </div>

            <br class="clrflt"/>
        <?php echo form_close() ?>

        <?php if(isset($telegram_senders)) { ?>
            <?php if (!empty($telegram_senders)) { ?>
            <h3><?php echo $this->lang->line('telegram_accounts') ?></h3>
            <div class="table-container">
                <table class="innerTable tablesorter">
                    <thead>
                        <tr>
                            <th><?php echo $this->lang->line('settings_bot_name') ?></th>
                            <th><?php echo $this->lang->line('settings_telegram_key') ?></th>
                            <th>&nbsp;</th>
                            <th>&nbsp;</th>
                            <th>&nbsp;</th>
                        </tr>
                    </thead>
                    <tbody>
                <?php foreach ($telegram_senders as $telegram_sender) { ?>
                        <tr>
                            <td><?php echo $telegram_sender->name ?></td>
                            <td><?php echo $telegram_sender->key ?></td>
                            <td><?php echo anchor('telegram/remove_telegram_sender/'.$telegram_sender->telegram_sender_id, '<img src="'.ASSET_URL.'images/delete.png" title="Remove telegram account" /> ', 'class="confirm" rel="#telegram-sender-delete-prompt"') ?></td>
                            <td><?php echo anchor('telegram/telegram_sender/'.$telegram_sender->telegram_sender_id, '<img src="'.ASSET_URL.'images/edit.png" title="Edit telegram account" />') ?></td>
                            <td>
                                <?php echo anchor('telegram/test/'.$telegram_sender->telegram_sender_id, '<img src="'.ASSET_URL.'images/mail.png" title="Test telegram account" /> ', 'class="telegram_test"') ?>
                            </td>
                        </tr>
                <?php } ?>
                    </tbody>
                    </table>
                </div>

                    <div id="telegram-sender-delete-prompt" class="modal" style="width:300px;">
                        <h2>Are you sure?</h2>
                        <p>Are you sure you want to delete this telegram account?</p>

                        <button class="close no"><img src="<?php echo ASSET_URL ?>images/no.png"/> No!</button>
                        <button class="close yes"><img src="<?php echo ASSET_URL ?>images/yes.png"/> Yes, delete account.</button>
                    </div>

            <?php } ?>

            <br class="clrflt" />
            <div class="fltl">
            <?php echo anchor('telegram/telegram_sender', '<img src="'.ASSET_URL.'images/add.png" /> '.$this->lang->line('settings_telegram_add_account'),'class="button"'); ?>
            </div>
            <br class="clrflt" />
            <br class="clrflt" />
        <?php } ?>

       <?php if (isset($telegram_templates)) {  $c = 0;?>
            <h3>Telegram Templates</h3>
            <div class="table-container">
                <table class="innerTable tablesorter">
                    <thead>
                    <tr>
                        <th>Active</th>
                        <th><?php echo $this->lang->line('report') ?></th>
                        <th><?php echo $this->lang->line('settings_telegram_template') ?></th>
                        <th>&nbsp;</th>
                    </tr>
                </thead>
                <tbody>
                     <?php if (!empty($telegram_templates)) { ?>
                <?php foreach ($telegram_templates as $telegram_template) { ?>
                        <tr class="<?php echo $c % 2 ? 'even' : 'odd' ?>">
                            <td><?php echo $telegram_template->active > 0 ? '<img src="'.ASSET_URL.'images/yes.png" title="Active" />' : '<img src="'.ASSET_URL.'images/no.png" title="Not active" />'; ?></td>
                            <td><?php echo $telegram_template->display_name ?></td>
                            <td><?php echo $telegram_template->text ?></td>
                            <td><?php echo anchor('telegram/template_setup/'.$telegram_template->id, '<img src="'.ASSET_URL.'images/edit.png" title="Edit telegram account" />') ?></td>
                        </tr>
                <?php
                                unset($reports[$telegram_template->report_id]);
                                $c++;
                           }
                         }
                         /*
                         // Templates get loaded on telegram module install now....
                          if (!empty($reports)) {
                           foreach ($reports as $report) { ?>
                        <tr class="<?php echo $c % 2 ? 'even' : 'odd' ?>">
                            <td><?php echo '<img src="'.ASSET_URL.'images/no.png" title="Not active" />'; ?></td>
                            <td><?php echo $report->display_name ?></td>
                            <td><?php echo $report->display_name ?></td>
                            <td><?php echo anchor('telegram/template_setup/'.$report->id, '<img src="'.ASSET_URL.'images/edit.png" title="Edit telegram account" />') ?></td>
                        </tr>
                <?php
                            $c++;
                           }
                          }
                          //*/
                ?>
            </tbody>
                    </table>
                </div>
            <?php } ?>
    <?php } //widget error ?>
    <br class="clrflt"/>

</div>
