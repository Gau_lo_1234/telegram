<style type="text/css">
.gray_box {
        width:600px;
}
</style>
<h2>Install Module: telegram</h2>

<div class="container" style="width:100%;">
    <p><strong>Version that will be installed: </strong><?php echo $version ?></p>
    <p><strong>Requires: </strong> <br/>
        OG > 0.6.0 <br/>
        Accounts Module > 0.3.0</p>
    <p>For this module to be activated there are some changes that need to be made to your database.</p>
    <p>Make sure you have a backup of your database before proceding.</p>
    <br class="clrflt"/>
</div>
<div class="container" style="width:100%;">
    <h2>CHANGES</h2>

    <h3>New Database Tables:</h3>
    <p>The following tables will be created:</p>
    <p class="gray_box">
        &bull; telegrams
        <br/>
         &bull; telegram_accounts
        <br/>
        &bull; telegram_alerts
        <br/>
        &bull; telegram_credits_log
        <br/>
        &bull; telegram_emails
        <br/>
        &bull; telegram_messages
        <br/>
        &bull; telegram_sender
        <br/>
        &bull; telegram_templates
    </p>
    <br class="clrflt"/>
    <br class="clrflt"/>

    <h3>Reports:</h3>
    <p class="gray_box">
         &bull; Telegram Credits Low - Alert
        <br/>
        &bull; Telegram Messages Sent - Report
    </p>

    <h3>Crontab:</h3>
    <p>Please ensure that the following is in your crontab!</p>
    <div class="gray_box">
        <pre>0 0 * * * php /premium/path/cron.php crons/_daily >/dev/null 2>&1</pre>
    </div>
    <br class="clrflt"/>
    <br class="clrflt"/>

</div>

<form action="<?php echo site_url("telegram/install/vamos") ?>" method="POST">
        <div class="fltr"><button type="submit" >Proceed  <img src="<?php echo ASSET_URL ?>images/next.png"/></button></div>
</form>