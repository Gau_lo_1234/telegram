<h3><img src="<?php echo $telegram_asset_path ?>icon_16.png" /> Telegram Sent</h3>
<?php
if (empty($credits_used)) {
    echo "<p>No telegram sent this month</p>";
} else {
?>
    <table width="100%">
        <tr>
            <td><strong>Site</strong></td>
            <td><strong>Credits Used</strong></td>
        </tr>
    <?php

    foreach ($credits_used as $used) { ?>
        <tr>
                <td><?php echo anchor('sites/view/'.$used->site_id, $used->name); ?></td>
                <td><?php echo $used->credits ?></td>
        </tr>
    <?php } ?>
    </table>
<?php } ?>