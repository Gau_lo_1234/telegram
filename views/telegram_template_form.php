<?php
$template = array(
	'name'	=> 'template',
	'id'	=> 'template',
	'value'	=> set_value('template',($telegram_template ? $telegram_template->text : $report_name)),
	'maxlength'	=> 80,
	'size'	=> 30,
);
?>
<div class="container" style="width:100%;">
    <h2><?php echo $this->lang->line('settings_telegram_edit_template'); ?></h2>

    <?php echo form_open(uri_string()); ?>

        <?php echo form_label($this->lang->line('report')) .$report_name; ?>
        <br class="clrflt"/>
        <br class="clrflt"/>

        <?php echo form_label($this->lang->line('settings_telegram_template'), $template['id']); ?>
        <?php echo form_input($template); ?>
                <div class="error"><?php echo form_error($template['name']); ?><?php echo isset($errors[$template['name']])?$errors[$template['name']]:''; ?></div>
        <br class="clrflt"/>

    <div class="fltr">
                &nbsp;<?php echo '<button type="submit"><img src="'.ASSET_URL.'images/yes.png"/> '.$this->lang->line('save').' '.$this->lang->line('settings_telegram_edit_template').'</button>'; ?>
    </div>
    <?php echo form_close(); ?>
    <br class="clrflt" />
</div>
