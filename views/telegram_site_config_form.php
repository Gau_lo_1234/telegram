<?php echo isset($container) ? '<div class="container" style="width:100%;">' : '' ?>
<h2>Setup magcell via telegram</h2>
<p>This function is only available on new generation Magcell units. If you own a new generation Magcell unit proceed as follows.</p>

<p><img src="<?php echo ASSET_URL ?>images/warning.png" width="16" height="16"/> <strong>If there is an error in the process it may result in a misconfigured Magcell.</strong></p>

<h3>How to put Magcell into telegram config mode</h3>
<ol>
    <li>Fill in the cellphone number of the SIM card currently in the Magcell
        and click on the <strong>"Send Setup telegram"</strong> button below to send configuration telegram</li>
    <li>Turn off the Magcell</li>
    <li>Press and hold the call button while turning on the Magcell</li>
    <li><strong>Hold the call button until the Magcell screen displays "READY"</strong></li>
    <li>Switch the Magcell off and turn it back on.</li>
</ol>
<form action="<?php echo site_url('telegram/site_telegram_config') ?>" method="POST" >

    <?php if ($this->tank_auth->get_user_role() == "super") { //show all settings ?>
    <p><em><strong>Note:</strong> Fields starting with * will remain unchanged, blank fields will be deleted</em></p>
    <label style="width:130px">Magcell Code: </label><input type="text" name="magcell_code" value="<?php echo $site_code ?>" readonly /><br class="clrflt"/>
    <label style="width:130px">Username: </label><input type="text" name="magcell_username" value="<?php echo set_value('magcell_username', '*') ?>"/><br class="clrflt"/>
    <label style="width:130px">Password: </label><input type="text" name="magcell_password" value="<?php echo set_value('magcell_password', '*') ?>"/><br class="clrflt"/>
    <label style="width:130px">APN: </label><input type="text" name="magcell_apn" value="<?php echo set_value('magcell_apn', '*') ?>"/><br class="clrflt"/>
    <label style="width:130px">DHCP: </label><input type="text" name="magcell_dhcp" value="<?php echo set_value('magcell_dhcp', '*') ?>"/><br class="clrflt"/>
    <label style="width:130px">Port: </label><input type="text" name="magcell_port" value="<?php echo set_value('magcell_port', '*') ?>"/><br class="clrflt"/>
    <label style="width:130px">Path: </label><input type="text" name="magcell_path" value="<?php echo set_value('magcell_path', '*') ?>"/><br class="clrflt"/>
    <label style="width:130px">IA: </label><input type="text" name="magcell_ia" value="<?php echo set_value('magcell_ia', '*') ?>"/><br class="clrflt"/>
    <?php } else { ?>
    <label style="width:130px">Magcell Code: </label><input type="text" name="magcell_code" value="<?php echo $site_code ?>" readonly /><br class="clrflt"/>
    <?php } ?>
    <?php if (!$telegram_active) {
        // We need to ask for telegram account to use!!
        echo '<label style="width:130px">Country</label>';
        echo '<select name="telegram_sender" id="telegram_sender">';
        foreach($telegram_senders as $ss) {
            echo '<option value="'.$ss->telegram_sender_id.'" '.($ss->telegram_sender_id == $this->input->post('telegram_sender') ? 'selected="selected"' : '').'>'.$ss->country.'</option>';
        }
        echo '</select>';
        echo '<br class="clrflt"/>';
    } ?>    
    <label style="width:130px">Cellphone Number: </label><input type="text" name="magcell_telephone" value="<?php echo set_value('magcell_telephone') ?>"/>
    <div class="error">
        <?php echo form_error('magcell_telephone'); ?><?php echo isset($errors['magcell_telephone'])?$errors['magcell_telephone']:''; ?>
</div>
    <input type="hidden" name="site_id" value="<?php echo $site_id ?>" />
        <button type="submit" class="fltr"><img src="<?php echo ASSET_URL ?>images/mail.png"/> Send Setup telegram</button>
</form>
<br class="clrflt"/>
<?php echo isset($container) ? '</div>' : '' ?>
