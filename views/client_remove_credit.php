<div class="container" style="width:100%;">
    <h2>Wakka Wakka</h2>
<?php echo validation_errors() ?>
    <?php echo form_open($this->uri->uri_string()); ?>

        <?php echo form_label('Wakka', "credits"); ?>
        <br class="clrflt"/>
        <input type="hidden" value="1" name="credits" id="credits"/>

        <br class="clrflt"/>
        <div class="slider_container" >
            <div id="credit-slider" style="float:right;width:450px;"></div>
            <p id="credit-amount" class="amount" style="float:right; padding:0px 20px;margin-top:-2px;">
                1
            </p>
        </div>
        <div class="error">
            <?php echo form_error("credits"); ?>
        </div>
        <br class="clrflt"/>
        <br class="clrflt"/>

	<div class="fltr">
	<?php echo '<button type="submit"><img src="images/yes.png"/> '.'Wakka'.'</button>';//form_submit('submit', "Save"); ?>
	</div>
        <br class="clrflt"/>
    <?php echo form_close(); ?>

</div>
