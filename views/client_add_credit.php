<div class="container" style="width:100%;">
    <h2><?php echo $this->lang->line('telegram_'.$action.'_credits') ?></h2>
<?php echo validation_errors() ?>
    <?php echo form_open($this->uri->uri_string()); ?>

        <label><b>Available credits:</b></label>
        <?php echo accounts_display_credits($account->credits) ?>
        <br class="clrflt"/>
        <br class="clrflt"/>

        <label><b>Credits used:</b></label>
        <?php echo accounts_display_credits('') ?><span id="credits-used">0</span>
        <br class="clrflt"/>

        <br class="clrflt"/>
        <?php echo form_label('telegrames', "credits"); ?>
        
        <input type="hidden" value="0" name="credits" id="credits"/>
        <div class="slider_container" >
            <div id="credit-amount" class="amount" style="float:left; padding:0px 20px 0 0;margin-top:-2px;">
                0
            </div>
            <div id="credit-slider" style="float:left;width:450px;"></div>
            <br class="clrflt"/>
        </div>
        <div class="error">
            <?php echo form_error("credits"); ?>
        </div>
        <br class="clrflt"/>
        <br class="clrflt"/>

        <div class="fltr">
        <?php echo '<button type="submit"><img src="'.$telegram_asset_path.'phone_'.($action == 'remove' ? 'delete' : 'add').'.png"/> '.$this->lang->line('telegram_'.$action.'_credits').'</button>';//form_submit('submit', "Save"); ?>
        </div>
        
        <div class="fltr">
	<?php 
    if ($telegram_account_id) {
            if ($telegram_account->client_id) {
                    $link = ('accounts/client_view/'.$telegram_account->client_id.'#telegram');
                } else {
                    $link = ('accounts/distributor_view/'.$telegram_account->distributor_id.'#telegram');
                }
            echo anchor(site_url($link), '<img src="'.ASSET_URL.'images/cancel.png"/> '.$this->lang->line('cancel'), 'class="button"'); 
}
            ?>
	</div>                
        <br class="clrflt"/>
    <?php echo form_close(); ?>

</div>
