<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head><title><?php printf( $this->lang->line('credits_statement_subject'), $name, $start_date, $end_date) ?></title></head>
<body>
<div style="max-width: 800px; margin: 0; padding: 30px 0;">
<table width="80%" border="0" cellpadding="0" cellspacing="0">
<tr>
<td width="5%"></td>
<td align="left" width="95%" style="font: 13px/18px Arial, Helvetica, sans-serif;">
<h2 style="font: normal 20px/23px Arial, Helvetica, sans-serif; margin: 0; padding: 0 0 18px; color: black;"><?php printf( $this->lang->line('credits_statement_subject'), $name, $start_date, $end_date) ?></h2>
<?php printf( $this->lang->line('credits_statement_content_html'), $start_date, $end_date) ?>
            <table width="100%">
            <tr>
                <td><strong>Site</strong></td>
                <td><strong>User</strong></td>
                <td><strong>Date</strong></td>
                <td><strong>Credits Used</strong></td>
            </tr>
            <?php
            foreach ($telegram_log as $log) { ?>
                <tr>
                     <td><?php echo $log->name ?></td>
                    <?php
                    // cater for 'system user'
                    if (!isset($log->username)) {
                        echo '<td>System</td>';
                    } else {
                    ?>
                    <td><?php echo $log->username?></td>
                    <?php } ?>
                    <td><?php echo date('Y-m-d H:i:s', gmt_to_local($log->date, $timezone)) ?></td>
                    <td style="text-align:center;color:<?php echo ($log->credits >=0) ? 'green' : 'red' ?>"><?php echo $log->credits ?></td>
                </tr>
            <?php } ?>
             </table>
<br />
<br />
<?php printf( nl2br($this->lang->line('mailer_footer')), $site_name) ?>
</td>
</tr>
</table>
</div>
</body>
</html>