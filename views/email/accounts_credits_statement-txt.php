telegram Module

<?php
if (empty($telegram_log)) {
    echo "<p>No telegram sent this month</p>";
} else {
    foreach ($telegram_log as $log) {
        echo 'Site: '.ucfirst($log->name)."\t User: ".(isset($log->username) ? $log->username : 'System')." Date: ".date('Y-m-d H:i:s', gmt_to_local($log->date, $timezone))." Credits: ".$log->credits."\n\n";
    }
}