<?php printf( $this->lang->line('credits_statement_subject'), $client, $module_id) ?>

<?php printf( $this->lang->line('credits_statement_content_html'), $start_date, $end_date ) ?>

<?php
foreach ($active_modules as $module) {
    if (strtolower($module->module) == 'accounts') continue;
    echo 'Module: '.ucfirst($module->module)."\t Credits: ".(isset($credits_used[$module->id]->credits) ? $credits_used[$module->id]->credits : 0 )."\n\n";
}
?>

<?php printf( nl2br($this->lang->line('mailer_footer')), $site_name) ?>