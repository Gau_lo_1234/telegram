<strong>telegram Module</strong>
<?php
if (empty($telegram_log)) {
    echo "<p>No telegram sent this month</p>";
} else {
?>
    <table width="100%">
        <tr>
            <td><strong>Site</strong></td>
            <td><strong>User</strong></td>
            <td><strong>Date</strong></td>
            <td><strong>Credits Used</strong></td>
        </tr>
    <?php

    foreach ($telegram_log as $log) { ?>
        <tr>
                <td><?php echo $log->name ?></td>
            <?php
            // cater for 'system user'
            if (!isset($log->username)) {
                echo '<td>System</td>';
            } else {
            ?>
            <td><?php echo $log->username?></td>
            <?php } ?>
            <td><?php echo date('Y-m-d H:i:s', gmt_to_local($log->date, $timezone)) ?></td>
            <td style="text-align:center;color:<?php echo ($log->credits >=0) ? 'green' : 'red' ?>"><?php echo $log->credits ?></td>
        </tr>
    <?php } ?>
    </table>
<?php } ?>