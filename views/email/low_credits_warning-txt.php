<?php printf( $this->lang->line('low_credits_warning_subject'), $name, $credits_left) ?>

<?php printf( $this->lang->line('low_credits_warning_content_txt'), $name, $credits_left ) ?>

<?php printf( nl2br($this->lang->line('mailer_footer'), $site_name)) ?>