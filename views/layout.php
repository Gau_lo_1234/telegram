<?php
    if (!isset($header_data)) $header_data = array("nav"=>"accounts");
    $this->load->view('elements/header', $header_data);
?>

<h2>Telegram Details</h2>

<div id="left-nav" class="container" style="width:100%;">
    <?php
    if (isset($master_account)) {
        echo '<a href="'.site_url('telegram/detail/'.$master_account->telegram_account_id).'" class="button '.(!isset($telegram_account_id) || $telegram_account_id==0 || $telegram_account_id == $master_account->telegram_account_id ? ' active ': '').(isset($js_left_nav) ? $js_left_nav : '').'" id="'.$master_account->telegram_account_id.'">';
        echo $master_account->name;
        echo '</a><br/><hr/>';
    }

    if (isset($master_accounts)) {
            foreach ($master_accounts as $account) {
        // we want to open all accoutns listed as master accounts!!
            echo '<a href="'.site_url('telegram/detail/'.$account->telegram_account_id).'" class="button '.(isset($telegram_account_id) && $telegram_account_id == $account->telegram_account_id ? ' active ': '').(isset($js_left_nav) ? $js_left_nav : '').'" id="'.$account->telegram_account_id.'">';
            echo $account->name;
            echo '</a><br/>';
            }
            echo "<hr/>";
    }
    if (isset($client_accounts) && !empty($client_accounts)) {
        foreach ($client_accounts as $account) {
                //normal operation...
                echo '<a href="'.site_url('telegram/detail/'.$account->telegram_account_id).'" class="button '.(isset($telegram_account_id) && $telegram_account_id == $account->telegram_account_id ? ' active ': '').(isset($js_left_nav) ? $js_left_nav : '').'" id="'.$account->telegram_account_id.'">';
                echo $account->name;
                echo '</a><br/>';
        }
    }
    ?>
</div><!-- left-nav-->

<div id="right-content">

<?php echo $content_for_layout; ?>

</div>

<br class="clrflt"/>

<?php echo $this->load->view('elements/footer', TRUE); ?>
