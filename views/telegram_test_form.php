<?php
$cellphone = array(
	'name'	=> 'cellphone',
	'id'	=> 'cellphone',
	'value'	=> set_value('cellphone', ''),
	'maxlength'	=> 80,
	'size'	=> 30,
);
?>
<div class="container" style="width:100%;">
    <h2><?php echo $this->lang->line('settings_telegram_test'); ?></h2>

    <?php echo form_open(uri_string()); ?>

        <input type="hidden" name="group" value="sys">

        <?php echo form_label($this->lang->line('settings_telegram_country')); ?><?php echo $telegram_sender->country ?>
        <br class="clrflt" />

        <?php echo form_label($this->lang->line('settings_telegram_test_number'), $cellphone['id']); ?>
        <?php echo form_input($cellphone); ?>
                <div class="error"><?php echo form_error($cellphone['name']); ?><?php echo isset($errors[$cellphone['name']])?$errors[$cellphone['name']]:''; ?></div>
        <br class="clrflt"/>

    <div class="fltr">
                &nbsp;<?php echo '<button type="submit"><img src="'.ASSET_URL.'images/yes.png"/> '.$this->lang->line('settings_telegram_test').'</button>'; ?>
    </div>
    <?php echo form_close(); ?>
    <br class="clrflt" />
</div>
