<?php
$user = array(
	'name' => 'user_select',
	'id' => 'user_select',
	'attr' => 'id="user_select"',
	'options' => array('0'=>'----'),
	'value' => set_value('user_select'),
);
$user['options'][$current_user->id] = $current_user->username. " (".$current_user->email.")";
foreach($users as $u) {
    if (isset($acl['edit_'.$u->role]) || $this->tank_auth->get_user_role() == 'super') {
        $user['options'][$u->id] = $u->username." (".$u->email.")";
    }
}
?>
<div class="container" style="width:100%;">
    <div class="fltr">
    <?php
    if (isset($acl['edit_telegram_account'])) {
        echo  anchor(site_url('telegram/setup/'.$client_id.'/'.$distributor_id), '<img src="'.ASSET_URL.'images/edit.png"/> '.$this->lang->line('telegram_setup'),'class="button"');
    }
        ?>
    </div>
            <?php echo  $show_add ? anchor('telegram/add_credit/'.$telegram_setup->telegram_account_id, '<img src="'.$telegram_asset_path.'phone_add.png" title="Add telegram Credits" /> Add telegram credits','class="button fltr"') : ''; ?>
    
    <h2><?php printf($this->lang->line('telegram_detail_title'), $account->name); ?></h2>

    <?php if ($telegram_setup) { ?>

            <label><b><?php echo $this->lang->line('telegram_account') ?>:</b></label>
            <?php echo $telegram_setup->country ?>
            <br class="clrflt">

            <?php if (!$telegram_setup->credits) {
                echo '<br class="clrflt">';
                show_message_box('error', 'No telegram credits! This means no telegram will be sent.');
            } else if ($telegram_setup->credits < 5 ){
                echo '<br class="clrflt">';
                show_message_box('error', 'Only '.$telegram_setup->credits." telegram's remaining!");
            } ?>

            <label><b><?php echo $this->lang->line('telegram_credits_available') ?>:</b></label>
            <label><?php echo ($telegram_setup->credits ? $telegram_setup->credits : "0") ?></label>

            <br class="clrflt">
            <br class="clrflt">


            <div class="accordion">

                <h3 id="emails"><?php echo $this->lang->line('automated_emails') ?><img class="fltr" src="<?php echo base_url() ?>assets/images/help.png" rel="#automated_emails_hint"/></h3>
                <div class="container">
                    <?php 
                    $this->load->view('mailers/list_mailers', array('client_id'=>$client_id, 'show_site'=>false, 'show_user'=>true, 'show_preview'=>true, 'show_report'=>true, 'show_frequency'=>true, 'show_last_sent'=>true)); 
                    ?>
                </div><!--email notifications container-->

                <h3>
                    <?php echo $this->lang->line('telegram_email_notification_title') ?>
                    <img class="fltr" src="<?php echo ASSET_URL ?>images/help.png" title="<?php echo $this->lang->line('telegram_email_notification_hint') ?>" rel="#telegram_email_notification_hint"/>
                </h3>
                <div class="container">
                        <div id="telegram_email_notification_hint" class="overlay-wide">
                            <h3 id="notify"><?php echo $this->lang->line('telegram_email_notification_title') ?> Help</h3>
                            <?php echo $this->lang->line('telegram_email_notification_hint') ?>
                        </div>           
                    <?php if (empty($telegram_emails)) { ?>
                            <p><em><?php echo $this->lang->line('no_notifications') ?></em></p>
                            <?php
                            } else {
                                ?>
                                    <table class="innerTable">
                                        <tr>
                                            <th width="22px">&nbsp;</th>
                                            <th><?php echo $this->lang->line('user') ?></th>
                                            <th><?php echo $this->lang->line('email') ?></th>
                                            <th><?php echo $this->lang->line('last_sent') ?></th>
                                            <th>&nbsp;</th>
                                            <th>&nbsp;</th>
                                        </tr>
                                <?php
                                    foreach ($telegram_emails as $email) {
                                        if ($email->frequency > 0) {
    //                                        if (isset($report_types[$email->report_id])) {
                                                echo '<tr>';
                                                
                                                echo '<td style="text-align:center">';
                                                $email_user = $this->users->get_user_by_id($email->user_id, 1);
                                                if (!is_null($email_user)) {
                                                    if ($email_user->banned) {
                                                        echo '<p title="'.$this->lang->line('users_disabled_title').'"><img src="'.ASSET_URL.'images/no.png" ></p>';
                                                    } else if ($email_user->new_email && $email_user->email != $email_user->new_email) {
                                                        //new email address not verifgied
                                                        echo'<img src="'.ASSET_URL.'images/no.png" title="'.$this->lang->line('users_changed_email_title').'"/>';
                                                    } else {
                                                        echo '<img src="'.ASSET_URL.'images/yes.png">';
                                                    }
                                                } else {
                                                    echo '<p title="'.$this->lang->line('users_unactive_title').'"><img src="'.ASSET_URL.'images/no.png" ></p>';
                                                }
                                                echo '</td>';
                                                
                                                echo '<td>'.$email->username.'</td>';
                                                echo '<td>'.$email->email.'</td>';

                                                echo '<td>'.($email->last_sent ? date('Y-m-d H:i:s',gmt_to_local($email->last_sent, $this->tank_auth->get_user_timezone())) : '-').'</td>';
                                                if (isset($acl['edit_telegram_notification'])) {
                                                    echo '<td>'.anchor('/telegram/remove_email/'.$email->telegram_email_id, '<img src="'.ASSET_URL.'images/delete.png" title="'.$this->lang->line('telegram_remove_email') .'"/> ').'</td>';
                                                } else {
                                                    echo '<td>&nbsp;</td>';
                                                }
                                                echo '</tr>';
    //                                        }
                                         }
                                        //unset hte users already in the tbale..
                                        unset($user['options'][$email->user_id] );
                                    }
                                    ?>
                                    </table>

                    <?php
                            }
                    ?>
                    <br class="clrflt" />
                    
                    <?php if (isset($acl['edit_telegram_notification'])) { ?>
                    <div class="fltr">
                        <form action="<?php echo site_url('telegram/add_email') ?>" method="POST">
                            <?php echo form_hidden('telegram_account_id', $telegram_setup->telegram_account_id); ?>
                            <?php echo form_dropdown($user['name'], $user['options'], $user['value'], $user['attr']); ?>
                            <button type="submit"><img src="<?php echo ASSET_URL ?>images/add.png" /> <?php echo $this->lang->line('telegram_add_email') ?></button>
                        </form>
                    </div>
                    <br class="clrflt">
                    <?php } ?>
                </div>

                <?php if (isset($telegram_messages)) { ?>
                <h3 id="messages">
                    <?php echo $this->lang->line('telegram_message_title') ?>
                    <img class="fltr" src="<?php echo ASSET_URL ?>images/help.png" title="<?php echo $this->lang->line('telegram_message_hint') ?>" rel="#telegram_message_hint"/>
                </h3>
                <div class="container">
                    <div id="telegram_message_hint" class="overlay-wide">
                        <h3><?php echo $this->lang->line('telegram_message_title') ?> Help</h3>
                        <?php echo $this->lang->line('telegram_message_hint') ?>
                    </div>
                    <?php if (empty($telegram_messages)) { ?>
                            <p><em><?php echo $this->lang->line('no_notifications') ?></em></p>
                            <?php
                            } else {
                                ?>
                                    <table class="innerTable">
                                        <tr>
                                            <th><?php echo $this->lang->line('cell') ?></th>
                                            <th><?php echo $this->lang->line('message') ?></th>
                                            <th>Status</th>
                                            <th>&nbsp;</th>
                                            <th><?php echo $this->lang->line('sent_date') ?></th>
                                        </tr>
                                <?php
                                    $c =0;
                                    foreach($telegram_messages as $message) {
                                        $c++;
                                        ?>
                                    <tr class="<?php echo ($c % 2 ? 'odd' : 'even') ?>" >
                                        <?php
                                        echo '<td>'.$message->number.'</td>';
                                        echo '<td>'.(strlen($message->message) > 50 ? '<span title="'.$message->message.'">'.substr($message->message, 0, 50).'...</span>' : $message->message).'</td>';
                                        if ($message->status == 'okay') {
                                            echo '<td class="positive">'.$message->status.'</td>';
                                            echo '<td>&nbsp;</td>';
                                        } else {
                                            echo '<td class="negative" >'.$message->status.'</td>';
                                            echo '<td  class="negative">';
                                            try {
                                                libxml_use_internal_errors(true);
                                                $response = new SimpleXMLElement($message->delivery_response);
                                                echo $response->call_result->error;
                                            } catch(Exception $e) {    
                                                echo $message->delivery_response;
                                            } 
                                            echo '</td>';
                                        }

                                        echo '<td>'.($message->created_date ? date('Y-m-d H:i:s',gmt_to_local($message->created_date, $this->tank_auth->get_user_timezone())) : '-').'</td>';
                                        echo '</tr>';
                                    }
                                    ?>
                                    </table>
                                     <br class="clrflt"/>
                                    <div class="pagination">
                                        <?php echo $telegram_messages_paging ?>
                                    </div>
                                    <br class="clrflt"/>
                    <?php
                            }
                    ?>

                    <br class="clrflt">
                </div>
                <?php } ?>

                <h3 id="log">
                    <?php echo $this->lang->line('telegram_credits_log') ?>
                    <img class="fltr" src="<?php echo ASSET_URL ?>images/help.png" title="<?php echo $this->lang->line('telegram_credits_log_hint') ?>" rel="#telegram_credits_log_hint"/>
                </h3>
                <div class="container">
                        <div id="telegram_credits_log_hint" class="overlay-wide">
                            <h3><?php echo $this->lang->line('telegram_credits_log') ?> Help</h3>
                            <?php echo $this->lang->line('telegram_credits_log_hint') ?>
                        </div>                    

                        <?php // TODO ?>
                        <form action="" method="POST">
                            <input type="text" id="credits_used_start" class="date text-field" name="start_date" value="<?php echo date('Y-m-d H:i', gmt_to_local($start_date, $this->tank_auth->get_user_timezone())) ?>"/>
                            <input type="text" id="credits_used_end" class="date text-field" name="end_date" value="<?php echo date('Y-m-d H:i', gmt_to_local($end_date, $this->tank_auth->get_user_timezone())) ?>" />
                            <input type="submit" value="<?php echo $this->lang->line('view') ?>"  />
                        </form>
                    <?php if (isset($telegram_log) && $telegram_log) { ?>
                        <table id="telegram-log-table" class="tablesorter">
                            <thead>
                                <tr>
                                    <th>Site</th>
                                    <th>User</th>
                                    <th>Date</th>
                                    <th style="text-align:center">Credit</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $c =0;
                                $total_telegram =0;
                                foreach($telegram_log as $log) {
                                    $c++;
                                    $total_telegram += ($log->credits < 0 ? $log->credits : 0);
                                    ?>
                                <tr class="<?php echo ($c % 2 ? 'odd' : 'even') ?>" >
                                     <td><?php echo $log->name ?></td>
                                    <?php
                                    // cater for 'system user'
                                    if (!isset($log->username)) {
                                        echo '<td>System</td>';
                                    } else {
                                    ?>
                                    <td title="<?php echo $log->email ?>"><?php echo $log->username?></td>
                                    <?php } ?>
                                    <td><?php echo date('Y-m-d H:i:s', gmt_to_local($log->date, $this->tank_auth->get_user_timezone())) ?></td>
                                    <td style="text-align:center" class="<?php echo ($log->credits >=0) ? 'positive' : 'negative' ?>"><?php echo $log->credits ?></td>
                                </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                        <p>Total telegrams used: <?php echo $total_telegram ?></p>
                     <br class="clrflt"/>
                    <div id="telegram-log-paging" class="pagination">
                        <?php //echo $telegram_log_paging ?>
                    </div>
                    <br class="clrflt"/>
                    <?php } else {
                        echo '<em>'.$this->lang->line('no_log_entries').'</em>';
                        } ?>
                </div>
            </div><!--accordion-->
    <?php } else { ?>
         <p><?php echo $this->lang->line('not_setup') ?></p>
    <?php } ?>

    <br class="clrflt">

</div>
