
    

<h3 id="telegram">
    <img src="<?php echo $telegram_asset_path ?>icon_16.png" /> Telegram Account
    
</h3>
<div class="container" style="width:100%;">

 

            
            <label><b>Telegram Account:</b></label>
            <?php echo $telegram_setup->country ?>
            <br class="clrflt"/>

            <label><b>telegrames available:</b></label>
            <label><?php echo $telegram_setup->credits ?></label>

            <?php if (isset($telegram_log) && $telegram_log) { ?>
                <br class="clrflt"/>
                <br class="clrflt"/>
                <table width="100%" class="tablesorter">
                    <thead>
                        <tr>
                            <?php //if ($type == 'master') { echo '<th>Account</th>'; } else { echo '<th>Site</th>'; } ?>
                            <th>User</th>
                            <th>Date</th>
                            <th style="text-align:center">Credit</th>
                            <th>&nbsp;</th>
                            <th>&nbsp;</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach($telegram_log as $log) { ?>
                        <tr>
                             <!-- <td><?php //echo $log->name ? $log->name : $account->name ?></td> -->
                            <?php
                            // cater for 'system user'
                            if (!isset($log->username)) {
                                echo '<td>System</td>';
                            } else {
                            ?>
                            <td title="<?php echo $log->email ?>"><?php echo $log->username?></td>
                            <?php } ?>
                            <td><?php echo date('Y-m-d H:i:s', gmt_to_local($log->date, $this->tank_auth->get_user_timezone())) ?></td>
                            <td style="text-align:center" class="<?php echo ($log->credits >=0) ? 'positive' : 'negative' ?>"><?php echo $log->credits ?></td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                        <?php } ?>
                    </tbody>
                </table>
            <?php } ?>
            <br class="clrflt"/>
<?php /* ?>
    <div class="fltr">
        <?php if ($telegram_setup) {
                if (($client_id == 0 && ($this->tank_auth->get_user_role() == 'super' || $this->tank_auth->get_user_role() == 'distrib')) || $client_id != 0) {
                    echo anchor(site_url('telegram/detail/'.$telegram_setup->telegram_account_id), '<img src="'.ASSET_URL.'images/edit.png"/> View Detailed','class="button"');
                }
            } ?>            
    </div>
<?php */ ?>    


    <?php /* echo  $show_add ? anchor('telegram/add_credit/'.$telegram_setup->telegram_account_id, '<img src="'.$telegram_asset_path.'phone_add.png" title="Add telegram Credits" /> Add telegram credits','class="button fltr"') : ''; */ ?>

        <div class="fltr">
        <?php  echo anchor(site_url('telegram/telegram_messages_report'), '<img src="'.ASSET_URL.'images/reports.png"/> Telegram Messages Sent Report','class="button"'); ?>
        </div>

        <div class="fltr">
        <?php  
        if ($this->tank_auth->get_user_role() != 'user')  {
            echo  anchor(site_url('telegram/setup/'.$client_id.($distributor_id ? '/'.$distributor_id : '')), '<img src="'.ASSET_URL.'images/edit.png"/> Setup','class="button fltr"'); 
        }
        ?>
        </div>


    <br class="clrflt">

</div>
