

<?php
    $days_of_the_week = array(
        '0' => 'XXX',
                    '1' => $this->lang->line('monday'),
                    '2' => $this->lang->line('tuesday'),
                    '3' => $this->lang->line('wednesday'),
                    '4' => $this->lang->line('thursday'),
                    '5' => $this->lang->line('friday'),
                    '6' => $this->lang->line('saturday'),
                    '7' => $this->lang->line('sunday')
    );
    ?>
<h3 id="telegram">
    <img src="<?php echo $telegram_asset_path ?>icon_16.png" /> <?php echo $this->lang->line('telegram_alerts') ?> &amp; Reports
    <?php
    if (!$telegram_setup) {
        echo "<span class=\"error\">Not Setup</span>";
    } else {

    }
    ?>
</h3>
<div class="container" style="width:100%;">

<?php if (isset($widget_error)) { echo $widget_error; } else { ?>



        <?php

        $ci =& get_instance();
        $ci->load->model('m_telegram_accounts');
        $telegram_account= $ci->m_telegram_accounts->check_account_exists($this->session->userdata('user_id'));
        if ((isset($telegram_account) && !empty($telegram_account)) || $this->session->userdata('role')=="super") {

            if (isset($alerts) && !empty($alerts) ){ ?>
        <div class="table-container" style="width:100%;">

            <table id="myTable" class="table display" style="width:95%;">
                <thead>
                <tr>
                    <th><input type="checkbox" name="selectall" class="selectall"/></th>
                    <th style="min-width: 100px;"><?php echo $this->lang->line('user') ?></th>
                    <th style="min-width: 160px;"><?php echo $this->lang->line('report') ?></th>
                    <th style="min-width: 80px;"><?php echo $this->lang->line('site')?></th>
                    <th style="min-width: 80px;"><?php echo $this->lang->line('last_sent') ?></th>
                    <th>&nbsp;</th>
                    <th>&nbsp;</th>
                </tr>
            </thead>
            <tbody>
                <?php

                    if (!isset($report_types)) {
                        $report_types = $this->m_report_type->get_array();
                    }

                    foreach ($alerts as $telegram) {
                        if ($telegram->frequency > 0) {
                            if (isset($report_types[$telegram->report_id])) {
                                $status=telegram_account_exists($telegram->user_id);
                                if($status){
                                    $icon="yes.png";
                                }else{
                                    $icon="no.png";
                                }
                                echo '<tr>';
                                echo '<td><input type="checkbox" name="selectone[]" value="'.$telegram->telegram_alert_id.'"></td>';
                                echo '<td>'.$telegram->username.' <img src="'.site_url("assets/images/".$icon).'"></td>';
                                echo '<td><img src="'.($report_types[$telegram->report_id]->icon_path ? base_url().$report_types[$telegram->report_id]->icon_path : ASSET_URL."images/mail.png").'"/> '.$report_types[$telegram->report_id]->display_name;
                                $point_site=null;
                                if (isset($telegram->settings)) {
                                     $settings = json_decode($telegram->settings);

                                     foreach($settings as $key=>$value) {
                                         if ($key == 'client_point_id') {
                                             $point_site=isset($site->name)?$site->name:'- -';
                                              $cp = $this->m_client_point->get_by_id($value);
                                             if(isset($cp->site_id)){
                                               $site=$this->m_site->get_by_id($cp->site_id);

                                               echo ' ['.$cp->name.']';
                                             }



                                         }
                                     }
                                 }

                                echo '</td>';
                                if(isset($point_site))
                                {
                                    echo '<td>'.$point_site.'</td>';
                                }
                                else{
                                    echo '<td>'.($telegram->site_id ? '<a href="'.base_url().'sites/view/'.$telegram->site_id.'">'.$telegram->site_name.'</a>' : '').'</td>';


                                }

                                echo '<td>'.($telegram->last_sent ? date('Y-m-d H:i:s', gmt_to_local($telegram->last_sent, $this->tank_auth->get_user_timezone())) : '-').'</td>';
                                if (isset($acl['manage_telegram_alerts'])) {
                                    echo '<td>'.anchor('/telegram/alerts/'.($telegram->site_id ? 'site/'.$telegram->site_id : 'client/'.$telegram->client_id).'/'.$telegram->user_id, '<img src="'.ASSET_URL.'images/edit.png" title="Edit Alert"/> ').'</td>';
                                    echo '<td>'.anchor('/telegram/remove_alert/client/'.$telegram->client_id.'/'.$telegram->telegram_alert_id, '<img src="'.ASSET_URL.'images/delete.png" title="Delete Alert"/> ', 'class="confirm" rel="#telegram-alert-delete-prompt"').'</td>';
                                } else {
                                    echo '<td colspan="2">&nbsp;</td>';
                                }
                                echo '</tr>';
                            }
                        }
                    }
                    ?>
                </tbody>

            </table>
        </div>
            <div id="telegram-alert-delete-prompt" class="modal" style="height:300px;">
                <h2>Are you sure?</h2>
                <p>Are you sure you want to delete this telegram alert?</p>
                <button class="close no"><img src="<?php echo ASSET_URL ?>images/no.png"/> No!</button>
                <button class="close yes"><img src="<?php echo ASSET_URL ?>images/yes.png"/> Yes, telegram alert.</button>
            </div>

                <div id="telegram-all-delete-prompt" class="modal" style="height:300px;">
                    <h2>Are you sure?</h2>
                    <p>Are you sure you want to delete selected telegram alerts?</p>
                    <button type="button" class="close no" class=""><img src="<?php echo ASSET_URL ?>images/no.png"/> No!</button>
                    <button type="button" id="yes" class=""><img src="<?php echo ASSET_URL ?>images/yes.png"/> Yes, telegram alert.</button>
                    <script>
                        var selected = new Array();
                        var checkedValue;
                        var myJSON;
                        var base_url="<?php echo base_url() ?>";
                        $('#yes').click(function (e) {
                            e.preventDefault();
                            $("input[name='selectone[]']").each(function() {

                                checkedValue = $(this).attr("checked");
                                if(checkedValue=="checked"){
                                    selected.push($(this).val());

                                }


                            });

                            myJSON = JSON.stringify(selected);

                            $.ajax({
                                url: base_url + "telegram/del_multi_alerts",
                                data: {myData: myJSON },
                                type: 'POST',
                                success: function(response) {
                                    window.location.reload();
                                }
                            });

                        });
                    </script>
                </div>
        <?php }
            else{

               ?>
                    <p><em><?php echo $this->lang->line('no_telegram_alerts') ?></em></p>
                <?php
            } ?>
        <br class="clrflt"/>
        <?php if (isset($acl['manage_telegram_alerts'])) { ?>
            <div class="fltr">
         <?php if(isset($alerts) && !empty($alerts)){ ?>
           <button class="confirm" rel="#telegram-all-delete-prompt">Delete</button>
         <?php } ?>
                <?php echo anchor('telegram/alerts/client/'.$client_id.'/'.$this->tank_auth->get_user_id(), '<img src="'.ASSET_URL.'images/add.png" /> '.$this->lang->line('manage_telegram_alerts'), 'class="button"'); ?>
                <?php echo anchor('telegram/alerts/site/0/'.$this->tank_auth->get_user_id().'/'.$client_id, '<img src="'.ASSET_URL.'images/add.png" />'.$this->lang->line('manage_site_telegram_alerts'), 'class="button"'); ?>
            </div>
            <br class="clrflt"/>
        <?php } ?>
    <?php
        }else{


            echo anchor('profile', 'Click here to setup telegram account', 'class="button"');
        }
}

?>
</div>
