<?php 

//dump($sites);?>



<script>

    telegramAlerts = <?php echo json_encode($telegram_details);?>;


    $(function() {




      
        $('.toggle_setting').change(function() {
            var id = $(this).attr('id').replace('n', '');
            if ( $(this).is(':checked') ) {
                $('#settings_'+id).slideDown();
            } else {
                $('#settings_'+id).slideDown();
            }
        });
        $('.toggle_setting').each(function() {
            
            var id = $(this).attr('id').replace('n', '');
            //if ( $(this).is(':checked') ) {
                $('#settings_'+id).slideDown();
            //}
        });

        $("#site_select").change(function(){
            var access = $(this).find(':selected').data('access');
                $("input[id^='n']").each(function (i, el) {
                   
                    $(this).removeAttr('checked');
                         //It'll be an array of elements
                }); 
            if($(this).val() == ''){
               
                //$("#Patrols").css('display','none');
                $("#Patrols").addClass('disabledbutton');
                $('#Patrols').find('input[type=checkbox]:checked').removeAttr('checked');
                $('#Patrols :input').attr('disabled', true);


                $("#Alerts").addClass('disabledbutton');
            
                //$("#alerts_selection_box").addClass('disabledbutton');
                $('#Alerts').find('input[type=checkbox]:checked').removeAttr('checked');
                $('#Alerts :input').attr('disabled', true); 
            }else{
                var selectedSiteID = $(this).val();

                $("input[id^='n']").each(function (i, el) {
                   idval =  $(this).attr('id').replace('n','');
                    $.each(telegramAlerts,function(k,v){
                       //alert(v[0].site_id+" - "+selectedSiteID);
                        if((v[0].site_id == selectedSiteID) && (v[0].report_id == idval)){

                           $('#n'+idval).attr('checked','');
                        };  

                    });
                });

                $("#Alerts").removeClass("disabledbutton");
                $('#Alerts :input').attr('disabled', false);

                if(access){
                    $("#Patrols").removeClass("disabledbutton");
                    $('#Patrols :input').attr('disabled', false);
                    //$("#Patrols").css('display','block');
                }else{
                    $("#Patrols").addClass('disabledbutton');
                   //$("#Patrols").css('display','none');
                }
                
            }
            
        });
    });
</script>
<?php
$type = isset($type) ? $type : 'site';

$frequency_field =  array(
    'name' => 'frequency',
    'value'=>set_value('frequency')
);
$weekday_field = array(
    'name' => 'weekly_day',
    'attr' => 'class="weekly_day"',
    'options' => array(
                '1' => $this->lang->line('monday'),
                '2' => $this->lang->line('tuesday'),
                '3' => $this->lang->line('wednesday'),
                '4' => $this->lang->line('thursday'),
                '5' => $this->lang->line('friday'),
                '6' => $this->lang->line('saturday'),
                '7' => $this->lang->line('sunday')),
    'value'=>set_value('weekly_day')
);
$monthday_field = array(
    'name' => 'monthly_day',
    'attr' => 'class="monthly_day"',
    'options' => array(),
);
for ($i=1 ; $i <= 31 ; $i++) {
    $monthday_field['options'][$i] = $i;
}

echo validation_errors();
?>
<h3>Alerts for: <?php echo $current_user->username.' ('.$current_user->email.')' ?></h3>

<?php echo form_open('telegram/alerts/'.$type.'/'.$type_id.'/'.$user_id); ?>
<input type="hidden" name="user_id" value="<?php echo $user_id ?>" />
<input type="hidden" name="client_id" value="<?php echo $client_id ?>" />
<input type="hidden" name="site_id" id="form_site_id" value="<?php echo $site_id ?>"/>
<div id="alerts_selection_box">
<?php


foreach($telegram_grouped_reports as $report_group=>$reports) {

        $r = 0;
        foreach ($reports as $report_id=>$report) {


            //added check to see if report module type was set to patrol as well
            //beginning of patrol module checker
            //if ( !$report->module ||  (isset($site_id) && $site_id && check_site_active($report->module, $site_id, $site->client_id))) {


            // if ($report->alert) { // reports are now linked up to telegram table so we don't needto check.
                if ($report->automated_type !== $type || $report->group == 'Reports') { continue; }

                    $selected = isset($telegram_details[$report_id]);

                if($site_id == 0 and $type != 'client'){
                    $selected = 0;
                }
                ?>


                <?php
               
                if (!$r) { $r=1; ?>


                <?php if($site_id == 0 && $report_group == 'Patrols'):?>
                    <div class="gray_box disabledbutton" id="<?php echo $report_group?>" style="width:auto; float:left;"><!-- style="display:none; width:auto%; float:left;">-->
                
                <?php elseif($site_id != 0 && $report_group == "Patrols"):?>
                
                    <div class="gray_box disabledbutton" id="<?php echo $report_group?>" style="float:left;"><!-- display:none;-->
                <?php else:?>
                
                    <div class="gray_box disabledbutton" id="<?php echo $report_group?>" style="float:left;">
                <?php endif;?>

                     <h3><?php echo display_name_from_handle($report_group) ?></h3>
                <?php } ?>

                    <div class="fltl"><input type="checkbox" class="toggle_setting" name="alerts[<?php echo $report_id ?>]" value="5" id="n<?php echo $report_id ?>" <?php echo ($selected == 1 ? "checked=checked" : "") ?> /></div>
                    <label for="ngit <?php echo $report_id ?>"><?php echo '<img src="'.base_url().($report->icon_path ? $report->icon_path : ASSET_PATH."/images/mail.png").'" width="16"/> '.$report->display_name ?></label>
                    <br class="clrflt"/>

                    <?php if ($report->report == 'alert_point') { ?>
                        <div id="settings_<?php echo $report_id ?>" class="alert_settings" style="margin-left:20px;<?php echo ($selected ? '' : 'display:none') ?>" >
                            <br/>
                            <p><strong>Select alert points:</strong></p>
                            <?php //get alert points...
                            $alert_points = $this->m_client_point->get_by_client_id($client_id, false, null, '1');
                            
                            foreach($alert_points as $ap) {
                                $ap_selected = false;
                                $match = json_encode(array('client_point_id'=>$ap->id));
                                if (isset($telegram_details[$report_id])) {
                                    if (is_object($telegram_details[$report_id])) {
                                        if ($match == $telegram_details[$report_id]->settings) {
                                            $ap_selected = true;
                                        }
                                    } else {
                                        foreach($telegram_details[$report_id] as $e) {
                                            if ($match == $e->settings) {
                                                $ap_selected = true;
                                            }
                                        }
                                    }
                                }
                                ?>
                                <div class="fltl"><input type="checkbox" class="alert_check" name="settings[<?php echo $report_id ?>][]" value="<?php echo $ap->id ?>" id="alert_<?php echo $ap->id ?>" <?php echo ($ap_selected ? "checked=checked" : "") ?> /></div>
                                <label for="alert_<?php echo $ap->id ?>"><?php echo $ap->name.' ('.$ap->point_code.')' ?></label>
                                <br class="clrflt"/>
                            <?php } ?>
                        </div>
                    <?php } else { ?>
        
                        <div id="settings_<?php echo $report_id ?>" class="alert_setting" style="display:none;">
                            <?php if ($report->hourly == 1) { ?>
                                <div class="fltl"><input type="radio" name="frequency[<?php echo $report_id ?>]" value="4" id="h<?php echo $report_id ?>" <?php echo (isset($telegram_details[$report_id]) && $telegram_details[$report_id][0]->frequency == 4 ? "checked=checked" : "") ?>/></div>
                                <label for="h<?php echo $report_id ?>" class="radio"><?php echo $this->lang->line('hourly') ?></label>
                                <br class="clrflt"/>
                            <?php } ?>

                            <?php if ($report->daily == 1) { ?>
                                <div class="fltl"><input type="radio" name="frequency[<?php echo $report_id ?>]" value="1" id="d<?php echo $report_id ?>" <?php echo (isset($telegram_details[$report_id]) && $telegram_details[$report_id][0]->frequency == 1 ? "checked=checked" : (isset($telegram_details[$report_id]) ? "" : "checked=checked")) ?>/></div>
                                <label for="d<?php echo $report_id ?>" class="radio"><?php echo $this->lang->line('daily') ?></label> at <input type="text" name="daily_time[<?php echo  $report_id ?>]" value="<?php echo  set_value('daily_time', (isset($telegram_details[$report_id]) ? $telegram_details[$report_id][0]->time+$tz : '06').":00") ?>" class="timepicker">
                                <br class="clrflt"/>
                            <?php } ?>

                            <?php if ($report->weekly == 1) { ?>
                                <div class="fltl"><input type="radio" name="frequency[<?php echo $report_id ?>]" value="2" id="w<?php echo $report_id ?>" <?php echo (isset($telegram_details[$report_id]) && $telegram_details[$report_id][0]->frequency == 2 ? "checked=checked" : "") ?> /></div>
                                <label for="w<?php echo $report_id ?>" class="radio"><?php echo $this->lang->line('weekly') ?></label> on a
                                <?php echo form_dropdown($weekday_field['name']."[".$report_id."]", $weekday_field['options'], (isset($telegram_details[$report_id]) ? $telegram_details[$report_id][0]->day_of_week : 25), $weekday_field['attr']); ?>
                                <span class="alert_time_span"> at <input type="text" name="weekly_time[<?php echo  $report_id ?>]" value="<?php echo  set_value('weekly_time', (isset($telegram_details[$report_id]) ? $telegram_details[$report_id][0]->time+$tz : '06').":00") ?>" class="timepicker"></span>
                                <br class="clrflt"/>
                            <?php } ?>

                            <?php if ($report->monthly == 1) { ?>
                                <div class="fltl"><input type="radio" name="frequency[<?php echo $report_id ?>]" value="3" id="m<?php echo $report_id ?>"<?php echo (isset($telegram_details[$report_id]) && $telegram_details[$report_id][0]->frequency == 3 ? "checked=checked" : "") ?> /></div>
                                <label for="m<?php echo $report_id ?>" class="radio"><?php echo $this->lang->line('monthly') ?></label> on the
                                <?php echo form_dropdown($monthday_field['name']."[".$report_id."]", $monthday_field['options'], (isset($telegram_details[$report_id]) ? $telegram_details[$report_id][0]->day_of_month : 25), $monthday_field['attr']); ?>
                                <span class="alert_time_span">at <input type="text" name="monthly_time[<?php echo  $report_id ?>]" value="<?php echo  set_value('monthly_time', (isset($telegram_details[$report_id]) ? $telegram_details[$report_id][0]->time+$tz : '06').":00") ?>" class="timepicker"></span>
                                <br class="clrflt"/>
                            <?php } ?>

                            <?php if ($report->after_upload == 1) { ?>
                                <div class="fltl"><input type="radio" name="frequency[<?php echo $report_id ?>]" value="6" id="au<?php echo $report_id ?>"<?php echo (isset($telegram_details[$report_id]) && $telegram_details[$report_id][0]->frequency == 6 ? "checked=checked" : "") ?> /></div>
                                <label for="au<?php echo $report_id ?>" class="radio"><?php echo $this->lang->line('after_upload') ?></label>
                                <br class="clrflt"/>
                            <?php } ?>

                        <div class="error"><?php echo isset($errors[$frequency_field['name']])?$errors[$frequency_field['name']]:''; ?></div>

                        </div>
                    <?php } ?>

            <?php
            // }//if alert
            //}end of patrol module checker
        }
        if ($r) {
            echo '</div>';
        }
}
?>
</div>


        <br class="clrflt"/>
       <div class="fltr">
        <?php echo '<button type="submit"><img src="'.ASSET_URL.'images/yes.png"/> '.$this->lang->line('save').'</button>';//form_submit('submit', "Save"); ?>
        </div>
    <div class="fltl">
    <?php
    if ($type=='client' && isset($client_id) && isset($acl['clients'])) {
        echo anchor('/clients/view/'.$client_id.'#telegram', '<img src="'.base_url().'assets/images/back.png"/> Back to Client', 'class="button"');
    } else if ($type=='site' && isset($site_id) && isset($acl['sites'])) {
        if ($site_id == 0) {
            echo anchor('/clients/view/'.$client_id.'#telegram', '<img src="'.base_url().'assets/images/back.png"/> Back to Client', 'class="button"');
        } else {
            echo anchor('/sites/view/'.$site_id.'#telegram', '<img src="'.base_url().'assets/images/back.png"/> Back to Site', 'class="button"');
        }
    } else {
        echo anchor('/sites', '<img src="'.base_url().'assets/images/back.png"/> Back to Site', 'class="button"');
    }
    ?>
    </div>
        <br class="clrflt"/>
<?php echo form_close(); ?>

