<?php
    $days_of_the_week = array(
        '0' => 'XXX',
                    '1' => $this->lang->line('monday'),
                    '2' => $this->lang->line('tuesday'),
                    '3' => $this->lang->line('wednesday'),
                    '4' => $this->lang->line('thursday'),
                    '5' => $this->lang->line('friday'),
                    '6' => $this->lang->line('saturday'),
                    '7' => $this->lang->line('sunday')
    );
    ?>
<h3 id="telegram">
    <img src="<?php echo $telegram_asset_path ?>icon_16.png" /> <?php echo $this->lang->line('telegram_alerts') ?> &amp; Reports
    <?php
    if (!$telegram_setup) {
        echo "<span class=\"error\">Not Setup</span>";
    } else {
       
    }
    ?>
</h3>
<div class="container" style="width:100%;">
<?php if (isset($widget_error)) { echo $widget_error; } else { ?>

    <?php if ($telegram_setup) { ?>

        <?php if (!($alerts) || empty($alerts)) { ?>
            <p><em><?php echo $this->lang->line('no_telegram_alerts') ?></em></p>
        <?php } else { ?>
        <div class="table-container">

            <table class="innerTable tablesorter">
                <thead>
                <tr>
                    <th style="    min-width: 80px;"><?php echo $this->lang->line('user') ?></th>
                    <th><?php echo $this->lang->line('cell') ?></th>
                    <th style="    min-width: 160px;"><?php echo $this->lang->line('report') ?></th>
                    <th>&nbsp;</th>
                    <th style="    min-width: 80px;"><?php echo $this->lang->line('last_sent') ?></th>
                    <th>&nbsp;</th>
                    <th>&nbsp;</th>
                </tr>
                </thead>
                <tbody>
                <?php
                    foreach ($alerts as $telegram) {
                        if ($telegram->frequency > 0) {
                            if (isset($report_types[$telegram->report_id])) {
                                echo '<tr>';
                                echo '<td>'.$telegram->username.'</td>';
                                echo '<td>'.$telegram->cellphone.'</td>';
                                echo '<td><img src="'.base_url().($report_types[$telegram->report_id]->icon_path ? $report_types[$telegram->report_id]->icon_path : ASSET_URL."images/mail.png").'"/> '.$report_types[$telegram->report_id]->display_name.'</td>';
                                echo '<td>';
                                switch ($telegram->frequency) {
                                    case 1: //DAILY
                                        $end_ts = gmt_to_local( mktime( $telegram->time, 0, 0), $this->tank_auth->get_user_timezone());
                                        $time_str = date('H:i', $end_ts);
                                        echo $time_str.' '.$this->lang->line('daily');
                                        break;
                                    case 2: //WEEKLY
                                        $time_str = date('H:i', gmt_to_local( mktime( $telegram->time, 0, 0), $this->tank_auth->get_user_timezone()));
                                        echo $time_str.' '.$days_of_the_week[$telegram->day_of_week].'s ';
                                        break;
                                    case 3: //MONTHLY
                                        $end_ts = gmt_to_local( mktime( $telegram->time, 0, 0, date("n"), $telegram->day_of_month), $this->tank_auth->get_user_timezone());
                                        echo date('H:i', $end_ts).' '.show_ordinal($telegram->day_of_month).' Monthly';
                                        break;
                                    case 4: //HOURLY
                                        echo $this->lang->line('hourly');
                                        break;
                                    case 6:
                                        echo $this->lang->line('after_upload');
                                        break;
                                    default:
                                        echo '&nbsp;';
                                }
                                echo '</td>';
                                echo '<td>'.($telegram->last_sent ? date('Y-m-d H:i:s',gmt_to_local($telegram->last_sent, $this->tank_auth->get_user_timezone())) : '-').'</td>';
                                if (isset($acl['manage_telegram_alerts'])) {
                                    echo '<td>'.anchor('/telegram/alerts/site/'.$telegram->site_id.'/'.$telegram->user_id, '<img src="'.ASSET_URL.'images/edit.png" title="Edit Alert"/> ').'</td>';
                                    echo '<td>'.anchor('/telegram/remove_alert/site/'.$telegram->site_id.'/'.$telegram->telegram_alert_id, '<img src="'.ASSET_URL.'images/delete.png" title="Delete Alert"/> ', 'class="confirm" rel="#telegram-alert-delete-prompt"').'</td>';
                                } else {
                                    echo '<td colspan="2">&nbsp;</td>';
                                }
                                echo '</tr>';
                            }
                         }
                    }
                    ?>
                </tbody>
            </table>
        </div>
            <div id="telegram-alert-delete-prompt" class="modal" style="height:300px;">
                <h2>Are you sure?</h2>
                <p>Are you sure you want to delete this telegram alert?</p>
                <button class="close no"><img src="<?php echo ASSET_URL ?>images/no.png"/> No!</button>
                <button class="close yes"><img src="<?php echo ASSET_URL ?>images/yes.png"/> Yes, telegram alert.</button>
            </div>
        <?php } ?>
        <br class="clrflt"/>
        <?php if (isset($acl['manage_telegram_alerts'])) { ?>
            <div class="fltr">
                <?php echo anchor('telegram/alerts/site/'.$site_id.'/'.$this->tank_auth->get_user_id(), '<img src="'.ASSET_URL.'images/add.png" /> '.$this->lang->line('manage_telegram_alerts'),'class="button"'); ?>
            </div>
            <br class="clrflt"/>
        <?php } ?>
    <?php
    } else {
        if (isset($acl['edit_telegram_account'])) { ?>
            <div class="fltr">
                <?php echo  anchor(site_url('telegram/setup/'.$client_id), '<img src="'.ASSET_URL.'images/edit.png"/> Setup','class="button"'); ?>
            </div>
        <?php } ?>
        <p><em>Telegram account has not yet been setup.</em></p>
        <br class="clrflt"/>
    <?php
    }
}
?>
</div>
