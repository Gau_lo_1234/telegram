<?php
   // var_dump($this);
    //exit();
?>

<script type="text/javascript">
    var client_id ;
    var site_id;
    var item_id;
    $(function() {
        site_id =$('#site_select').find(":selected").val();
        $('#site_id').val(site_id);
        $('#form_site_id').val(site_id);
       
        //alert( $("#site_id").val());
        client_id = $('#client_id').val();
        $('#site_select').change(function() {
       
         site_id = $("#site_select").val();
         $("#site_id").val(site_id);  
         $('#form_site_id').val(site_id);
         if($(this).val() != ''){
               $("#form_site_id").val(site_id);// = $(this).val();
           }else{
               $("#form_site_id").val(0);// = 0;
            }
            //form_site_id
         });
         $('#client_select').change(function() {
        var client_id = $("#site_select").val();
         });
       
     
        var type = $('#type').val();
           item_id = 0;
        if (type == 'site') {
            item_id = site_id;
        } else {
            item_id = client_id;
        }

       
        $('#user_select').change( function() {

            $("#user_select option:selected").each(function () {
               
                var type = $('#type').val();
                item_id = 0;
                if (type == 'site') {
                item_id = site_id;
                } else {
                item_id = client_id;
                }
                user = $(this).val();

                if (user > 0) {
                    $('#loader').show();
                   
                    //do ajax call
                       $.ajax({
                          url: "<?php echo site_url('telegram/ajax/telegram_alerts/') ?>/" + type + "/" + item_id + "/" + user,
                          cache: false,
                          dataType: "html",
                          success: function(html){
                            $('#loader').hide();
                            e = $("#automated_email_form");
                            e.html(html);
                            $( 'button, .button', e ).button();
                            $('.timepicker', e).timepicker({'showMinute':false});
                            $( '#tabs' ).tabs();
                          }
                        });
                }
            });
        });

       

        
              $('form').on("submit",function(e) {

                    if($("#site_select").val() == '' && type == 'site'){
                        e.preventDefault();
                        alert("Please select a valid SITE PROFILE!");
                    }
              });            
        
    });
</script>


<?php
$client_field = array(
    'name'  => 'client_id',
    'id'    => 'client_select',
    'value' => set_value('client', (isset($saved_settings['client']) ? $saved_settings['client'] : 0)),
    );
$site_field = array(
    'name'  => 'site_id',
    'id'    => 'site_select',
    'value' => set_value('site', (isset($saved_settings['site']) ? $saved_settings['site'] : 0)),
    );

$user_field = array(
    'name' => 'user_select',
    'id' => 'user_select',
    'attr' => 'id="user_select"',
    'value' => set_value('user_id', $user_id),
);

$user_field['options'][$current_user->id] = $current_user->username. " (".strtolower(base_convert_arbitrary($current_user->id, 10, 31)).")";

foreach($users as $u) {
    if (isset($acl['edit_'.$u->role])) {
        $user_field['options'][$u->id] = $u->username." (/s".strtolower(base_convert_arbitrary($u->id, 10, 31)).")";
    }
}

?>
<div id="" class="container"  style="width:100%;">

    <div class="fltr">
        <img src="<?php echo site_url() ?>/assets/images/help.png" rel="#help">
    </div>
    <div id="help" class="overlay-wide">
        <?php printf($this->lang->line('telegram_alert_help'), anchor('/user/add_user', 'add a new user')); ?>
    </div>
    <h2><?php echo $this->lang->line('telegram_alerts') ?></h2>
<?php
if (!$edit && !$user_field['options']) {
?>
    <p><?php echo $this->lang->line('telegram_no_users') ?></p>

    <div class="fltr">
        <?php echo anchor('/user/add_user', '<img src="'.ASSET_URL.'images/add.png"/> '.$this->lang->line('add_a_user'),'class="button"'); ?>
    </div>

    <div class="fltr">
        <?php echo anchor('/clients', '<img src="'.ASSET_URL.'images/no.png"/> '.$this->lang->line('cancel'),'class="button"'); ?>
    </div>
     <br class="clrflt"/>
<?php
} else {
?>

    <div id="automated_email">
            <?php
            if ($type == 'client') {
                echo '<br class="clrflt"/>';
               echo form_label($this->lang->line('client'), $client_field['id']);
               echo $client->name;
            } else if ($site_id > 0){
                echo '<br class="clrflt"/>';
                echo form_label($this->lang->line('site'), $site_field['id']);
                if (isset($site->name)) {
                    echo $site->name;
                }
            }
            echo '<br class="clrflt"/>';
            ?>        
        <?php echo form_open($this->uri->uri_string()); ?>


        <?php if ($site_id == 0 && $type != 'client') {
          
            ?>
          
            <?php echo form_label($this->lang->line('site').":", $site_field['id']);

            ?>
            <select name="<?php echo $site_field['name'] ?>" id="<?php echo $site_field['id']; ?>"  style="min-width: 200px;">
             
    		
               <?php

		echo '<option  data-access="'.$sites [0]->access.'" value="'.$sites[0]->siteID.'" '.($site_field['value'] == $sites[0]->siteID ? 'selected="selected"' : '').' selected="selected">'.$sites[0]->name.($sites[0]->active ? '' : ' (Deactivated)').'</option>';
                    if (is_array($sites)) {
                        foreach ($sites as $s) {
                         if($s->id!=$sites[0]->id){
                            if (isset($acl['view_deactive_site']) || $s->active) {
                                echo '<option  data-access="'.$s->access.'" value="'.$s->id.'" '.($site_field['value'] == $s->id ? '' : '').'>'.$s->name.($s->active ? '' : ' (Deactivated)').'</option>';
                                if ($site_field['value'] == $s->id ) $current_site = $s;
                            }
                            }
                        }
                    }
                ?>
            </select>
           

            <?php echo '<br class="clrflt"/>'; ?>
        <?php
        
    } else { 
         
            ?>
                <?php } ?>
              
            <input type="hidden" name="site_id" id="site_id" value="<?php echo isset($site_id) ? $site_id : 0 ?>" /> 
  
            <input type="hidden" name="client_id" id="client_id" value="<?php echo isset($client_id) ? $client_id : 0 ?>"/>
            <input type="hidden" name="type" id="type" value="<?php echo isset($type) ? $type : '' ?>"/>

            <div id="view_to_edit" class="fltr">
                <?php echo '<button type="submit"><img src="'.ASSET_URL.'images/edit.png"/> '.$this->lang->line('site_email_select_user').'</button>'; ?>
            </div>
            <?php echo form_label($this->lang->line('user'), $user_field['id']); ?>

            <?php echo form_dropdown($user_field['name'], $user_field['options'], $user_field['value'], $user_field['attr']); ?>
            <div class="error"><?php echo form_error($user_field['name']); ?><?php echo isset($errors[$user_field['name']])?$errors[$user_field['name']]:''; ?></div>
        <?php echo form_close(); ?>
        <br/>

        <br class="clrflt"/>
        <div id="loader"  style="text-align:center;display:none;"><img src="<?php echo ASSET_URL ?>images/ajax-loader.gif" alt="loading..."/></div>
        <div id="automated_email_form">
            <?php
            if (isset($user_id) && $user_id > 0) {
                $this->load->view('telegram_alerts_form');
            }
            ?>
        </div>

        <br class="clrflt"/>
    </div>
     <?php } ?>
</div>
<br class="clrflt"/>
<!-- <script>
    $(document).ready(function(){

      //$('form').on("submit",function(e) {

            // if($("#site_select").val() == ''){
            //     e.preventDefault();
            //     alert("Please select a valid SITE PROFILE!");
            // }
     // });

    });
</script> -->
