<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head><title><?php printf( $this->lang->line($type.'_mailer_subject'), 'Detailed', $site) ?></title></head>
<body>
<div style="max-width: 800px; margin: 0; padding: 30px 0;">
<table width="80%" border="0" cellpadding="0" cellspacing="0">
<tr>
<td width="5%"></td>
<td align="left" width="95%" style="font: 13px/18px Arial, Helvetica, sans-serif;">
<h2 style="font: normal 20px/23px Arial, Helvetica, sans-serif; margin: 0; padding: 0 0 18px; color: black;"><?php printf( $this->lang->line($type.'_mailer_subject'), 'Detailed', $site) ?></h2>
<?php printf( $this->lang->line('report_mailer_content_html'), $start, $end) ?>
<br />
<br />
<?php if (isset($html_report)) { echo $html_report."<br/><br/>"; }?>
<?php printf( nl2br($this->lang->line('mailer_footer')), $site_name) ?>
</td>
</tr>
<tr><td colspan="2">&nbsp;</td></tr>
<?php if (isset($unsubscribe_link) && $unsubscribe_link !== '') { ?>
<tr>
    <td width="5%"></td>
    <td align="left" width="95%" style="font: 10px/13px Arial, Helvetica, sans-serif;">
        <?php printf( nl2br($this->lang->line('unsubscribe_email_html')), $unsubscribe_link) ?>
</tr>
<?php } ?>
</table>
</div>
</body>
</html>