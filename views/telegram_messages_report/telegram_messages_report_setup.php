<?php
$client = array(
    'name'  => 'client',
    'id'    => 'client_select',
    'value' => set_value('client', (isset($saved_settings['client']) ? $saved_settings['client'] : 0)),
    );
    
//Field code moved to controller
?>
    <?php
    
    // if (empty($sites)) {
    //     echo "Please setup some sites before using the reporting functionality.";
    // } else { ?>
        <?php echo form_open($this->uri->uri_string(), 'id="report-setup"'); ?>

        <input type="hidden" name="ajax" id="ajax" value="0" />

       <?php if ($show_client) { ?>
            <?php echo form_label($this->lang->line('client').":", $client['id']); ?>
            <select name="<?php echo $client['name'] ?>" id="<?php echo $client['id']; ?>" >
                <?php
                $clientID=end($this->uri->segment_array());
                $clientName=$this->m_client->fetch_client_name($clientID);
             
               if(isset($clientID)){
                if(is_numeric($clientID)){
                    echo "<option value='$clientID'>".$clientName."</option>";
                }
            }
                foreach ($clients as $c) {
                    if (isset($acl['view_deactive_site']) || $c->active) {
                        echo '<option value="'.$c->id.'" '.($client['value'] == $c->id ? 'selected="selected"' : '').'>'.$c->name.($c->active ? '' : ' (Deactivated)').'</option>';
                    }
                }
                ?>
            </select>
            <br class="clrflt"/>
        <?php } ?>

        <?php echo form_label($this->lang->line('start_date').":", $start_field['id']); ?>
        <?php echo form_input($start_field); ?>
        <div class="error"><?php echo form_error($start_field['name']); ?><?php echo isset($errors[$start_field['name']])?$errors[$start_field['name']]:''; ?></div>
	<br/>
        <?php echo form_label($this->lang->line('end_date').":", $end_field['id']); ?>
        <?php echo form_input($end_field); ?>
        <div class="error"><?php echo form_error($end_field['name']); ?><?php echo isset($errors[$end_field['name']])?$errors[$end_field['name']]:''; ?></div>
<br/>
        <?php //$this->view('reports/'.$report_name.'/detail_settings_form'); ?>

        <br class="clrflt"/>

        <div class="fltr"><button type="submit"><img src="<?php echo base_url() ?>assets/images/edit.png"/> <?php echo $this->lang->line('generate_report'); ?></button></div>
        <?php echo form_close(); ?>

    <?php // } ?>
