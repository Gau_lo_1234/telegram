<?php if (isset($telegram_messages) && $telegram_messages && count($telegram_messages) > 0 ) { ?>
<div class="table-container">
    <table class="report-detail">
        <tr>
            <th><?php echo $this->lang->line('cell') ?></th>
            <th><?php echo $this->lang->line('message') ?></th>
            <th colspan="2">Status</th>
            <th><?php echo $this->lang->line('sent_date') ?></th>
        </tr>
    <?php
        $c = 1;
        $t = 0;
        foreach($telegram_messages as $m => $message) {
            if ($m === "site_detail") { continue; }

            // if (!isset($summary[$clocking->point_code])) {
            //     $summary[$clocking->point_code] = array('name'=>$clocking->name, 'group_id'=>$clocking->group_id, 'count'=>1);
            // } else {
            //     $summary[$clocking->point_code]['count']++;
            // }
            ?>
           <tr class="<?php echo ($c % 2 ? 'odd' : 'even') ?>" >
                <?php
                echo '<td>'.$message->number.'</td>';
                echo '<td>'.(strlen($message->message) > 500 ? '<span title="'.$message->message.'">'.substr($message->message, 0, 50).'...</span>' : $message->message).'</td>';
                if ($message->status == 'okay') {
                    echo '<td colspan="2" class="positive">'.$message->status.'</td>';
                } else {
                    echo '<td class="negative" >'.$message->status.'</td>';
                    echo '<td  class="negative">';
                    try {
                        libxml_use_internal_errors(true);
                        $response = new SimpleXMLElement($message->delivery_response);
                        echo $response->call_result->error;
                    } catch(Exception $e) {
                        echo $message->delivery_response;
                    }
                    echo '</td>';
                }
                echo '<td>'.($message->created_date ?date("Y-m-d H:i:s",$message->created_date): '-').'</td>';
                //echo '<td>'.($message->created_date ? date('Y-m-d H:i:s',gmt_to_local($message->created_date, $tz)) : '-').'</td>';
            echo '</tr>';
            $c++;

            // $current_insert_date = $clocking->data_date;
        }
    ?>
    </table>
</div>
<?php if (isset($summary)) {
    /**
     SUMMARY
    */
    ?>
    <br class="clrfltl"/>
    <h3>Summary</h3>
    <table class="report-detail">
        <tr>
            <th ><?php echo  $this->lang->line('point_code') ?></th>
            <th ><?php echo  $this->lang->line('point_name') ?></th>
            <th ><?php echo  $this->lang->line('point_group') ?></th>
            <th>Total Times Clocked</th>
        </tr>
        <?php $c=0; foreach ($summary as $point_code=>$data) { ?>
        <tr <?php $c++; echo 'class="'.($c % 2 ? 'odd' : 'even' ). '"' ?>>
            <td><?php echo $point_code ?></td>
            <td><?php echo $data['name'] ?></td>
            <td><?php echo isset($groups[$data['group_id']]) ? $groups[$data['group_id']]->group_name : '-'; ?></td>
            <td><?php echo $data['count'] ?></td>
        </tr>
        <?php } ?>

    </table>
<?php
    }//isset summary
} else {
    echo $this->lang->line('report_no_data');
}
?>
