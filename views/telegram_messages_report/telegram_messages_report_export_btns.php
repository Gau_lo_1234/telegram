<?php
if (isset($telegram_messages) and count($telegram_messages)>1 ) {
    ?>
<div class="fltr">
    <form action="<?php echo  site_url('telegram/telegram_messages_report/generate_csv') ?>" method="post">
        <input type="hidden" name="start_date" value="<?php echo  set_value('start_date', date('Y-m-d')." 00:00") ?>" />
        <input type="hidden" name="end_date" value="<?php echo  set_value('end_date', date('Y-m-d', time()+86400)." 00:00") ?>" />
        <input type="hidden" name="client" value="<?php echo  set_value('client') ?>" />
        <button type="submit"><img src="<?php echo base_url() ?>assets/images/csv.gif" alt="csv"/> <?php echo  $this->lang->line('report_export_csv') ?></button>
    </form>
</div>

<?php $this->load->view('reports/export_pdf'); ?>

<?php // $this->load->view('reports/export_email'); ?>

<br class="clrflt"/>
<br class="clrflt"/>
<?php } ?>