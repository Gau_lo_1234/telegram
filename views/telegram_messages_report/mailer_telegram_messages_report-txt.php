<?php printf( $this->lang->line($type.'_mailer_subject'), 'Detailed', $site) ?>

<?php printf( $this->lang->line('report_mailer_content'), $start, $end) ?>

<?php printf( nl2br($this->lang->line('mailer_footer')), $site_name) ?>

<?php if (isset($unsubscribe_link) && $unsubscribe_link !== '') { 
     printf( nl2br($this->lang->line('unsubscribe_email_txt')), $unsubscribe_link);
 } ?>