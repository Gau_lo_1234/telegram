<?php
;
$key = array(
	'name'	=> 'key',
	'id'	=> 'key',
	'value'	=> set_value('key', isset($telegram_sender) ? $telegram_sender->key : ''),
	'maxlength'	=> 80,
	'size'	=> 30,
);
$name = array(
	'name'	=> 'name',
	'id'	=> 'name',
	'value'	=> set_value('name', isset($telegram_sender) ? $telegram_sender->name : ''),
	'maxlength'	=> 80,
	'size'	=> 30,
);

?>
<div class="container" style="width:100%;">
    <h2><?php echo $title ?></h2>

    <?php echo form_open(uri_string()); ?>

        <input type="hidden" name="group" value="sys">

        <div class="help">
            <?php echo $this->lang->line('settings_telegram_key_note') ?>
        </div>
        <?php echo form_label($this->lang->line('settings_bot_name'), $name['id']); ?>
        <?php echo form_input($name); ?>
                <div class="error"><?php echo form_error($name['name']); ?><?php echo isset($errors[$name['name']])?$errors[$name['name']]:''; ?></div>
        <br class="clrflt"/>

        <div class="help">
            <?php echo $this->lang->line('settings_telegram_key_note') ?>
        </div>
        <?php echo form_label($this->lang->line('settings_telegram_key'), $key['id']); ?>
        <?php echo form_input($key); ?>
                <div class="error"><?php echo form_error($key['name']); ?><?php echo isset($errors[$key['name']])?$errors[$key['name']]:''; ?></div>
        <br class="clrflt"/>

        
    <div class="fltr">
                &nbsp;<?php echo '<button type="submit"><img src="'.ASSET_URL.'images/yes.png"/> '.$this->lang->line('save').' '.$this->lang->line('settings_telegram_add_account').'</button>'; ?>
    </div>
    <?php echo form_close(); ?>
    <br class="clrflt" />
</div>
